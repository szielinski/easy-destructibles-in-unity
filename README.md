Destructible Walls. 

Single Class algorithm for numerical destruction of a concrete wall covered with plaster.
The plaster comes off first. The edges of the chipped fragments are blunted (bluntFraction).
The wall can be chipped off piece by piece. The break-up progress depends on the energy supplied to each hit.
The isolated wall fragments become separate objects and receive the bounding box and physics. The reaction is accompanied by sounds (damageSound).
Isolated wall fragments differentiation of shapes depends on chippedFragmentsDifferentiation, wallPlasterLayer and bluntFraction.
Dynamic mesh autoconstruction and mesh display functions are optimized for performance.
 						
Usage: add a class to an object (cuboid) on scene.

