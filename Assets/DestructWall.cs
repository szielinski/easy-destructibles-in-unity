﻿/********************************************************************************************************
 * 
 *      Destructible Walls. 
 *      Single Class algorithm for numerical destruction of a concrete wall covered with plaster.
 *      The plaster comes off first. The edges of the chipped fragments are blunted (bluntFraction).
 *      The wall can be chipped off piece by piece. The break-up progress depends on the energy supplied to each hit.
 *      The isolated wall fragments become separate objects and receive the bounding box and physics. 
 *      The reaction is accompanied by sounds (damageSound).
 *      Isolated wall fragments differentiation of shapes depends on chippedFragmentsDifferentiation, wallPlasterLayer and bluntFraction.
 *      Dynamic mesh autoconstruction and mesh display functions are optimized for performance.
 *                         
 *      Usage: add a class to an object (cuboid) on scene.
 *                         
 *      Version 0.9 (c) 2016-2017 Frost3D Games Sp. z o.o.        
 *      Programming: Sebastian Zielinski szielinski@frost3d.com
 * 
 * ******************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]


public class DestructWall : MonoBehaviour
{
    //================= wall resistance to damage ============
    public int WallHealth = 15;
    //=================== the maximum number of Colliders in the broken wall ================
    public int maxColliders = 32;
    //================ variety of chipped the fragments shapes ==============
    float bluntFraction = 0.25f; // how much to blunt the edges?
    public float fragmentSize = 0.5f;
    public float wallPlasterLayer = 0.25f;
    public bool fragmentDeform = true;
    float[] chippedFragmentsDifferentiation = new float[8] { 1.00f, 1.30f, 0.85f, 1.20f, 0.95f, 1.50f, 0.70f, 1.40f };
    //============= sound data ===================
    public AudioClip damageSound;
    //=== primitive data ==============
    Vector3[] primitive_vertices = null;
    Vector2[] primitive_uv = null;
    int[] primitive_triangles = null; int primitive_triangles_len = 0;
    Material primitive_material = null;
    Vector3 primitive_scale;
    //======================= plaster mesh data ===========
    Vector3[] outer_vertices = null; int outer_vertices_index = 0;
    Vector2[] outer_uv = null;
    Color[] outer_colors = null; // Color32
    int[] outer_triangles = null; int outer_triangles_len = 0;
    int maxtriangles = 5000;
    int maxvertex = 0;
    //======================= concrete mesh data ===========
    int[] inner_triangles = null; int inner_triangles_len = 0;
    int maxinnertriangles = 5000;
    //============== colliders ===========
    int boxCollidersNr = 0;
    public GameObject[] collideChild;
    public Vector3 primitive_collider_center;
    public Vector3 primitive_collider_size;
    //================== visibilty helpers ================
    int damageMask = 127;
    int damageMask1 = 127 - 3;
    int damageMask2 = 127 - 12;
    int damageMask3 = 127 - 48;
    int disableDepth = 256;
    int fragmentEnergy = 127; // 6 faces + center
    const int MAXFACE = 6;
    const int TABVERTSIZE = 5;
    public int DebugHitFace = -1;
    public int DebugVerticesNr = -1;
    //===================== face visibilty =========
    int[] nextp = new int[3] { 1, 2, 0 }; int[] prevp = new int[3] { 2, 0, 1 };
    int[] face_exist = new int[MAXFACE] { 0, 0, 0, 0, 0, 0 };
    int[] face_exist_n1 = new int[MAXFACE]; int[,] quadIndicesface = new int[MAXFACE, 4];
    //=========================== fragment matrices ============================
    int[, ,] box_fragments_power;
    Vector3[, ,] box_vectors;
    Vector2[,] face_teksture_vectors; Vector3[,] face_pos_vectors;
    int[] box_hunks_sizes = new int[3];
    float[] box_dimension = new float[3];
    public int testFace = 127;
    public int[] testPoint = new int[3] { 10, 1, 2 };
    int displayFaceInfo = 0;
    //==============================
    Mesh source_mesh = null;
    public Material source_material;
    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    //================= bit mask ===============
    int[, ,] dispBitMask = new int[2, 2, 2];
    //======================= speed up dimension index data =============
    int[] indOut0 = new int[] { 6, 0, 0, 1, 0, 0, 0, 0 }; // z y 
    int[] indOut1 = new int[] { 6, 0, 1, 1, 1, 0, 1, 3 }; // z y
    int[] indOut2 = new int[] { 2, 0, 6, 0, 2, 1, 2, 0 }; // x,z
    int[] indOut3 = new int[] { 3, 0, 6, 0, 3, 1, 3, 3 }; // x,z
    int[] indOut4 = new int[] { 4, 0, 4, 1, 6, 0, 4, 0 }; // x,y
    int[] indOut5 = new int[] { 5, 0, 5, 1, 6, 0, 5, 1 }; // x,y

    int[] indInn0 = new int[] { 0, 2, 0, 0, 0, 1, 0, 0 }; // z y 
    int[] indInn1 = new int[] { 1, 2, 1, 0, 1, 1, 1, 3 }; // z y
    int[] indInn2 = new int[] { 2, 2, 2, 0, 2, 1, 2, 0 }; // z y
    int[] indInn3 = new int[] { 3, 2, 3, 0, 3, 1, 3, 3 }; // z y
    int[] indInn4 = new int[] { 4, 2, 4, 0, 4, 1, 4, 0 }; // z y
    int[] indInn5 = new int[] { 5, 2, 5, 0, 5, 1, 5, 1 }; // z y 

    int[] pointOffset0 = new int[] { 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0 };
    int[] pointOffset1 = new int[] { 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1 };
    int[] pointOffset2 = new int[] { 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 2 };
    int[] pointOffset0Inn = new int[] { 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0 };
    int[] pointOffset1Inn = new int[] { 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 };
    int[] pointOffset2Inn = new int[] { 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 2 };

    //                                    1  2  3   4 
    int[] pointMasks0 = new int[] { 1 + 64, 4, 2, 32, 16, 1, 8 }; // 61+ [0] is an AND mask
    int[] pointMasks1 = new int[] { 2 + 64, 4, 2, 32, 16, 1, 8 }; // 62+
    int[] pointMasks2 = new int[] { 4 + 64, 4, 1, 32, 8, 2, 16 }; // 55+
    int[] pointMasks3 = new int[] { 8 + 64, 4, 1, 32, 8, 2, 16 }; // 59
    int[] pointMasks4 = new int[] { 16 + 64, 1, 2, 8, 16, 4, 32 }; // 31+
    int[] pointMasks5 = new int[] { 32 + 64, 1, 2, 8, 16, 4, 32 }; // 47+

    int[] pointMasks0L = new int[] { 1, 4, 2, 32, 16, 1, 8 }; // 127
    int[] pointMasks1L = new int[] { 2, 4, 2, 32, 16, 1, 8 };
    int[] pointMasks2L = new int[] { 4, 4, 1, 32, 8, 2, 16 };
    int[] pointMasks3L = new int[] { 8, 4, 1, 32, 8, 2, 16 };
    int[] pointMasks4L = new int[] { 16, 1, 2, 8, 16, 4, 32 };
    int[] pointMasks5L = new int[] { 32, 1, 2, 8, 16, 4, 32 };
    //========================================= outer layer depth =====================
    Vector3 fLayer;// fLayer.x

    void Awake()
    {
        WallHealth = 12;
    }

    //========================= Use this for initialization =====================================
    void Start()
    {
    }

    public void DestructThisWall(int hit_face)
    { // , Vector3 hit_position
        DebugHitFace = hit_face;
        //WallHealth = 14;
        if (!source_mesh) DuplicateMeshObject();
    }

    public void OnParticleCollision(GameObject missile)
    {
        Rigidbody body = missile.GetComponent<Rigidbody>();
        if (body)
        {
            Vector3 hitPosition = missile.transform.position;
            Debug.Log("DestructByMissile: " + hitPosition.x + " , " + hitPosition.y + " , " + hitPosition.z);
        }
        else Debug.Log("No rigibody");
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Fireball")
        {
            ContactPoint contact = collision.contacts[0];
            Vector3 pos = transform.InverseTransformPoint(contact.point);

            Vector3[] hitPositionAndDirection = new Vector3[2];
            hitPosition[0] = pos; //contact.point;
            hitPosition[1] = collision.relativeVelocity.normalized;//contact.normal;
            DestructThisWallBox(hitPosition);
        }
    }

    public void DestructThisWallBox(Vector3[] hitPositionAndDirection)
    {
        Vector3 hitVector = hitPositionAndDirection[1];
        if (!source_mesh) DuplicateMeshObject();
        DamageBoxHunk(hitPositionAndDirection[0], hitVector, 16, 0.4f);
        MakeNewMesh();
        MakeBoxColliders();
        //================ play damage sound ======================
        if (damageSound != null) AudioSource.PlayClipAtPoint(damageSound, transform.position, 2.0f);
    }

    public void DestructThisWallBoxSmall(Vector3[] hitPositionAndDirection)
    {
        Vector3 hitVector = hitPositionAndDirection[1];
        if (!source_mesh) DuplicateMeshObject();
        DamageBoxHunk(hitPositionAndDirection[0], hitVector, 1, 0.1f);
        MakeNewMesh();
        MakeBoxColliders();
        //================ play damage sound ======================
        if (damageSound != null) AudioSource.PlayClipAtPoint(damageSound, transform.position, 2.0f);
    }

    void DamageBoxHunk(Vector3 hitPosition, Vector3 hitVector, int maxHoles, float distHoles)
    {
        int maxTab = maxHoles;
        int numTab = 0;
        int[] pxTab = new int[maxTab];
        int[] pyTab = new int[maxTab];
        int[] pzTab = new int[maxTab];
        int[] newPower = new int[maxTab];
        meshFilter = (UnityEngine.MeshFilter)gameObject.GetComponent(typeof(MeshFilter));
        // meshFilter.mesh.bounds.center.x; meshFilter.mesh.bounds.center.y; meshFilter.mesh.bounds.center.z;
        float fragmentSizeScaledx = (meshFilter.mesh.bounds.max.x - meshFilter.mesh.bounds.min.x) * meshFilter.transform.localScale.x / box_hunks_sizes[0];
        float fragmentSizeScaledy = (meshFilter.mesh.bounds.max.y - meshFilter.mesh.bounds.min.y) * meshFilter.transform.localScale.y / box_hunks_sizes[1];
        float fragmentSizeScaledz = (meshFilter.mesh.bounds.max.z - meshFilter.mesh.bounds.min.z) * meshFilter.transform.localScale.z / box_hunks_sizes[2];
        Vector3 relative_hitVector = transform.InverseTransformDirection(hitVector).normalized; //relative_hitVector = relative_hitVector.normalized;
        int hitMask = 0;
        float x = Math.Abs(hitPosition[0] - meshFilter.mesh.bounds.max.x) * meshFilter.transform.localScale.x;
        float y = Math.Abs(hitPosition[1] - meshFilter.mesh.bounds.max.y) * meshFilter.transform.localScale.y;
        float z = Math.Abs(hitPosition[2] - meshFilter.mesh.bounds.max.z) * meshFilter.transform.localScale.z;
        if (x < (fragmentSizeScaledx * 0.2f)) hitMask |= 1;
        if (y < (fragmentSizeScaledy * 0.2f)) hitMask |= 4;
        if (z < (fragmentSizeScaledz * 0.2f)) hitMask |= 16;
        //============================================================================
        float dx = (hitPosition[0] - meshFilter.mesh.bounds.min.x) * meshFilter.transform.localScale.x;
        float dy = (hitPosition[1] - meshFilter.mesh.bounds.min.y) * meshFilter.transform.localScale.y;
        float dz = (hitPosition[2] - meshFilter.mesh.bounds.min.z) * meshFilter.transform.localScale.z;
        if (Math.Abs(dx) < (fragmentSizeScaledx * 0.2f)) hitMask |= 2;
        if (Math.Abs(dy) < (fragmentSizeScaledy * 0.2f)) hitMask |= 8;
        if (Math.Abs(dz) < (fragmentSizeScaledz * 0.2f)) hitMask |= 32;
        float fpx = ((dx) / fragmentSizeScaledx) + relative_hitVector.x * fragmentSizeScaledx * 0.3f;
        float fpy = ((dy) / fragmentSizeScaledy) + relative_hitVector.y * fragmentSizeScaledy * 0.3f;
        float fpz = ((dz) / fragmentSizeScaledz) + relative_hitVector.z * fragmentSizeScaledz * 0.3f;
        //==============================================================================
        for (int n = 0; n < maxHoles; ++n)
        {
            int px = (int)fpx; int py = (int)fpy; int pz = (int)fpz;
            float dist = distHoles;
            if (n > 0)
            {
                if ((n & 16) != 0) dist = dist * 4.0f;
                if ((n & 8) != 0) dist = dist * 2.0f;
                if ((n & 1) != 0) px = (int)(fpx + UnityEngine.Random.Range(dist * 0.6f, dist * 1.4f));
                else px = (int)(fpx - UnityEngine.Random.Range(dist * 0.6f, dist * 1.4f));
                if ((n & 2) != 0) py = (int)(fpy + UnityEngine.Random.Range(dist * 0.6f, dist * 1.4f));
                else py = (int)(fpy - UnityEngine.Random.Range(dist * 0.6f, dist * 1.4f));
                if ((n & 4) != 0) pz = (int)(fpz + UnityEngine.Random.Range(dist * 0.6f, dist * 1.4f));
                else pz = (int)(fpz - UnityEngine.Random.Range(dist * 0.6f, dist * 1.4f));
            }
            if (px >= box_hunks_sizes[0]) { px = box_hunks_sizes[0] - 1; } else if (px < 0) px = 0;
            if (py >= box_hunks_sizes[1]) { py = box_hunks_sizes[1] - 1; } else if (py < 0) py = 0;
            if (pz >= box_hunks_sizes[2]) { pz = box_hunks_sizes[2] - 1; } else if (pz < 0) pz = 0;

            //================= skip the same hits ==========================
            int yes = 0;
            for (int d = 0; d < numTab; ++d)
            {
                if ((pxTab[d] == px) && (pyTab[d] == py) && (pzTab[d] == pz))
                {
                    yes = 1;
                    d = numTab;
                }
            }

            //==================== new hit position ======================
            if (yes == 0)
            {
                //======================================================================
                int power = safe_box_fragments(px, py, pz);
                if (power > 0)
                {
                    pxTab[numTab] = px; pyTab[numTab] = py; pzTab[numTab] = pz;
                    int in_moc = power;
                    int start_moc = in_moc;
                    //========= damage to a fragment =====================
                    if (((hitMask & 1) != 0) && (px == (box_hunks_sizes[0] - 1))) power = power & (damageMask - 1);
                    if (((hitMask & 2) != 0) && (px == 0)) power = power & (damageMask - 2);
                    if (in_moc != power)
                    {
                        MakeNewChip(px, py, pz, hitVector, in_moc ^ power, power);
                        in_moc = power;
                    }
                    if (((hitMask & 4) != 0) && (py == (box_hunks_sizes[1] - 1))) power = power & (damageMask - 4);
                    if (((hitMask & 8) != 0) && (py == 0)) power = power & (damageMask - 8);
                    if (in_moc != power)
                    {
                        MakeNewChip(px, py, pz, hitVector, in_moc ^ power, power);
                        in_moc = power;
                    }
                    if (((hitMask & 16) != 0) && (pz == (box_hunks_sizes[2] - 1))) power = power & (damageMask - 16);
                    if (((hitMask & 32) != 0) && (pz == 0)) power = power & (damageMask - 32);
                    if (in_moc != power)
                    {
                        MakeNewChip(px, py, pz, hitVector, in_moc ^ power, power);
                        in_moc = power;
                    }
                    if (power == start_moc)
                    {
                        MakeNewChip(px, py, pz, hitVector, 0, 0);
                        power = 0;
                    }
                    newPower[numTab] = power;
                    numTab += 1;
                }
                else Debug.Log("false hit at: " + px + " " + py + " " + pz);
            }
        }
        //======================== save for later ============================
        for (int d = 0; d < numTab; ++d)
        {
            box_fragments_power[pxTab[d], pyTab[d], pzTab[d]] = newPower[d];
        }
        //======================================== search for tangents fragments X ==============
        checkLayerDamageX(0, 61, hitVector); checkLayerDamageX(box_hunks_sizes[0] - 1, 62, hitVector); // 62 61
        //======================================== search for tangents fragments Y ==============
        checkLayerDamageY(0, 55, hitVector); checkLayerDamageY(box_hunks_sizes[1] - 1, 59, hitVector); // 59 55
        //======================================== search for tangents fragments Z ==============
        checkLayerDamageZ(0, 31, hitVector); checkLayerDamageZ(box_hunks_sizes[2] - 1, 47, hitVector); // 47 31
        //=============================================== find holes ==========
        FindUnattachedFragments(hitVector);
    }

    void checkLayerDamageX(int px, int mask, Vector3 hitVector)
    {
        int unmask = (mask ^ 63) & 63;
        for (int y = 0; y < (box_hunks_sizes[1]); ++y)
        {
            for (int z = 0; z < (box_hunks_sizes[2]); ++z)
            {
                if (box_fragments_power[px, y, z] == 0)
                {
                    int power = safe_box_fragments(px, y - 1, z);
                    if ((power & unmask) != 0) { MakeNewChip(px, y - 1, z, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(px, y + 1, z);
                    if ((power & unmask) != 0) { MakeNewChip(px, y + 1, z, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(px, y, z - 1);
                    if ((power & unmask) != 0) { MakeNewChip(px, y, z - 1, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(px, y, z + 1);
                    if ((power & unmask) != 0) { MakeNewChip(px, y, z + 1, hitVector, power & unmask, power - unmask); }
                }
            }
        }
        if (box_hunks_sizes[0] == 1)
        {
            for (int y = 0; y < (box_hunks_sizes[1]); ++y)
            {
                for (int z = 0; z < (box_hunks_sizes[2]); ++z)
                {
                    int power = box_fragments_power[px, y, z];
                    if ((power & 1) == 0)
                    {
                        if ((power & 2) != 0) MakeNewChip(px, y, z, hitVector, 2, power - 2);
                    }
                    else if ((power & 2) == 0)
                    {
                        if ((power & 1) != 0) MakeNewChip(px, y, z, hitVector, 1, power - 1);
                    }
                }
            }
        }
    }

    void checkLayerDamageY(int py, int mask, Vector3 hitVector)
    {
        int unmask = (mask ^ 63) & 63;
        for (int z = 0; z < (box_hunks_sizes[2]); ++z)
        { //z
            for (int x = 0; x < (box_hunks_sizes[0]); ++x)
            {
                if (box_fragments_power[x, py, z] == 0)
                {
                    int power = safe_box_fragments(x - 1, py, z);
                    if ((power & unmask) != 0) { MakeNewChip(x - 1, py, z, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(x + 1, py, z);
                    if ((power & unmask) != 0) { MakeNewChip(x + 1, py, z, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(x, py, z - 1);
                    if ((power & unmask) != 0) { MakeNewChip(x, py, z - 1, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(x, py, z + 1);
                    if ((power & unmask) != 0) { MakeNewChip(x, py, z + 1, hitVector, power & unmask, power - unmask); }
                }
            }
        }
        if (box_hunks_sizes[1] == 1)
        {
            for (int z = 0; z < (box_hunks_sizes[2]); ++z)
            { //z
                for (int x = 0; x < (box_hunks_sizes[0]); ++x)
                {
                    int power = box_fragments_power[x, py, z];
                    if ((power & 4) == 0)
                    {
                        if ((power & 8) != 0) MakeNewChip(x, py, z, hitVector, 8, power - 8);
                    }
                    else if ((power & 8) == 0)
                    {
                        if ((power & 4) != 0) MakeNewChip(x, py, z, hitVector, 4, power - 4);
                    }
                }
            }
        }
    }

    void checkLayerDamageZ(int pz, int mask, Vector3 hitVector)
    {
        int unmask = (mask ^ 63) & 63;
        for (int y = 0; y < box_hunks_sizes[1]; ++y)
        {
            for (int x = 0; x < box_hunks_sizes[0]; ++x)
            {
                if (box_fragments_power[x, y, pz] == 0)
                {
                    int power = safe_box_fragments(x - 1, y, pz);
                    if ((power & unmask) != 0) { MakeNewChip(x - 1, y, pz, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(x + 1, y, pz);
                    if ((power & unmask) != 0) { MakeNewChip(x + 1, y, pz, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(x, y - 1, pz);
                    if ((power & unmask) != 0) { MakeNewChip(x, y - 1, pz, hitVector, power & unmask, power - unmask); }
                    power = safe_box_fragments(x, y + 1, pz);
                    if ((power & unmask) != 0) { MakeNewChip(x, y + 1, pz, hitVector, power & unmask, power - unmask); }
                }
            }
        }
        if (box_hunks_sizes[2] == 1)
        {
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    int power = box_fragments_power[x, y, pz];
                    if ((power & 16) == 0)
                    {
                        if ((power & 32) != 0) MakeNewChip(x, y, pz, hitVector, 32, power - 32);
                    }
                    else if ((power & 32) == 0)
                    {
                        if ((power & 16) != 0) MakeNewChip(x, y, pz, hitVector, 16, power - 16);
                    }
                }
            }
        }
    }

    int safe_box_fragments(int x, int y, int z)
    {
        if ((x >= 0) && (y >= 0) && (z >= 0))
        {
            if ((x < box_hunks_sizes[0]) && (y < box_hunks_sizes[1]) && (z < box_hunks_sizes[2])) return (box_fragments_power[x, y, z]);
        }
        return (0);
    }

    int valid_box_fragments(int x, int y, int z)
    {
        if ((x >= 0) && (y >= 0) && (z >= 0))
        {
            if ((x < box_hunks_sizes[0]) && (y < box_hunks_sizes[1]) && (z < box_hunks_sizes[2])) return (box_fragments_power[x, y, z]);
        }
        return (127);
    }

    int makeChipVertex(int nr, Vector3 pointDisplacement, int[] localIndex, int[] indInn, int face, Vector3 mov, int[] bottomInd)
    {
        if (localIndex[nr] >= 0) return (localIndex[nr]);
        //=============== add vertex ===========================
        if (face > 0) bottomInd[nr] = makeVertexInn(pointDisplacement + mov, indInn);
        localIndex[nr] = makeVertexInn(pointDisplacement, indInn); return (localIndex[nr]);
    }

    int fillChipVertex(int nr, Vector3 f, int[] localIndex, int[] indInn)
    {
        if (localIndex[nr] >= 0) return (localIndex[nr]);
        if (localIndex[1] >= 0) return (localIndex[1]);
        if (localIndex[2] >= 0) return (localIndex[2]);
        if (localIndex[3] >= 0) return (localIndex[3]);
        //=============== add vertex ===========================
        if (localIndex[0] < 0) localIndex[0] = makeVertexInn(f, indInn); return (localIndex[0]);
    }

    bool checkInd(int displacementIndicator, int mask_main, int mask_add)
    {
        if ((displacementIndicator & mask_main) != 0) { if ((displacementIndicator & mask_add) != 0) return (true); }
        return (false);
    }

    int duplicateFaceVertex()
    {
        int vertSt = outer_vertices_index;
        for (int v = 0; v < vertSt; ++v)
        {
            if (outer_vertices_index < maxvertex)
            {
                outer_vertices[outer_vertices_index] = outer_vertices[v];
                outer_uv[outer_vertices_index] = outer_uv[v];
                ++outer_vertices_index;
            }
        }
        return (vertSt);
    }

    void resetLocalInd(int[] localIndex, int[] bootInd) { localIndex[0] = -1; localIndex[1] = -1; localIndex[2] = -1; localIndex[3] = -1; bootInd[0] = -1; bootInd[1] = -1; bootInd[2] = -1; bootInd[3] = -1; }

    void MakeNewRoundedChipInner(int x, int y, int z, int dam, int dam2, int[] pointMasks, int[] indInn, bool flip, int[] pointOffset, bool flipDepth)
    {
        Vector2[] cmpDepth = new Vector2[4] { new Vector2(1f, 1f), new Vector2(1f, 1f), new Vector2(1f, 1f), new Vector2(1f, 1f) };
        int[] pointMode = new int[4] { 0, 0, 0, 0 };
        int[] quadIndicesA = new int[4] { -1, -1, -1, -1 };
        int[] quadIndicesBp = new int[4] { -1, -1, -1, -1 };
        int[] quadIndicesBm = new int[4] { -1, -1, -1, -1 };
        int[] quadIndicesCp = new int[4] { -1, -1, -1, -1 };
        int[] quadIndicesCm = new int[4] { -1, -1, -1, -1 };
        int[] quadIndicesD = new int[4] { -1, -1, -1, -1 };
        int[] footPrint = new int[1];
        int indSv;
        displayFaceInfo = 1;
        //============================================================ point 0 =============================================
        Vector3 mSf = cornerVector(x, y, z);
        Vector3 hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal
        Vector3 vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical
        int mask = pointMasks[1] | pointMasks[2];
        Vector3 f = layerVec(0, x, y, z, dam, dam2, pointOffset, flipDepth, footPrint);
        indSv = vertexDisplacementIndicatorChip(0, x, y, z, dam, pointOffset, flipDepth, pointMode, x, y, z, pointMasks[0]); //0
        if (pointMode[0] == 7) quadIndicesD[0] = makeVertexInn(f + hori_mSf + vert_mSf, indInn);
        if ((indSv & pointMasks[1]) != 0) quadIndicesA[0] = quadIndicesBp[0] = makeVertexInn(f + hori_mSf - get_dept_mSf(mSf, indSv, pointMasks[2], pointOffset, flipDepth), indInn);
        if ((indSv & pointMasks[2]) != 0) quadIndicesCp[0] = makeVertexInn(f + vert_mSf - get_dept_mSf(mSf, indSv, pointMasks[1], pointOffset, flipDepth), indInn);
        if (pointMode[0] != 3) quadIndicesA[0] = makeVertexInn(f - get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        cmpDepth[0] = compareVisibilityHV(0, x, y, z, dam, pointOffset, indSv, flipDepth, pointMode[0]);
        //============================================================ point 1 =============================================
        int xa = x + pointOffset[0]; int ya = y + pointOffset[1]; int za = z + pointOffset[2];
        mSf = cornerVector(xa, ya, za);
        hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal
        vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical
        mask = pointMasks[3] + pointMasks[2];
        f = layerVec(1, xa, ya, za, dam, dam2, pointOffset, flipDepth, footPrint);
        indSv = vertexDisplacementIndicatorChip(1, x, y, z, dam, pointOffset, flipDepth, pointMode, xa, ya, za, pointMasks[0]);
        if (pointMode[1] == 7) quadIndicesD[1] = makeVertexInn(f - hori_mSf + vert_mSf, indInn);
        if ((indSv & pointMasks[3]) != 0) quadIndicesBm[1] = makeVertexInn(f - hori_mSf - get_dept_mSf(mSf, indSv, pointMasks[2], pointOffset, flipDepth), indInn);
        if ((indSv & pointMasks[2]) != 0) quadIndicesA[1] = quadIndicesCp[1] = makeVertexInn(f + vert_mSf - get_dept_mSf(mSf, indSv, pointMasks[3], pointOffset, flipDepth), indInn);
        if (pointMode[1] != 3) quadIndicesA[1] = makeVertexInn(f - get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        cmpDepth[1] = compareVisibilityHV(1, xa, ya, za, dam, pointOffset, indSv, flipDepth, pointMode[1]);
        //============================================================ point 2 =============================================
        int xb = x + pointOffset[3]; int yb = y + pointOffset[4]; int zb = z + pointOffset[5];
        mSf = cornerVector(xb, yb, zb);
        hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal
        vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical
        mask = pointMasks[1] | pointMasks[4];
        f = layerVec(2, xb, yb, zb, dam, dam2, pointOffset, flipDepth, footPrint);
        indSv = vertexDisplacementIndicatorChip(2, x, y, z, dam, pointOffset, flipDepth, pointMode, xb, yb, zb, pointMasks[0]);
        if (pointMode[2] == 7) quadIndicesD[2] = makeVertexInn(f + hori_mSf - vert_mSf, indInn);
        if ((indSv & pointMasks[1]) != 0) quadIndicesBp[2] = makeVertexInn(f + hori_mSf - get_dept_mSf(mSf, indSv, pointMasks[4], pointOffset, flipDepth), indInn);
        if ((indSv & pointMasks[4]) != 0) quadIndicesA[2] = quadIndicesCm[2] = makeVertexInn(f - vert_mSf - get_dept_mSf(mSf, indSv, pointMasks[1], pointOffset, flipDepth), indInn);
        if (pointMode[2] != 3) quadIndicesA[2] = makeVertexInn(f - get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        cmpDepth[2] = compareVisibilityHV(2, xb, yb, zb, dam, pointOffset, indSv, flipDepth, pointMode[2]);
        //============================================================ point 3 =============================================
        int xc = x + pointOffset[6]; int yc = y + pointOffset[7]; int zc = z + pointOffset[8];
        mSf = cornerVector(xc, yc, zc);
        hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal 
        vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical  
        mask = pointMasks[3] | pointMasks[4];
        f = layerVec(3, xc, yc, zc, dam, dam2, pointOffset, flipDepth, footPrint);
        indSv = vertexDisplacementIndicatorChip(3, x, y, z, dam, pointOffset, flipDepth, pointMode, xc, yc, zc, pointMasks[0]);
        if (pointMode[3] == 7) quadIndicesD[3] = makeVertexInn(f - hori_mSf - vert_mSf, indInn);
        if ((indSv & pointMasks[3]) != 0) quadIndicesA[3] = quadIndicesBm[3] = makeVertexInn(f - hori_mSf - get_dept_mSf(mSf, indSv, pointMasks[4], pointOffset, flipDepth), indInn);
        if ((indSv & pointMasks[4]) != 0) quadIndicesCm[3] = makeVertexInn(f - vert_mSf - get_dept_mSf(mSf, indSv, pointMasks[3], pointOffset, flipDepth), indInn);
        if (pointMode[3] != 3) quadIndicesA[3] = makeVertexInn(f - get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        cmpDepth[3] = compareVisibilityHV(3, xc, yc, zc, dam, pointOffset, indSv, flipDepth, pointMode[3]);
        //===========================================================================================
        inner_triangles_len = makeMultiFaceD(quadIndicesA, quadIndicesBp, quadIndicesBm, quadIndicesCp, quadIndicesCm, quadIndicesD, pointMode, cmpDepth, flip, inner_triangles, inner_triangles_len);
        displayFaceInfo = 0;
    }

    void MakeNewRoundedChip(int px, int py, int pz, Vector3 localDir, int face, int[] pointOffset, int[] indInn, int[] pointMasks, Vector3 mov, int oldmoc, bool flipDepth)
    {
        int[] localIndex = new int[4];
        int[] bottomInd = new int[4];
        int[] chipInd = new int[24];
        int[] chipBotInd = new int[24];
        int[] footPrint = new int[1];
        int offset = 0;
        //==================================== point 0 ==================================
        Vector3 f = new Vector3(layerX(px, py, pz, oldmoc, pointOffset, footPrint), layerY(px, py, pz, oldmoc, pointOffset, footPrint), layerZ(px, py, pz, pointOffset, flipDepth));
        Vector3 fp0 = f;
        int displacementIndicator = localDisplacementIndicator(px, py, pz, damageMask, 0);
        Vector3 mSf = cornerVector(px, py, pz);
        Vector3 hori_mSf, vert_mSf, dept_mSf;
        if ((displacementIndicator & pointMasks[1]) != 0) hori_mSf = set_cornerHorizontal(mSf, pointOffset); else hori_mSf = new Vector3(0.0f, 0.0f, 0.0f); //corner horizontal surface YZ
        if ((displacementIndicator & pointMasks[2]) != 0) vert_mSf = set_cornerVertical(mSf, pointOffset); else vert_mSf = new Vector3(0.0f, 0.0f, 0.0f); //corner vertical surface YZ
        if ((displacementIndicator & pointMasks[5]) != 0) dept_mSf = set_cornerDepth(mSf, pointOffset); else dept_mSf = new Vector3(0.0f, 0.0f, 0.0f);
        resetLocalInd(localIndex, bottomInd);
        if (checkInd(displacementIndicator, pointMasks[1], pointMasks[2] | pointMasks[5])) { makeChipVertex(1, f + hori_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[2], pointMasks[1] | pointMasks[5])) { makeChipVertex(2, f + vert_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[5], pointMasks[1] | pointMasks[2])) { makeChipVertex(3, f + hori_mSf + vert_mSf, localIndex, indInn, face, mov, bottomInd); }
        chipInd[0] = fillChipVertex(1, f, localIndex, indInn); if (face > 0) chipBotInd[0] = fillChipVertex(1, f + mov, bottomInd, indInn);
        chipInd[1] = fillChipVertex(2, f, localIndex, indInn); if (face > 0) chipBotInd[1] = fillChipVertex(2, f + mov, bottomInd, indInn);
        chipInd[2] = fillChipVertex(3, f, localIndex, indInn); if (face > 0) chipBotInd[2] = fillChipVertex(3, f + mov, bottomInd, indInn);
        //==================================== point 1 ==================================
        int xa = px + pointOffset[0]; int ya = py + pointOffset[1]; int za = pz + pointOffset[2];
        f = new Vector3(layerX(xa, ya, za, oldmoc, pointOffset, footPrint), layerY(xa, ya, za, oldmoc, pointOffset, footPrint), layerZ(xa, ya, za, pointOffset, flipDepth));
        Vector3 fp1 = f;
        displacementIndicator = localDisplacementIndicator(xa, ya, za, damageMask, 0);
        mSf = cornerVector(xa, ya, za);
        if ((displacementIndicator & pointMasks[3]) != 0) hori_mSf = -set_cornerHorizontal(mSf, pointOffset); else hori_mSf = new Vector3(0.0f, 0.0f, 0.0f); //corner horizontal surface YZ
        if ((displacementIndicator & pointMasks[2]) != 0) vert_mSf = set_cornerVertical(mSf, pointOffset); else vert_mSf = new Vector3(0.0f, 0.0f, 0.0f); //corner vertical surface YZ
        if ((displacementIndicator & pointMasks[5]) != 0) dept_mSf = set_cornerDepth(mSf, pointOffset); else dept_mSf = new Vector3(0.0f, 0.0f, 0.0f);
        resetLocalInd(localIndex, bottomInd);
        if (checkInd(displacementIndicator, pointMasks[3], pointMasks[2] | pointMasks[5])) { makeChipVertex(1, f + hori_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[2], pointMasks[3] | pointMasks[5])) { makeChipVertex(2, f + vert_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[5], pointMasks[1] | pointMasks[2])) { makeChipVertex(3, f + hori_mSf + vert_mSf, localIndex, indInn, face, mov, bottomInd); }
        chipInd[3] = fillChipVertex(1, f, localIndex, indInn); if (face > 0) chipBotInd[3] = fillChipVertex(1, f + mov, bottomInd, indInn);
        chipInd[4] = fillChipVertex(2, f, localIndex, indInn); if (face > 0) chipBotInd[4] = fillChipVertex(2, f + mov, bottomInd, indInn);
        chipInd[5] = fillChipVertex(3, f, localIndex, indInn); if (face > 0) chipBotInd[5] = fillChipVertex(3, f + mov, bottomInd, indInn);
        //==================================== point 2 ==================================
        int xb = px + pointOffset[3]; int yb = py + pointOffset[4]; int zb = pz + pointOffset[5];
        f = new Vector3(layerX(xb, yb, zb, oldmoc, pointOffset, footPrint), layerY(xb, yb, zb, oldmoc, pointOffset, footPrint), layerZ(xb, yb, zb, pointOffset, flipDepth));
        displacementIndicator = localDisplacementIndicator(xb, yb, zb, damageMask, 0);
        mSf = cornerVector(xb, yb, zb);
        if ((displacementIndicator & pointMasks[1]) != 0) hori_mSf = set_cornerHorizontal(mSf, pointOffset); else hori_mSf = new Vector3(0.0f, 0.0f, 0.0f); //corner horizontal surface YZ
        if ((displacementIndicator & pointMasks[4]) != 0) vert_mSf = -set_cornerVertical(mSf, pointOffset); else vert_mSf = new Vector3(0.0f, 0.0f, 0.0f); //corner vertical surface YZ
        if ((displacementIndicator & pointMasks[5]) != 0) dept_mSf = set_cornerDepth(mSf, pointOffset); else dept_mSf = new Vector3(0.0f, 0.0f, 0.0f);
        resetLocalInd(localIndex, bottomInd);
        if (checkInd(displacementIndicator, pointMasks[1], pointMasks[4] | pointMasks[5])) { makeChipVertex(1, f + hori_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[4], pointMasks[1] | pointMasks[5])) { makeChipVertex(2, f + vert_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[5], pointMasks[1] | pointMasks[2])) { makeChipVertex(3, f + hori_mSf + vert_mSf, localIndex, indInn, face, mov, bottomInd); }
        chipInd[6] = fillChipVertex(1, f, localIndex, indInn); if (face > 0) chipBotInd[6] = fillChipVertex(1, f + mov, bottomInd, indInn);
        chipInd[7] = fillChipVertex(2, f, localIndex, indInn); if (face > 0) chipBotInd[7] = fillChipVertex(2, f + mov, bottomInd, indInn);
        chipInd[8] = fillChipVertex(3, f, localIndex, indInn); if (face > 0) chipBotInd[8] = fillChipVertex(3, f + mov, bottomInd, indInn);
        //==================================== point 3 ==================================
        int xc = px + pointOffset[6]; int yc = py + pointOffset[7]; int zc = pz + pointOffset[8];
        f = new Vector3(layerX(xc, yc, zc, oldmoc, pointOffset, footPrint), layerY(xc, yc, zc, oldmoc, pointOffset, footPrint), layerZ(xc, yc, zc, pointOffset, flipDepth));
        displacementIndicator = localDisplacementIndicator(xc, yc, zc, damageMask, 0);
        mSf = cornerVector(xc, yc, zc);
        if ((displacementIndicator & pointMasks[3]) != 0) hori_mSf = -set_cornerHorizontal(mSf, pointOffset); else hori_mSf = new Vector3(0.0f, 0.0f, 0.0f); //corner horizontal surface YZ
        if ((displacementIndicator & pointMasks[4]) != 0) vert_mSf = -set_cornerVertical(mSf, pointOffset); else vert_mSf = new Vector3(0.0f, 0.0f, 0.0f);//corner vertical surface YZ
        if ((displacementIndicator & pointMasks[5]) != 0) dept_mSf = set_cornerDepth(mSf, pointOffset); else dept_mSf = new Vector3(0.0f, 0.0f, 0.0f);
        resetLocalInd(localIndex, bottomInd);
        if (checkInd(displacementIndicator, pointMasks[3], pointMasks[4] | pointMasks[5])) { makeChipVertex(1, f + hori_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[4], pointMasks[3] | pointMasks[5])) { makeChipVertex(2, f + vert_mSf + dept_mSf, localIndex, indInn, face, mov, bottomInd); }
        if (checkInd(displacementIndicator, pointMasks[5], pointMasks[1] | pointMasks[2])) { makeChipVertex(3, f + hori_mSf + vert_mSf, localIndex, indInn, face, mov, bottomInd); }
        chipInd[9] = fillChipVertex(1, f, localIndex, indInn); if (face > 0) chipBotInd[9] = fillChipVertex(1, f + mov, bottomInd, indInn);
        chipInd[10] = fillChipVertex(2, f, localIndex, indInn); if (face > 0) chipBotInd[10] = fillChipVertex(2, f + mov, bottomInd, indInn);
        chipInd[11] = fillChipVertex(3, f, localIndex, indInn); if (face > 0) chipBotInd[11] = fillChipVertex(3, f + mov, bottomInd, indInn);
        //==================== make faces ============
        if (face > 0)
        {
            bool zew = false; bool wne = true;
            if ((face == 2) || (face == 4) || (face == 32)) { zew = true; wne = false; } // 4-8 flip
            Debug.Log("chip face: " + face);
            offset = duplicateFaceVertex();
            makeFaceOut(chipInd[2], chipInd[5], chipInd[11], chipInd[8], zew); // 11 8
            makeFaceOut(chipInd[0], chipInd[3], chipInd[5], chipInd[2], zew); // 5 2
            makeFaceOut(chipInd[8], chipInd[11], chipInd[9], chipInd[6], zew); // 9 6
            makeFaceOut(chipInd[1], chipInd[2], chipInd[8], chipInd[7], zew); // 8 7 
            makeFaceOut(chipInd[5], chipInd[4], chipInd[10], chipInd[11], zew); // 10 11
            makeFaceOut(chipInd[3], chipInd[4], chipInd[5], zew);
            makeFaceOut(chipInd[11], chipInd[10], chipInd[9], zew);
            makeFaceOut(chipInd[7], chipInd[8], chipInd[6], zew);
            makeFaceOut(chipInd[1], chipInd[0], chipInd[2], zew);
            //=================== bottom =============================
            makeFaceSeq(chipBotInd[2], chipBotInd[5], chipBotInd[11], chipBotInd[8], wne); // 11 8
            makeFaceSeq(chipBotInd[0], chipBotInd[3], chipBotInd[5], chipBotInd[2], wne); // 5 2
            makeFaceSeq(chipBotInd[8], chipBotInd[11], chipBotInd[9], chipBotInd[6], wne); // 9 6
            makeFaceSeq(chipBotInd[1], chipBotInd[2], chipBotInd[8], chipBotInd[7], wne); // 8 7 
            makeFaceSeq(chipBotInd[5], chipBotInd[4], chipBotInd[10], chipBotInd[11], wne); // 10 11
            makeFaceInn(chipBotInd[3], chipBotInd[4], chipBotInd[5], zew);
            makeFaceInn(chipBotInd[11], chipBotInd[10], chipBotInd[9], zew);
            makeFaceInn(chipBotInd[7], chipBotInd[8], chipBotInd[6], zew);
            makeFaceInn(chipBotInd[1], chipBotInd[0], chipBotInd[2], zew);
            //============================= sides =====================
            makeFaceSeq(chipInd[3] + offset, chipInd[0] + offset, chipBotInd[0] + offset, chipBotInd[3] + offset, zew);
            makeFaceSeq(chipInd[4] + offset, chipInd[3] + offset, chipBotInd[3] + offset, chipBotInd[4] + offset, zew);
            makeFaceSeq(chipInd[10] + offset, chipInd[4] + offset, chipBotInd[4] + offset, chipBotInd[10] + offset, zew);
            makeFaceSeq(chipInd[9] + offset, chipInd[10] + offset, chipBotInd[10] + offset, chipBotInd[9] + offset, zew);
            makeFaceSeq(chipInd[6] + offset, chipInd[9] + offset, chipBotInd[9] + offset, chipBotInd[6] + offset, zew);
            makeFaceSeq(chipInd[7] + offset, chipInd[6] + offset, chipBotInd[6] + offset, chipBotInd[7] + offset, zew);
            makeFaceSeq(chipInd[1] + offset, chipInd[7] + offset, chipBotInd[7] + offset, chipBotInd[1] + offset, zew);
            makeFaceSeq(chipInd[0] + offset, chipInd[1] + offset, chipBotInd[1] + offset, chipBotInd[0] + offset, zew);

        }
        else
        {
            //==============================================================================
            bool zew = false;
            //if ((front == 2) || (front == 4) || (front == 32)) { zew = true;  } // 4-8 flip
            makeFaceSeq(chipInd[2], chipInd[5], chipInd[11], chipInd[8], zew); // 11 8
            makeFaceSeq(chipInd[0], chipInd[3], chipInd[5], chipInd[2], zew); // 5 2
            makeFaceSeq(chipInd[8], chipInd[11], chipInd[9], chipInd[6], zew); // 9 6
            makeFaceSeq(chipInd[1], chipInd[2], chipInd[8], chipInd[7], zew); // 8 7 
            makeFaceSeq(chipInd[5], chipInd[4], chipInd[10], chipInd[11], zew); // 10 11
            makeFaceInn(chipInd[3], chipInd[4], chipInd[5], zew);
            makeFaceInn(chipInd[11], chipInd[10], chipInd[9], zew);
            makeFaceInn(chipInd[7], chipInd[8], chipInd[6], zew);
            makeFaceInn(chipInd[1], chipInd[0], chipInd[2], zew);
        }
        //===============================================================================
        if (face == 1)
        {
            if (localDir.x < 0.0f) localDir.x = -localDir.x * 0.5f;
        }
        else if (face == 2)
        {
            if (localDir.x > 0.0f) localDir.x = -localDir.x * 0.5f;
        }
        else if (face == 4)
        {
            if (localDir.y < 0.0f) localDir.y = -localDir.y * 0.5f;
        }
        else if (face == 8)
        {
            if (localDir.y > 0.0f) localDir.y = -localDir.y * 0.5f;
        }
        else if (face == 16)
        {
            if (localDir.z < 0.0f) localDir.z = -localDir.z * 0.5f;
        }
        else if (face == 32)
        {
            if (localDir.z > 0.0f) localDir.z = -localDir.z * 0.5f;
        }
    }

    void MakeNewSimpleChip(int px, int py, int pz, Vector3 localDir, int face)
    {
        Vector3[, ,] bv = new Vector3[2, 2, 2];
        bv[0, 0, 0] = box_vectors[px, py, pz];
        bv[0, 0, 1] = box_vectors[px, py + 0, pz + 1];
        bv[0, 1, 0] = box_vectors[px, py + 1, pz + 0];
        bv[0, 1, 1] = box_vectors[px, py + 1, pz + 1];
        bv[1, 0, 0] = box_vectors[px + 1, py + 0, pz + 0];
        bv[1, 0, 1] = box_vectors[px + 1, py + 0, pz + 1];
        bv[1, 1, 0] = box_vectors[px + 1, py + 1, pz + 0];
        bv[1, 1, 1] = box_vectors[px + 1, py + 1, pz + 1];
        if (face == 1)
        {
            if (localDir.x < 0.0f) localDir.x = -localDir.x * 0.5f;
            bv[0, 0, 0].x = bv[1, 0, 0].x - fLayer.x;
            bv[0, 0, 1].x = bv[1, 0, 1].x - fLayer.x;
            bv[0, 1, 0].x = bv[1, 1, 0].x - fLayer.x;
            bv[0, 1, 1].x = bv[1, 1, 1].x - fLayer.x;
        }
        else if (face == 2)
        {
            if (localDir.x > 0.0f) localDir.x = -localDir.x * 0.5f;
            bv[1, 0, 0].x = bv[0, 0, 0].x + fLayer.x;
            bv[1, 0, 1].x = bv[0, 0, 1].x + fLayer.x;
            bv[1, 1, 0].x = bv[0, 1, 0].x + fLayer.x;
            bv[1, 1, 1].x = bv[0, 1, 1].x + fLayer.x;
        }
        else if (face == 4)
        {
            if (localDir.y < 0.0f) localDir.y = -localDir.y * 0.5f;
            bv[0, 1, 0].y = bv[0, 0, 0].y - fLayer.y;
            bv[0, 1, 1].y = bv[0, 0, 1].y - fLayer.y;
            bv[1, 1, 0].y = bv[1, 0, 0].y - fLayer.y;
            bv[1, 1, 1].y = bv[1, 0, 1].y - fLayer.y;
        }
        else if (face == 8)
        {
            if (localDir.y > 0.0f) localDir.y = -localDir.y * 0.5f;
            bv[0, 1, 0].y = bv[0, 0, 0].y + fLayer.y; // OK!
            bv[0, 1, 1].y = bv[0, 0, 1].y + fLayer.y;
            bv[1, 1, 0].y = bv[1, 0, 0].y + fLayer.y;
            bv[1, 1, 1].y = bv[1, 0, 1].y + fLayer.y;
        }
        else if (face == 16)
        {
            if (localDir.z < 0.0f) localDir.z = -localDir.z * 0.5f;
            bv[0, 0, 0].z = bv[0, 0, 1].z - fLayer.z;
            bv[0, 1, 0].z = bv[0, 1, 1].z - fLayer.z;
            bv[1, 0, 0].z = bv[1, 0, 1].z - fLayer.z;
            bv[1, 1, 0].z = bv[1, 1, 1].z - fLayer.z;
        }
        else if (face == 32)
        {
            if (localDir.z > 0.0f) localDir.z = -localDir.z * 0.5f;
            bv[0, 0, 1].z = bv[0, 0, 0].z + fLayer.z;
            bv[0, 1, 1].z = bv[0, 1, 0].z + fLayer.z;
            bv[1, 0, 1].z = bv[1, 0, 0].z + fLayer.z;
            bv[1, 1, 1].z = bv[1, 1, 0].z + fLayer.z;
        }
        //========================= x ==============================
        int a, b, c, d, x, y, z;
        x = px + 1; y = py; z = pz;
        if ((face & 1) != 0)
        { // FIXME corner
            a = makeVertexOut(x, y, z, indOut0); b = makeVertexOut(x, y, z + 1, indOut0); c = makeVertexOut(x, y + 1, z, indOut0); d = makeVertexOut(x, y + 1, z + 1, indOut0); makeFaceInn(a, b, c, d, false);
        }
        else
        {
            a = makeVertexInn(bv[1, 0, 0].z, bv[1, 0, 0].y, 1.0f - bv[1, 0, 0].x);
            b = makeVertexInn(bv[1, 0, 1].z, bv[1, 0, 1].y, 1.0f - bv[1, 0, 1].x);
            c = makeVertexInn(bv[1, 1, 0].z, bv[1, 1, 0].y, 1.0f - bv[1, 1, 0].x);
            d = makeVertexInn(bv[1, 1, 1].z, bv[1, 1, 1].y, 1.0f - bv[1, 1, 1].x);
            makeFaceInn(a, b, c, d, false);
        }
        //================================================================
        x = px; y = py; z = pz;
        if ((face & 2) != 0)
        {  // FIXME corner
            a = makeVertexOut(x, y, z, indOut1); b = makeVertexOut(x, y, z + 1, indOut1); c = makeVertexOut(x, y + 1, z, indOut1); d = makeVertexOut(x, y + 1, z + 1, indOut1); makeFaceInn(a, c, b, d, false);
        }
        else
        {
            a = makeVertexInn1(bv[0, 0, 0].z, bv[0, 0, 0].y, bv[0, 0, 0].x);
            b = makeVertexInn1(bv[0, 0, 1].z, bv[0, 0, 1].y, bv[0, 0, 1].x);
            c = makeVertexInn1(bv[0, 1, 0].z, bv[0, 1, 0].y, bv[0, 1, 0].x);
            d = makeVertexInn1(bv[0, 1, 1].z, bv[0, 1, 1].y, bv[0, 1, 1].x);
            makeFaceInn(a, c, b, d, false);
        }
        //======================== y ==============================
        x = px; y = py + 1; z = pz;
        if ((face & 4) != 0)
        {
            a = makeVertexOut(x, y, z, indOut2); b = makeVertexOut(x, y, z + 1, indOut2); c = makeVertexOut(x + 1, y, z, indOut2); d = makeVertexOut(x + 1, y, z + 1, indOut2); makeFaceInn(a, c, b, d, false);
        }
        else
        {
            a = makeVertexInn2(bv[0, 1, 0].x, bv[0, 1, 0].z, 1.0f - bv[0, 1, 0].y);
            b = makeVertexInn2(bv[0, 1, 1].x, bv[0, 1, 1].z, 1.0f - bv[0, 1, 1].y);
            c = makeVertexInn2(bv[1, 1, 0].x, bv[1, 1, 0].z, 1.0f - bv[1, 1, 0].y);
            d = makeVertexInn2(bv[1, 1, 1].x, bv[1, 1, 1].z, 1.0f - bv[1, 1, 1].y);
            makeFaceInn(a, c, b, d, false);
        }
        //================================================================
        x = px; y = py; z = pz;
        if ((face & 8) != 0)
        {
            a = makeVertexOut(x, y, z, indOut3); b = makeVertexOut(x, y, z + 1, indOut3); c = makeVertexOut(x + 1, y, z, indOut3); d = makeVertexOut(x + 1, y, z + 1, indOut3); makeFaceInn(a, b, c, d, false);
        }
        else
        {
            a = makeVertexInn3(bv[0, 0, 0].x, bv[0, 0, 0].z, bv[0, 0, 0].y);
            b = makeVertexInn3(bv[0, 0, 1].x, bv[0, 0, 1].z, bv[0, 0, 1].y);
            c = makeVertexInn3(bv[1, 0, 0].x, bv[1, 0, 0].z, bv[1, 0, 0].y);
            d = makeVertexInn3(bv[1, 0, 1].x, bv[1, 0, 1].z, bv[1, 0, 1].y);
            makeFaceInn(a, b, c, d, false);
        }
        //======================== z ==============================
        x = px; y = py; z = pz + 1;
        if ((face & 16) != 0)
        {
            a = makeVertexOut(x, y, z, indOut4); b = makeVertexOut(x + 1, y, z, indOut4); c = makeVertexOut(x, y + 1, z, indOut4); d = makeVertexOut(x + 1, y + 1, z, indOut4); makeFaceInn(a, c, b, d, false);
        }
        else
        {
            a = makeVertexInn4(bv[0, 0, 1].x, bv[0, 0, 1].y, 1.0f - bv[0, 0, 1].z);
            b = makeVertexInn4(bv[1, 0, 1].x, bv[1, 0, 1].y, 1.0f - bv[1, 0, 1].z);
            c = makeVertexInn4(bv[0, 1, 1].x, bv[0, 1, 1].y, 1.0f - bv[0, 1, 1].z);
            d = makeVertexInn4(bv[1, 1, 1].x, bv[1, 1, 1].y, 1.0f - bv[1, 1, 1].z);
            makeFaceInn(a, c, b, d, false);
        }
        //================================================================
        x = px; y = py; z = pz;
        if ((face & 32) != 0)
        {
            a = makeVertexOut(x, y, z, indOut5); b = makeVertexOut(x + 1, y, z, indOut5); c = makeVertexOut(x, y + 1, z, indOut5); d = makeVertexOut(x + 1, y + 1, z, indOut5); makeFaceInn(a, b, c, d, false);
        }
        else
        {
            a = makeVertexInn5(bv[0, 0, 0].x, bv[0, 0, 0].y, bv[0, 0, 0].z);
            b = makeVertexInn5(bv[1, 0, 0].x, bv[1, 0, 0].y, bv[1, 0, 0].z);
            c = makeVertexInn5(bv[0, 1, 0].x, bv[0, 1, 0].y, bv[0, 1, 0].z);
            d = makeVertexInn5(bv[1, 1, 0].x, bv[1, 1, 0].y, bv[1, 1, 0].z);
            makeFaceInn(a, b, c, d, false);
        }
    }

    int planDirection(int px, int py, int pz, int face, float[] shapeDirection)
    {
        int front = 0;
        if (safe_box_fragments(px + 1, py, pz) == 0) front |= 1;
        else if (safe_box_fragments(px - 1, py, pz) == 0) front |= 2;
        else if (safe_box_fragments(px, py + 1, pz) == 0) front |= 4;
        else if (safe_box_fragments(px, py - 1, pz) == 0) front |= 8;
        else if (safe_box_fragments(px, py, pz + 1) == 0) front |= 16;
        else if (safe_box_fragments(px, py, pz - 1) == 0) front |= 32;
        //====================================================
        int fr = front; if ((fr & 3) == 0) fr = face; if ((fr & 3) == 2) shapeDirection[0] -= 1f; else if ((fr & 3) == 1) shapeDirection[0] += 1f; if ((front & 3) == 0) shapeDirection[0] *= 0.1f;
        //====================================================
        fr = front; if ((fr & 12) == 0) fr = face; if ((fr & 12) == 8) shapeDirection[1] -= 1f; else if ((fr & 12) == 4) shapeDirection[1] += 1f; if ((front & 12) == 0) shapeDirection[1] *= 0.1f;
        //====================================================
        fr = front; if ((fr & 48) == 0) fr = face; if ((fr & 48) == 32) shapeDirection[2] -= 1f; else if ((fr & 48) == 16) shapeDirection[2] += 1f; if ((front & 48) == 0) shapeDirection[2] *= 0.1f;
        return (front);
    }

    void MakeNewChip(int px, int py, int pz, Vector3 hitVector, int face, int newPower)
    { // , int power
        maxvertex = 96;
        maxtriangles = 240;
        maxinnertriangles = maxtriangles;
        float[] shapeDirection = new float[3] { UnityEngine.Random.Range(-0.6f, 0.6f), UnityEngine.Random.Range(-0.6f, 0.6f), UnityEngine.Random.Range(-0.6f, 0.6f) };
        //===========================================================
        int front = face & 63;
        int fr = planDirection(px, py, pz, front, shapeDirection);
        if (front == 0) front = fr; if (front == 0) { Debug.Log("inner damage, x: " + px + " y: " + py + " z: " + pz); return; }
        int oldmoc = box_fragments_power[px, py, pz];
        box_fragments_power[px, py, pz] = newPower;
        //return;
        //=============================================================
        outer_vertices = null; outer_vertices = new Vector3[maxvertex];
        outer_uv = null; outer_uv = new Vector2[maxvertex]; outer_vertices_index = 0;
        outer_triangles = null; outer_triangles = new int[maxtriangles]; outer_triangles_len = 0;
        inner_triangles = null; inner_triangles = new int[maxinnertriangles]; inner_triangles_len = 0;
        //============================================
        int max_Line_len = box_hunks_sizes[0]; if (max_Line_len < box_hunks_sizes[1]) max_Line_len = box_hunks_sizes[1]; if (max_Line_len < box_hunks_sizes[2]) max_Line_len = box_hunks_sizes[2];
        //==========================================================================================================
        Vector3 boxX = new Vector3(1.3f, 1.3f, 1.3f); // scale
        Vector3 boxC = new Vector3(0.0f, 0.0f, 0.0f); // centre
        // word to local
        Vector3 localDir = transform.InverseTransformDirection(new Vector3(hitVector.x * UnityEngine.Random.Range(1.0f, 1.4f), hitVector.y * UnityEngine.Random.Range(1.0f, 1.4f), hitVector.z * UnityEngine.Random.Range(1.0f, 1.4f)));
        //Debug.Log ("Chip face : " + face + " hitvek: " + hitVector.x + " , " + hitVector.y + " , " + hitVector.z + " , localDir : " + localDir.x + " , " + localDir.y + " , " + localDir.z );
        //Debug.Log ("Chip face : " + face +" newPower: "+newPower+ " pos: " + px + " , " + py + " , " + pz );
        //MakeNewSimpleChip( px, py, pz, localDir, front );
        if ((face == 1) && ((newPower & 64) != 0)) MakeNewRoundedChip(px + 1, py, pz, localDir, face, pointOffset0Inn, indInn0, pointMasks0L, getDepthChip(front, false), oldmoc, true);
        else if ((face == 2) && ((newPower & 64) != 0)) MakeNewRoundedChip(px, py, pz, localDir, face, pointOffset0Inn, indInn1, pointMasks1L, getDepthChip(front, false), oldmoc, false);
        else if ((face == 4) && ((newPower & 64) != 0)) MakeNewRoundedChip(px, py + 1, pz, localDir, face, pointOffset1Inn, indInn2, pointMasks2L, getDepthChip(front, false), oldmoc, true);
        else if ((face == 8) && ((newPower & 64) != 0)) MakeNewRoundedChip(px, py, pz, localDir, face, pointOffset1Inn, indInn3, pointMasks3L, getDepthChip(front, false), oldmoc, false);
        else if ((face == 16) && ((newPower & 64) != 0)) MakeNewRoundedChip(px, py, pz + 1, localDir, face, pointOffset2Inn, indInn4, pointMasks4L, getDepthChip(front, false), oldmoc, true);
        else if ((face == 32) && ((newPower & 64) != 0)) MakeNewRoundedChip(px, py, pz, localDir, face, pointOffset2Inn, indInn5, pointMasks5L, getDepthChip(front, false), oldmoc, false);
        else
        {
            //=========== make mesh ==============
            int dam = oldmoc & damageMask;
            int dam2 = safe_box_fragments(px + 1, py, pz) & damageMask;
            if (px == (box_hunks_sizes[0] - 1)) MakeNewRoundedChipInner(px + 1, py, pz, dam, dam2, pointMasks0L, indInn0, false, pointOffset0, true);
            else MakeNewRoundedChipInner(px + 1, py, pz, dam, dam2, pointMasks0L, indInn0, false, pointOffset0Inn, true);
            dam2 = safe_box_fragments(px - 1, py, pz) & damageMask;
            if (px == 0) MakeNewRoundedChipInner(px, py, pz, dam, dam2, pointMasks1L, indInn1, true, pointOffset0, false);
            else MakeNewRoundedChipInner(px, py, pz, dam, dam2, pointMasks1L, indInn1, true, pointOffset0Inn, false);
            dam2 = safe_box_fragments(px, py + 1, pz) & damageMask;
            if (py == (box_hunks_sizes[1] - 1)) MakeNewRoundedChipInner(px, py + 1, pz, dam, dam2, pointMasks2L, indInn2, true, pointOffset1, true);
            else MakeNewRoundedChipInner(px, py + 1, pz, dam, dam2, pointMasks2L, indInn2, true, pointOffset1Inn, true);
            dam2 = safe_box_fragments(px, py - 1, pz) & damageMask;
            if (py == 0) MakeNewRoundedChipInner(px, py, pz, dam, dam2, pointMasks3L, indInn3, false, pointOffset1, false);
            else MakeNewRoundedChipInner(px, py, pz, dam, dam2, pointMasks3L, indInn3, false, pointOffset1Inn, false);
            dam2 = safe_box_fragments(px, py, pz + 1) & damageMask;
            if (pz == (box_hunks_sizes[2] - 1)) MakeNewRoundedChipInner(px, py, pz + 1, dam, dam2, pointMasks4L, indInn4, true, pointOffset2, true);
            else MakeNewRoundedChipInner(px, py, pz + 1, dam, dam2, pointMasks4L, indInn4, true, pointOffset2Inn, true);
            dam2 = safe_box_fragments(px, py, pz - 1) & damageMask;
            if (pz == 0) MakeNewRoundedChipInner(px, py, pz, dam, dam2, pointMasks5L, indInn5, false, pointOffset2, false);
            else MakeNewRoundedChipInner(px, py, pz, dam, dam2, pointMasks5L, indInn5, false, pointOffset2Inn, false);
            //Debug.Log ("Inn box inner_triangles_len: "+inner_triangles_len );
        }
        //==========================================================================================================
        //Debug.Log ("outer_vertices_index: " + outer_vertices_index);
        Array.Resize<Vector3>(ref outer_vertices, outer_vertices_index);
        Array.Resize<Vector2>(ref outer_uv, outer_vertices_index);
        Array.Resize<int>(ref outer_triangles, outer_triangles_len);
        Array.Resize<int>(ref inner_triangles, inner_triangles_len);
        //================================================================
        Vector3 centre_local = FindLocalCentre(outer_vertices, outer_vertices_index);
        Vector3 centre_local_normalized = centre_local; centre_local_normalized.Normalize();
        Vector3 size_local = FindLocalSize(outer_vertices, outer_vertices_index, centre_local);
        ScaleLocalVertex(outer_vertices, outer_vertices_index);
        //=============================================================================================
        //var vertices = mesh.vertices;
        //var worldPos = transform.TransformPoint(vertices[0]);
        GameObject destructChip = new GameObject("Chip");
        //===========================================================
        Vector3 chipPosition = transform.TransformPoint(centre_local);
        //destructChip.transform.parent = transform; 
        destructChip.transform.rotation = transform.rotation;
        destructChip.transform.localPosition = Vector3.zero;
        destructChip.transform.localScale = Vector3.one;
        destructChip.transform.position = chipPosition;
        //====================================================================
        BoxCollider boxCollider = destructChip.AddComponent<BoxCollider>();
        boxCollider.center = boxC;
        boxCollider.size = new Vector3(size_local.x * primitive_scale.x * boxX.x, size_local.y * primitive_scale.y * boxX.y, size_local.z * primitive_scale.z * boxX.z);
        //====================== Add Components ==============================
        Rigidbody rigBody = destructChip.AddComponent<Rigidbody>();
        rigBody.mass = 5;
        rigBody.isKinematic = false; // change it after traffic stops or after some time
        rigBody.detectCollisions = true;
        rigBody.useGravity = true;
        rigBody.position = chipPosition;//transform.TransformPoint( centre_local );

        Vector3 recoil = transform.TransformDirection(new Vector3(shapeDirection[0] * 3f, shapeDirection[1] * 3f, shapeDirection[2] * 3f));
        if ((recoil.y > -3f) && (recoil.y < 4f)) recoil.y += 3.1f;//-0.1f;

        rigBody.velocity = recoil; //rigBody.AddRelativeForce( recoil*10.0f, ForceMode.Impulse); // local coordinates
        float scale = 440.0f;
        rigBody.AddTorque(new Vector3(UnityEngine.Random.Range(-scale, scale), UnityEngine.Random.Range(-scale, scale), UnityEngine.Random.Range(-scale, scale)));
        //rigBody.angularVelocity = new Vector3 (UnityEngine.Random.Range(-scale, scale ), UnityEngine.Random.Range(-scale, scale ), UnityEngine.Random.Range(-scale, scale ) );
        //================== new mesh ================================
        MeshFilter meshFilter = destructChip.AddComponent<MeshFilter>();
        source_mesh = new Mesh();
        source_mesh.Clear();
        source_mesh.name = "DestructibleChip";
        int matnr; if (outer_triangles_len > 0) matnr = 2; else matnr = 1;
        source_mesh.subMeshCount = matnr;
        source_mesh.vertices = outer_vertices;
        source_mesh.uv = outer_uv;
        source_mesh.SetTriangles(inner_triangles, 0);
        if (outer_triangles_len > 0) source_mesh.SetTriangles(outer_triangles, 1);
        //meshFilter.transform.localScale += new Vector3 (1.0f, 1.0f, 1.01f);
        meshFilter.mesh = source_mesh;
        meshFilter.mesh.RecalculateNormals();
        //meshFilter.mesh.Optimize();
        //=========================== debug position ==================
        //Debug.Log ("boxcollider size local: " + size_local + " primitive_scale: "+primitive_scale+" boxX: "+boxX+" shapeDirection: "+shapeDirection[0]+" "+shapeDirection[1]+" "+shapeDirection[2]+" face: "+face);

        //===================================================================
        MeshRenderer meshRenderer = destructChip.AddComponent<MeshRenderer>();
        Material[] mats = new Material[matnr];
        mats[0] = source_material;//primitive_material;
        if (matnr > 1) mats[1] = primitive_material;//source_material;
        meshRenderer.material = source_material;//primitive_material;//source_material;
        meshRenderer.materials = mats;

    }

    bool testColliders(int xp, int yp, int zp, int dx, int dy, int dz, int[, ,] fc)
    {
        for (int z = 0; z < dz; ++z)
        {
            for (int y = 0; y < dy; ++y)
            {
                for (int x = 0; x < dx; ++x) { if (fc[xp + x, yp + y, zp + z] != 0) return (false); }
            }
        }
        return (true);
    }

    int getLayer(int x, int y, int z, int[, ,] fillFragments)
    {
        int layer = 100000;
        if ((x > 0) && (fillFragments[x - 1, y, z] > 0)) { if (layer > fillFragments[x - 1, y, z]) layer = fillFragments[x - 1, y, z]; }
        if ((x < (box_hunks_sizes[0] - 1)) && (fillFragments[x + 1, y, z] > 0)) { if (layer > fillFragments[x + 1, y, z]) layer = fillFragments[x + 1, y, z]; }
        if ((z > 0) && (fillFragments[x, y, z - 1] > 0)) { if (layer > fillFragments[x, y, z - 1]) layer = fillFragments[x, y, z - 1]; }
        if ((z < (box_hunks_sizes[2] - 1)) && (fillFragments[x, y, z + 1] > 0)) { if (layer > fillFragments[x, y, z + 1]) layer = fillFragments[x, y, z + 1]; }
        if ((y > 0) && (fillFragments[x, y - 1, z] > 0)) { if (layer > fillFragments[x, y - 1, z]) layer = fillFragments[x, y - 1, z]; }
        if ((y < (box_hunks_sizes[1] - 1)) && (fillFragments[x, y + 1, z] > 0)) { if (layer > fillFragments[x, y + 1, z]) layer = fillFragments[x, y + 1, z]; }
        return (layer);
    }

    void FindUnattachedFragments(Vector3 hitVector)
    {
        int[, ,] fillFragments = new int[box_hunks_sizes[0], box_hunks_sizes[1], box_hunks_sizes[2]];
        //========== clear
        for (int z = 0; z < box_hunks_sizes[2]; ++z)
        { //z
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if (box_fragments_power[x, y, z] > 0) fillFragments[x, y, z] = 0; else fillFragments[x, y, z] = -2;  // no box
                }
            }
        }
        //============= find UP direction ==================
        float dup1 = Vector3.Dot(transform.up, Vector3.up);
        float dup2 = Vector3.Dot(transform.right, Vector3.up);
        float dup3 = Vector3.Dot(transform.forward, Vector3.up);
        //========== foundation ==================
        int layer_max = 1;
        if (dup1 > 0.9f)
        {
            for (int z = 0; z < box_hunks_sizes[2]; ++z)
            { //z
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if (box_fragments_power[x, 0, z] > 0) fillFragments[x, 0, z] = layer_max; // attachement
                }
            }
        }
        else if (dup1 < -0.9f)
        {
            for (int z = 0; z < box_hunks_sizes[2]; ++z)
            { //z
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if (box_fragments_power[x, box_hunks_sizes[1] - 1, z] > 0) fillFragments[x, box_hunks_sizes[1] - 1, z] = layer_max; // attachement
                }
            }
        }
        else if (dup2 > 0.9f)
        {
            for (int z = 0; z < box_hunks_sizes[2]; ++z)
            { //z
                for (int y = 0; y < box_hunks_sizes[1]; ++y)
                {
                    if (box_fragments_power[0, y, z] > 0) fillFragments[0, y, z] = layer_max; // attachement
                }
            }
        }
        else if (dup2 < -0.9f)
        {
            for (int z = 0; z < box_hunks_sizes[2]; ++z)
            { //z
                for (int y = 0; y < box_hunks_sizes[1]; ++y)
                {
                    if (box_fragments_power[box_hunks_sizes[0] - 1, y, z] > 0) fillFragments[box_hunks_sizes[0] - 1, y, z] = layer_max; // attachement
                }
            }
        }
        else if (dup3 > 0.9f)
        {
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            { //z
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if (box_fragments_power[x, y, 0] > 0) fillFragments[x, y, 0] = layer_max; // attachement
                }
            }
        }
        else if (dup3 < -0.9f)
        {
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            { //z
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if (box_fragments_power[x, y, box_hunks_sizes[2] - 1] > 0) fillFragments[x, y, box_hunks_sizes[2] - 1] = layer_max; // attachement
                }
            }
        }
        //==============================
        bool any_hole_fix = true; bool any_hole = true;
        int hole_loops = 2;
        while ((any_hole_fix) && (hole_loops > 0))
        {
            hole_loops -= 1;
            any_hole_fix = false;
            any_hole = false;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                { //z
                    for (int x = 0; x < box_hunks_sizes[0]; ++x)
                    {
                        if (fillFragments[x, y, z] == 0)
                        {
                            //if (any_hole == false) min_y = y;
                            int layer = getLayer(x, y, z, fillFragments);
                            //=======================================
                            if (layer < 100000)
                            {
                                fillFragments[x, y, z] = layer + 1;
                                //=========== count the holes in each layer ============
                                if (layer_max <= layer) layer_max = layer + 1;
                                any_hole_fix = true;
                            }
                            else any_hole = true;
                        }
                    }
                }
            }
            //=======================================
            if (any_hole)
            {
                for (int y = box_hunks_sizes[1] - 1; y >= 0; y -= 1)
                {
                    for (int z = box_hunks_sizes[2] - 1; z >= 0; z -= 1)
                    { //z
                        for (int x = box_hunks_sizes[0] - 1; x >= 0; x -= 1)
                        {
                            if (fillFragments[x, y, z] == 0)
                            {
                                //if (any_hole == false) min_y = y;
                                int layer = getLayer(x, y, z, fillFragments);
                                //=======================================
                                if (layer < 100000)
                                {
                                    fillFragments[x, y, z] = layer + 1;
                                    //=========== count the holes in each layer ============
                                    if (layer_max <= layer) layer_max = layer + 1;
                                    any_hole_fix = true;
                                }
                                else any_hole = true;
                            }
                        }
                    }
                }
            }
        }
        //==================== find the groove in layers and then reverse the part above the foundation =======================
        if (any_hole)
        {
            for (int z = 0; z < box_hunks_sizes[2]; ++z)
            { //z
                for (int y = 0; y < box_hunks_sizes[1]; ++y)
                {
                    for (int x = 0; x < box_hunks_sizes[0]; ++x)
                    {
                        if (fillFragments[x, y, z] == 0)
                        {
                            if (box_fragments_power[x, y, z] > 0) MakeNewChip(x, y, z, hitVector, 0, 0);
                        }
                    }
                }
            }
            //=================================================
        }
    }

    void MakeBoxColliders()
    {
        int[, ,] fillColliders = new int[box_hunks_sizes[0], box_hunks_sizes[1], box_hunks_sizes[2]];
        for (int z = 0; z < box_hunks_sizes[2]; ++z)
        { //z
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if (box_fragments_power[x, y, z] > 0) fillColliders[x, y, z] = 0; else fillColliders[x, y, z] = 2;  // no box
                }
            }
        }
        //===========================
        int newCollidersNr = 0;
        for (int zp = 0; zp < box_hunks_sizes[2]; ++zp)
        { //z
            for (int yp = 0; yp < box_hunks_sizes[1]; ++yp)
            {
                for (int xp = 0; xp < box_hunks_sizes[0]; ++xp)
                {
                    if (fillColliders[xp, yp, zp] == 0)
                    { // no collider but chip
                        int dx = 1, dy = 1, dz = 1;
                        bool grow = true;
                        while (grow != false)
                        {
                            grow = false;
                            if (((xp + dx) < box_hunks_sizes[0]) && (testColliders(xp, yp, zp, dx + 1, dy, dz, fillColliders))) { dx += 1; grow = true; }
                            if (((yp + dy) < box_hunks_sizes[1]) && (testColliders(xp, yp, zp, dx, dy + 1, dz, fillColliders))) { dy += 1; grow = true; }
                            if (((zp + dz) < box_hunks_sizes[2]) && (testColliders(xp, yp, zp, dx, dy, dz + 1, fillColliders))) { dz += 1; grow = true; }
                        }
                        //======= add collider ===========
                        for (int z = 0; z < dz; ++z)
                        {
                            for (int y = 0; y < dy; ++y)
                            {
                                for (int x = 0; x < dx; ++x) fillColliders[xp + x, yp + y, zp + z] = 1;
                            }
                        }
                        //=====================================
                        if (newCollidersNr < maxColliders)
                        {
                            BoxCollider boxCollider;
                            if (newCollidersNr >= boxCollidersNr)
                            {
                                collideChild[newCollidersNr] = new GameObject("CollideChild" + newCollidersNr);
                                collideChild[newCollidersNr].transform.parent = transform;
                                collideChild[newCollidersNr].transform.rotation = transform.rotation;
                                collideChild[newCollidersNr].transform.position = transform.position;
                                collideChild[newCollidersNr].transform.localPosition = Vector3.zero;
                                collideChild[newCollidersNr].transform.localScale = Vector3.one;
                                boxCollider = collideChild[newCollidersNr].AddComponent<BoxCollider>();
                                collideChild[newCollidersNr].AddComponent<DestructedFragment>();
                                boxCollidersNr += 1;
                            }
                            else
                            {
                                //Debug.Log ("Use old Collider: " + newCollidersNr + " ,all: "+ boxCollidersNr);
                                boxCollider = collideChild[newCollidersNr].GetComponent<BoxCollider>();
                            }
                            //====================
                            BoxCollider srcCollider = GetComponent<BoxCollider>();
                            //==============================
                            Vector3 stepCollider;
                            stepCollider.x = primitive_collider_size.x / box_hunks_sizes[0];
                            stepCollider.y = primitive_collider_size.y / box_hunks_sizes[1];
                            stepCollider.z = primitive_collider_size.z / box_hunks_sizes[2];
                            //===================================================

                            boxCollider.enabled = true;
                            boxCollider.center = new Vector3((float)((xp + dx * 0.5f - box_hunks_sizes[0] * 0.5f) * stepCollider.x) + srcCollider.center.x, (float)((yp + dy * 0.5f - box_hunks_sizes[1] * 0.5f) * stepCollider.y) + srcCollider.center.y, (float)((zp + dz * 0.5f - box_hunks_sizes[2] * 0.5f) * stepCollider.z + srcCollider.center.z));
                            boxCollider.size = new Vector3(stepCollider.x * dx, stepCollider.y * dy, stepCollider.z * dz);//size_local;
                            ++newCollidersNr;
                        }
                    }
                }
            }
        }
        //======= disable unused colliders =========
        while (newCollidersNr < boxCollidersNr)
        {
            BoxCollider boxCollider = collideChild[newCollidersNr].GetComponent<BoxCollider>();
            boxCollider.enabled = false;
            ++newCollidersNr;

        }
        //============= disable prime box collider
        BoxCollider bc = GetComponent<BoxCollider>();
        bc.enabled = false;
    }



    void MakeNewMesh()
    {
        maxvertex = (box_hunks_sizes[1] + 1) * (box_hunks_sizes[2] + 1) * 2 + (box_hunks_sizes[0] + 1) * (box_hunks_sizes[1] + 1) * 2 + (box_hunks_sizes[0] + 1) * (box_hunks_sizes[2] + 1) * 400;
        //maxvertex = 32;
        maxtriangles = maxvertex * 6;
        maxinnertriangles = maxvertex * 6;
        //=============================================================
        outer_vertices = null; outer_vertices = new Vector3[maxvertex]; outer_colors = null; outer_colors = new Color[maxvertex];
        outer_uv = null; outer_uv = new Vector2[maxvertex]; outer_vertices_index = 0;
        outer_triangles = null; outer_triangles = new int[maxtriangles]; outer_triangles_len = 0;
        inner_triangles = null; inner_triangles = new int[maxinnertriangles]; inner_triangles_len = 0;
        //=========================
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        primitive_collider_center = boxCollider.center;
        primitive_collider_size = boxCollider.size;
        //===================== camera =====================
        Camera cam = Camera.main;
        float dist = Vector3.Distance(cam.transform.position, transform.position);

        if (dist > 10f) MakeMeshMediumDetails(); else MakeMeshHighDetails();

        //====================== make inner layer =======================
        Array.Resize<Vector3>(ref outer_vertices, outer_vertices_index);
        Array.Resize<Vector2>(ref outer_uv, outer_vertices_index);
        Array.Resize<int>(ref outer_triangles, outer_triangles_len);
        Array.Resize<int>(ref inner_triangles, inner_triangles_len);
        Array.Resize<Color>(ref outer_colors, outer_vertices_index);

        //================== new mesh ================================
        source_mesh = new Mesh();
        source_mesh.Clear();
        source_mesh.name = "DestructibleWall";
        source_mesh.subMeshCount = meshFilter.mesh.subMeshCount + 1;

        source_mesh.vertices = outer_vertices;
        source_mesh.uv = outer_uv;
        source_mesh.SetTriangles(outer_triangles, 0);
        source_mesh.SetTriangles(inner_triangles, 1);
        source_mesh.colors = outer_colors;
        meshFilter.mesh = source_mesh;

        meshFilter.mesh.RecalculateNormals();

        //=================================================
        Material[] mats = new Material[2];
        mats[0] = primitive_material;
        mats[1] = source_material;
        //========================= materials ============================================
        meshRenderer.material = primitive_material;//source_material;
        meshRenderer.materials = mats;//new Material[1];

    }

    void DuplicateMeshObject()
    {
        collideChild = null; collideChild = new GameObject[maxColliders];
        //=========================
        makeBitMask();
        //============ save old mesh ==========================================
        meshFilter = (UnityEngine.MeshFilter)gameObject.GetComponent(typeof(MeshFilter));
        primitive_vertices = new Vector3[meshFilter.mesh.vertices.Length]; for (int i = 0; i < meshFilter.mesh.vertices.Length; i++) primitive_vertices[i] = meshFilter.mesh.vertices[i];
        primitive_uv = new Vector2[meshFilter.mesh.uv.Length]; for (int i = 0; i < meshFilter.mesh.uv.Length; i++) primitive_uv[i] = meshFilter.mesh.uv[i];
        primitive_triangles_len = meshFilter.mesh.triangles.Length;
        primitive_triangles = new int[meshFilter.mesh.triangles.Length]; for (int i = 0; i < meshFilter.mesh.triangles.Length; i++) primitive_triangles[i] = meshFilter.mesh.triangles[i];
        DebugVerticesNr = meshFilter.mesh.vertices.Length;
        primitive_scale = meshFilter.transform.localScale;
        //====== real-scale bounds ===========
        box_dimension[0] = meshFilter.mesh.bounds.size.x * meshFilter.transform.localScale.x;
        box_dimension[1] = meshFilter.mesh.bounds.size.y * meshFilter.transform.localScale.y;
        box_dimension[2] = meshFilter.mesh.bounds.size.z * meshFilter.transform.localScale.z;
        if (fragmentSize <= 0.01f) fragmentSize = 0.1f;
        box_hunks_sizes[0] = (int)(box_dimension[0] / fragmentSize); box_hunks_sizes[1] = (int)(box_dimension[1] / fragmentSize); box_hunks_sizes[2] = (int)(box_dimension[2] / fragmentSize);
        if (box_hunks_sizes[0] < 1) box_hunks_sizes[0] = 1; else if (box_hunks_sizes[0] == 1) box_hunks_sizes[0] = 2;
        if (box_hunks_sizes[1] < 1) box_hunks_sizes[1] = 1; else if (box_hunks_sizes[1] == 1) box_hunks_sizes[1] = 2;
        if (box_hunks_sizes[2] < 1) box_hunks_sizes[2] = 1; else if (box_hunks_sizes[2] == 1) box_hunks_sizes[2] = 2;
        box_fragments_power = new int[box_hunks_sizes[0], box_hunks_sizes[1], box_hunks_sizes[2]];
        for (int x = 0; x < box_hunks_sizes[0]; ++x)
        {
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                for (int z = 0; z < box_hunks_sizes[2]; ++z) box_fragments_power[x, y, z] = fragmentEnergy;
            }
        }
        //========== make vectors ===========================
        box_vectors = new Vector3[box_hunks_sizes[0] + 1, box_hunks_sizes[1] + 1, box_hunks_sizes[2] + 1];
        for (int x = 0; x <= box_hunks_sizes[0]; ++x)
        {
            for (int y = 0; y <= box_hunks_sizes[1]; ++y)
            {
                for (int z = 0; z <= box_hunks_sizes[2]; ++z)
                {
                    box_vectors[x, y, z].x = randVektor(x, box_hunks_sizes[0]);
                    box_vectors[x, y, z].y = randVektor(y, box_hunks_sizes[1]);
                    box_vectors[x, y, z].z = randVektor(z, box_hunks_sizes[2]);
                    if (x == box_hunks_sizes[0]) { box_vectors[x, y, z].y = box_vectors[x - 1, y, z].y; box_vectors[x, y, z].z = box_vectors[x - 1, y, z].z; }
                    if (y == box_hunks_sizes[1]) { box_vectors[x, y, z].x = box_vectors[x, y - 1, z].x; box_vectors[x, y, z].z = box_vectors[x, y - 1, z].z; }
                    if (z == box_hunks_sizes[2]) { box_vectors[x, y, z].y = box_vectors[x, y, z - 1].y; box_vectors[x, y, z].x = box_vectors[x, y, z - 1].x; }
                }
            }
        }
        //=================== first Layer vector ==========================
        fLayer = new Vector3(box_vectors[1, 0, 0].x * wallPlasterLayer, box_vectors[0, 1, 0].y * wallPlasterLayer, box_vectors[0, 0, 1].z * wallPlasterLayer);
        //================================= make data ============
        checkOryginalFaces();
        //======================================
        face_teksture_vectors = new Vector2[7, 2];
        face_pos_vectors = new Vector3[7, 3];
        face_teksture_vectors[0, 0] = makeFaceTekVector(quadIndicesface[0, 0], quadIndicesface[0, 3]); // side0 x=1
        face_teksture_vectors[0, 1] = makeFaceTekVector(quadIndicesface[0, 0], quadIndicesface[0, 1]);
        face_teksture_vectors[1, 0] = makeFaceTekVector(quadIndicesface[1, 3], quadIndicesface[1, 0]); // side1 x=-1   1,1  1,2
        face_teksture_vectors[1, 1] = makeFaceTekVector(quadIndicesface[1, 3], quadIndicesface[1, 2]);         //      1,1  1,0
        face_teksture_vectors[2, 0] = makeFaceTekVector(quadIndicesface[2, 0], quadIndicesface[2, 3]); // side1 y=1
        face_teksture_vectors[2, 1] = makeFaceTekVector(quadIndicesface[2, 0], quadIndicesface[2, 1]);
        face_teksture_vectors[3, 0] = makeFaceTekVector(quadIndicesface[3, 3], quadIndicesface[3, 0]); // side1 y=-1
        face_teksture_vectors[3, 1] = makeFaceTekVector(quadIndicesface[3, 3], quadIndicesface[3, 2]);
        face_teksture_vectors[4, 1] = makeFaceTekVector(quadIndicesface[4, 0], quadIndicesface[4, 3]); // side1 z=1
        face_teksture_vectors[4, 0] = makeFaceTekVector(quadIndicesface[4, 0], quadIndicesface[4, 1]);
        face_teksture_vectors[5, 1] = makeFaceTekVector(quadIndicesface[5, 1], quadIndicesface[5, 2]); // side1 z=-1
        face_teksture_vectors[5, 0] = makeFaceTekVector(quadIndicesface[5, 1], quadIndicesface[5, 0]);
        face_teksture_vectors[6, 0] = new Vector3(0f, 0f, 0f);
        face_teksture_vectors[6, 1] = new Vector3(0f, 0f, 0f);
        //======================================
        float div = 0f;
        Vector3 vekX = new Vector3(0f, 0f, 0f);
        if ((quadIndicesface[1, 3] >= 0) && (quadIndicesface[0, 0] >= 0)) { vekX += makeFacePosVector(quadIndicesface[1, 3], quadIndicesface[0, 0]); div += 1f; }
        if ((quadIndicesface[2, 0] >= 0) && (quadIndicesface[2, 3] >= 0)) { vekX += makeFacePosVector(quadIndicesface[2, 0], quadIndicesface[2, 3]); div += 1f; }
        if ((quadIndicesface[3, 3] >= 0) && (quadIndicesface[3, 0] >= 0)) { vekX += makeFacePosVector(quadIndicesface[3, 3], quadIndicesface[3, 0]); div += 1f; }
        if ((quadIndicesface[4, 0] >= 0) && (quadIndicesface[4, 1] >= 0)) { vekX += makeFacePosVector(quadIndicesface[4, 0], quadIndicesface[4, 1]); div += 1f; }
        if ((quadIndicesface[5, 1] >= 0) && (quadIndicesface[5, 0] >= 0)) { vekX += makeFacePosVector(quadIndicesface[5, 1], quadIndicesface[5, 0]); div += 1f; }
        if (div > 0f) vekX = vekX / div;
        //=====================================================
        Vector3 vekY = new Vector3(0f, 0f, 0f); div = 0f;
        if ((quadIndicesface[0, 0] >= 0) && (quadIndicesface[0, 1] >= 0)) { vekY += makeFacePosVector(quadIndicesface[0, 0], quadIndicesface[0, 1]); div += 1f; }
        if ((quadIndicesface[1, 3] >= 0) && (quadIndicesface[1, 2] >= 0)) { vekY += makeFacePosVector(quadIndicesface[1, 3], quadIndicesface[1, 2]); div += 1f; }
        if ((quadIndicesface[3, 3] >= 0) && (quadIndicesface[2, 0] >= 0)) { vekY += makeFacePosVector(quadIndicesface[3, 3], quadIndicesface[2, 0]); div += 1f; }
        if ((quadIndicesface[4, 0] >= 0) && (quadIndicesface[4, 3] >= 0)) { vekY += makeFacePosVector(quadIndicesface[4, 0], quadIndicesface[4, 3]); div += 1f; }
        if ((quadIndicesface[5, 1] >= 0) && (quadIndicesface[5, 2] >= 0)) { vekY += makeFacePosVector(quadIndicesface[5, 1], quadIndicesface[5, 2]); div += 1f; }
        if (div > 0f) vekY = vekY / div;
        //=====================================================
        Vector3 vekZ = new Vector3(0f, 0f, 0f); div = 0f;
        if ((quadIndicesface[0, 0] >= 0) && (quadIndicesface[0, 3] >= 0)) { vekZ += makeFacePosVector(quadIndicesface[0, 0], quadIndicesface[0, 3]); div += 1f; }
        if ((quadIndicesface[1, 3] >= 0) && (quadIndicesface[1, 0] >= 0)) { vekZ += makeFacePosVector(quadIndicesface[1, 3], quadIndicesface[1, 0]); div += 1f; }
        if ((quadIndicesface[2, 0] >= 0) && (quadIndicesface[2, 1] >= 0)) { vekZ += makeFacePosVector(quadIndicesface[2, 0], quadIndicesface[2, 1]); div += 1f; }
        if ((quadIndicesface[3, 3] >= 0) && (quadIndicesface[3, 2] >= 0)) { vekZ += makeFacePosVector(quadIndicesface[3, 3], quadIndicesface[3, 2]); div += 1f; }
        if ((quadIndicesface[5, 1] >= 0) && (quadIndicesface[4, 0] >= 0)) { vekZ += makeFacePosVector(quadIndicesface[5, 1], quadIndicesface[4, 0]); div += 1f; }
        if (div > 0f) vekZ = vekZ / div;

        //Debug.Log ("vekX: " + vekX + " , vekY: " + vekY + " , vekZ: " + vekZ);
        face_pos_vectors[0, 0] = vekZ;// side0  z=1
        face_pos_vectors[0, 1] = vekY;// y=1
        face_pos_vectors[0, 2] = -vekX;
        //================= face 1 =================
        face_pos_vectors[1, 0] = vekZ;// side1 z= -1     1,1   1,0  1,1 1,2
        face_pos_vectors[1, 1] = vekY;// y=1             1,1   1,2  1,2 1,3
        face_pos_vectors[1, 2] = vekX;
        //================= face 2 =================
        face_pos_vectors[2, 0] = vekX;// side2 x=1
        face_pos_vectors[2, 1] = vekZ;// z= 1
        face_pos_vectors[2, 2] = -vekY;
        //================= face 3 =================
        face_pos_vectors[3, 0] = vekX;// side3 x=-1   3,2 3,1
        face_pos_vectors[3, 1] = vekZ;// z=1          3,2 3,3
        face_pos_vectors[3, 2] = vekY;
        //================= face 4 =================
        face_pos_vectors[4, 1] = vekY;// side4 x=-1
        face_pos_vectors[4, 0] = vekX;// y=1
        face_pos_vectors[4, 2] = -vekZ;
        //================= face 5 =================
        face_pos_vectors[5, 0] = vekX;// side5 x=1      5,2 5,1 5,3 5,0
        face_pos_vectors[5, 1] = vekY;// y=1             5,2 5,3 5,3 5,2
        face_pos_vectors[5, 2] = vekZ;

        face_pos_vectors[6, 0] = new Vector3(0f, 0f, 0f);
        face_pos_vectors[6, 1] = new Vector3(0f, 0f, 0f);
        face_pos_vectors[6, 2] = new Vector3(0f, 0f, 0f);
        //============================================ fix mesh deformation =================
        primitive_vertices[quadIndicesface[1, 3]] = primitive_vertices[quadIndicesface[0, 0]] - vekX;
        primitive_vertices[quadIndicesface[2, 0]] = primitive_vertices[quadIndicesface[0, 0]] - vekX + vekY;
        primitive_vertices[quadIndicesface[3, 3]] = primitive_vertices[quadIndicesface[0, 0]] - vekX;
        primitive_vertices[quadIndicesface[4, 0]] = primitive_vertices[quadIndicesface[0, 0]] - vekX + vekZ;
        primitive_vertices[quadIndicesface[5, 1]] = primitive_vertices[quadIndicesface[0, 0]] - vekX;

        //===============================
        meshRenderer = (UnityEngine.MeshRenderer)gameObject.GetComponent(typeof(MeshRenderer));
        primitive_material = meshRenderer.material;
    }

    int getMask(int x, int y, int z)
    {
        if ((x >= box_hunks_sizes[0]) || (y >= box_hunks_sizes[1]) || (z >= box_hunks_sizes[2]) || (x < 0) || (y < 0) || (z < 0)) return (0);
        return (box_fragments_power[x, y, z]);
    }

    int boxMask(int x, int y, int z, int bitmask)
    { // ,bool layer
        if ((x >= box_hunks_sizes[0]) || (y >= box_hunks_sizes[1]) || (z >= box_hunks_sizes[2]) || (bitmask == 0))
        {
            return (0);
        }
        else if ((box_fragments_power[x, y, z] & bitmask) != bitmask) return (0); else return (1);
    }

    void makeBitMask()
    {
        for (int x = 0; x < 2; ++x)
        {
            for (int y = 0; y < 2; ++y)
            {
                for (int z = 0; z < 2; ++z)
                {
                    int bit = 0;
                    if (x == 0) bit |= 1; else bit |= 2;
                    if (y == 0) bit |= 4; else bit |= 8;
                    if (z == 0) bit |= 16; else bit |= 32;
                    dispBitMask[x, y, z] = bit;
                }
            }
        }
    }


    int localDisplacementIndicator(int xa, int ya, int za, int current_bitmask, int over_bitmask)
    { // ,bool layer
        int flag = localDisplacementIndicatorP(xa, ya, za, current_bitmask);
        if (over_bitmask != 0) flag |= localDisplacementIndicatorP(xa, ya, za, over_bitmask);
        return (flag);
    }

    int localDisplacementIndicatorP(int xa, int ya, int za, int current_bitmask)
    { // ,bool layer
        //                                  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
        int[] splitModuleX = new int[16] { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 8, 0, 1, 8, 0 }; //8 = minus, 1 = plus
        int[] splitModuleY = new int[16] { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 8, 8, 0 };
        int splitModule = 0;
        //=================== ignore edge damage ===========================
        int xb = xa, yb = ya, zb = za;
        if (xa > 0) xb = xa - 1;
        if (ya > 0) yb = ya - 1;
        if (za > 0) zb = za - 1;
        //============ bitmask ===========
        int bitmask = current_bitmask;
        //bitmask = bitmask & 1;
        int maska = 0; int maskb = 0; int maskc = 0;
        int ma = 0; int mb = 0; int mc = 0;
        if (boxMask(xa, ya, za, bitmask & dispBitMask[1, 1, 1]) > 0) { maska |= 8; maskb |= 8; maskc |= 8; }
        if (boxMask(xa, ya, zb, bitmask & dispBitMask[1, 1, 0]) > 0) ma |= 8; // os XY
        if (boxMask(xa, yb, za, bitmask & dispBitMask[1, 0, 1]) > 0) mb |= 8; // XZ
        if (boxMask(xb, ya, za, bitmask & dispBitMask[0, 1, 1]) > 0) mc |= 8; // YZ
        if (xa > 0)
        {
            if (boxMask(xb, ya, za, bitmask & dispBitMask[0, 1, 1]) > 0) maska |= 4;
            if (boxMask(xb, ya, za, bitmask & dispBitMask[0, 1, 1]) > 0) maskb |= 4;
            if (boxMask(xb, ya, zb, bitmask & dispBitMask[0, 1, 0]) > 0) ma |= 4;
            if (boxMask(xb, yb, za, bitmask & dispBitMask[0, 0, 1]) > 0) mb |= 4;
            if (ya > 0)
            {
                if (boxMask(xb, yb, za, bitmask & dispBitMask[0, 0, 1]) > 0) maska |= 1;
                if (boxMask(xb, yb, zb, bitmask & dispBitMask[0, 0, 0]) > 0) ma |= 1;
            }
            if (za > 0)
            {
                if (boxMask(xb, ya, zb, bitmask & dispBitMask[0, 1, 0]) > 0) maskb |= 1;
                if (boxMask(xb, yb, zb, bitmask & dispBitMask[0, 0, 0]) > 0) mb |= 1;
            }
        }
        //==========
        if (ya > 0)
        {
            if (boxMask(xa, yb, za, bitmask & dispBitMask[1, 0, 1]) > 0) maska |= 2;
            if (boxMask(xa, yb, za, bitmask & dispBitMask[1, 0, 1]) > 0) maskc |= 4;
            if (boxMask(xa, yb, zb, bitmask & dispBitMask[1, 0, 0]) > 0) ma |= 2;
            if (boxMask(xb, yb, za, bitmask & dispBitMask[0, 0, 1]) > 0) mc |= 4;
            if (za > 0)
            {
                if (boxMask(xa, yb, zb, bitmask & dispBitMask[1, 0, 0]) > 0) maskc |= 1;
                if (boxMask(xb, yb, zb, bitmask & dispBitMask[0, 0, 0]) > 0) mc |= 1;
            }
        }
        //========== if z>0 ========
        if (za > 0)
        {
            if (boxMask(xa, ya, zb, bitmask & dispBitMask[1, 1, 0]) > 0) maskb |= 2;
            if (boxMask(xa, ya, zb, bitmask & dispBitMask[1, 1, 0]) > 0) maskc |= 2;
            if (boxMask(xa, yb, zb, bitmask & dispBitMask[1, 0, 0]) > 0) mb |= 2;
            if (boxMask(xb, ya, zb, bitmask & dispBitMask[0, 1, 0]) > 0) mc |= 2;
        }
        //mask of local shifts 1= +X 2= +Y 4= +Z 8 = -X 16 = -Y 32 = -Z
        splitModule |= splitModuleX[maska]; // X / multipliers denote the axes x=1 y-2 z=4
        splitModule |= splitModuleY[maska] * 2; // Y
        splitModule |= splitModuleX[maskb]; // X 
        splitModule |= splitModuleY[maskb] * 4; // Z
        splitModule |= splitModuleX[maskc] * 2; // Y
        splitModule |= splitModuleY[maskc] * 4; // Z
        splitModule |= splitModuleX[ma]; // X / multipliers denote the axes x=1 y-2 z=4
        splitModule |= splitModuleY[ma] * 2; // Y
        splitModule |= splitModuleX[mb]; // X 
        splitModule |= splitModuleY[mb] * 4; // Z
        splitModule |= splitModuleX[mc] * 2; // Y
        splitModule |= splitModuleY[mc] * 4; // Z
        return (splitModule);
    }

    bool vertexOuterDisplacementV(int corner_sr, int x, int y, int z, int current_bitmask, int[] pointOffset, bool flipDepth, int xo, int yo, int zo)
    {
        int depth; if (flipDepth) depth = -pointOffset[13]; else depth = 0;//-pointOffset [12];
        //============================
        int[] dirX = new int[4] { -1, 1, -1, 1 };
        int[] dirY = new int[4] { -1, -1, 1, 1 };
        int xa = x + pointOffset[0] * dirX[corner_sr] + pointOffset[9] * depth; int ya = y + pointOffset[1] * dirX[corner_sr] + pointOffset[10] * depth; int za = z + pointOffset[2] * dirX[corner_sr] + pointOffset[11] * depth;
        // ====================================================================================
        int flag; if (flipDepth) flag = 1; else flag = 2;
        flag = (flag * (pointOffset[9] + pointOffset[10] * 4 + pointOffset[11] * 16) + 64) & current_bitmask;
        if ((testPoint[0] == xo) && (testPoint[1] == yo) && (testPoint[2] == zo)) Debug.Log(" test displacementV(empty): " + xo + " " + yo + " " + zo + "(" + x + " " + y + " " + z + ") corner: " + corner_sr + " posa: " + xa + " " + ya + " " + za + " depth: " + depth + " flag: " + flag + " box " + safe_box_fragments(xa, ya, za));
        if ((safe_box_fragments(xa, ya, za) & flag) != flag)
        { // current_bitmask
            //=================================================================================
            int xb = xa + pointOffset[3] * dirY[corner_sr]; int yb = ya + pointOffset[4] * dirY[corner_sr]; int zb = za + pointOffset[5] * dirY[corner_sr];
            if ((testPoint[0] == xo) && (testPoint[1] == yo) && (testPoint[2] == zo)) Debug.Log(" test displacementV(full): " + xo + " " + yo + " " + zo + "(" + x + " " + y + " " + z + ") corner: " + corner_sr + " posb: " + xb + " " + yb + " " + zb + " depth: " + depth + " flag: " + flag + " box " + safe_box_fragments(xb, yb, zb));
            if ((flag != 0) && ((safe_box_fragments(xb, yb, zb) & flag) == flag)) return (true);
        }
        return (false);
    }

    bool vertexOuterDisplacementH(int corner_sr, int x, int y, int z, int current_bitmask, int[] pointOffset, bool flipDepth, int xo, int yo, int zo)
    {
        int depth; if (flipDepth) depth = -pointOffset[13]; else depth = 0; //-pointOffset [12]; 
        //============================
        int[] dirX = new int[4] { -1, 1, -1, 1 };
        int[] dirY = new int[4] { -1, -1, 1, 1 };
        int xa = x + pointOffset[3] * dirY[corner_sr] + pointOffset[9] * depth; int ya = y + pointOffset[4] * dirY[corner_sr] + pointOffset[10] * depth; int za = z + pointOffset[5] * dirY[corner_sr] + pointOffset[11] * depth;
        // ====================================================================================
        int flag; if (flipDepth) flag = 1; else flag = 2;
        flag = (flag * (pointOffset[9] + pointOffset[10] * 4 + pointOffset[11] * 16) + 64) & current_bitmask;
        //if ((testPoint [0] == xo) && (testPoint [1] == yo) && (testPoint [2] == zo)) Debug.Log (" test displacementH(empty): "+xo+" "+yo+" "+zo+"("+x+" "+y+" "+z+") corner: "+corner_sr+" posa: " +xa+" "+ya+" "+za+" depth: " + depth + " flag: " + flag + " box "+safe_box_fragments (xa, ya, za));
        if ((safe_box_fragments(xa, ya, za) & flag) != flag)
        { // current_bitmask
            //=================================================================================
            int xb = xa + pointOffset[0] * dirX[corner_sr]; int yb = ya + pointOffset[1] * dirX[corner_sr]; int zb = za + pointOffset[2] * dirX[corner_sr];
            //if ((testPoint [0] == xo) && (testPoint [1] == yo) && (testPoint [2] == zo)) Debug.Log (" test displacementH(full): "+xo+" "+yo+" "+zo+"("+x+" "+y+" "+z+") corner: "+corner_sr+" posb: " +xb+" "+yb+" "+zb+" depth: " + depth + " flag: " + flag + " box "+safe_box_fragments (xb, yb, zb));
            if ((flag != 0) && ((safe_box_fragments(xb, yb, zb) & flag) == flag)) return (true);
        }
        return (false);
    }

    int vertexDisplacementIndicatorChip(int corner, int x, int y, int z, int current_bitmask, int[] pointOffset, bool flipDepth, int[] pointMode, int xo, int yo, int zo, int face)
    { // ,int xo,int yo,int zo
        int[] dirX = new int[4] { -1, 1, -1, 1 };
        int[] dirY = new int[4] { -1, -1, 1, 1 };
        int[] flagX = new int[4] { 2, 1, 2, 1 };
        int[] flagY = new int[4] { 2, 2, 1, 1 };
        int[] modFlagX = new int[4] { 1, 8, 1, 8 };
        int[] modFlagY = new int[4] { 1, 1, 8, 8 };
        int modFlagZ, xa = x, ya = y, za = z, xb = x, yb = y, zb = z;
        int splitModule = 0;
        int depth; int low_depth; if (flipDepth) { depth = 0; low_depth = -1; modFlagZ = 1; } else { depth = -1; low_depth = 0; modFlagZ = 8; }
        int xc = x + pointOffset[9] * depth; int yc = y + pointOffset[10] * depth; int zc = z + pointOffset[11] * depth; // above
        int xd = x + pointOffset[9] * low_depth; int yd = y + pointOffset[10] * low_depth; int zd = z + pointOffset[11] * low_depth;
        xa = xd + pointOffset[0] * dirX[corner]; ya = yd + pointOffset[1] * dirX[corner]; za = zd + pointOffset[2] * dirX[corner];
        xb = xd + pointOffset[3] * dirY[corner]; yb = yd + pointOffset[4] * dirY[corner]; zb = zd + pointOffset[5] * dirY[corner];
        int flagc = 64;
        int smx = modFlagX[corner] * (pointOffset[0] + pointOffset[1] * 2 + pointOffset[2] * 4);
        int smy = modFlagY[corner] * (pointOffset[3] + pointOffset[4] * 2 + pointOffset[5] * 4);
        int smh = modFlagZ * (pointOffset[9] + pointOffset[10] * 2 + pointOffset[11] * 4);
        //===================================================================
        if ((safe_box_fragments(xa, ya, za) & flagc) == flagc)
        {
            if ((safe_box_fragments(xb, yb, zb) & flagc) == flagc)
            {
                if ((safe_box_fragments(xc, yc, zc) & flagc) == flagc) { splitModule |= smx | smy | smh; pointMode[corner] = 7; } // corner
                else { splitModule |= smx | smy | disableDepth; pointMode[corner] = 3; }
            }
            else if ((safe_box_fragments(xc, yc, zc) & flagc) == flagc) { splitModule |= smx + smh; pointMode[corner] = 2; }
        }
        else if ((safe_box_fragments(xb, yb, zb) & flagc) == flagc)
        {
            if ((safe_box_fragments(xc, yc, zc) & flagc) == flagc) { splitModule |= smy + smh; pointMode[corner] = 1; }
        }
        return (splitModule);
    }

    int vertexDisplacementIndicator(int corner, int x, int y, int z, int current_bitmask, int[] pointOffset, bool flipDepth, int[] pointMode, int xo, int yo, int zo)
    {
        int[] dirX = new int[4] { -1, 1, -1, 1 };
        int[] dirY = new int[4] { -1, -1, 1, 1 };
        int[] flagX = new int[4] { 2, 1, 2, 1 };
        int[] flagY = new int[4] { 2, 2, 1, 1 };
        int[] modFlagX = new int[4] { 1, 8, 1, 8 };
        int[] modFlagY = new int[4] { 1, 1, 8, 8 };
        int modFlagZ, xa = x, ya = y, za = z, xb = x, yb = y, zb = z, xd = x, yd = y, zd = z;
        int splitModule = 0; //                                                -1??
        int depth; int low_depth; if (flipDepth) { depth = 0; low_depth = -1; modFlagZ = 1; } else { depth = -1; low_depth = 0; modFlagZ = 8; }
        //if ((testPoint [0] == xo) && (testPoint [1] == yo) && (testPoint [2] == zo)) {
        //    Debug.Log ("================================= vertexDisplacementIndicator =================================================");
        //    Debug.Log ("corner: " + corner + " vertex: " + x + " " + y + " " + z + " depth: " + depth + " modFlagZ: "+modFlagZ);
        //}
        //===========================================================================================================================
        int xc = x + pointOffset[9] * depth; int yc = y + pointOffset[10] * depth; int zc = z + pointOffset[11] * depth; // above
        xa = xc + pointOffset[0] * dirX[corner]; ya = yc + pointOffset[1] * dirX[corner]; za = zc + pointOffset[2] * dirX[corner];
        xb = xc + pointOffset[3] * dirY[corner]; yb = yc + pointOffset[4] * dirY[corner]; zb = zc + pointOffset[5] * dirY[corner];
        xd = xa + pointOffset[3] * dirY[corner]; yd = ya + pointOffset[4] * dirY[corner]; zd = za + pointOffset[5] * dirY[corner];
        int xl = x + pointOffset[9] * low_depth; int yl = y + pointOffset[10] * low_depth; int zl = z + pointOffset[11] * low_depth;
        int xh = xl + pointOffset[0] * dirX[corner]; int yh = yl + pointOffset[1] * dirX[corner]; int zh = zl + pointOffset[2] * dirX[corner];
        int xf = xl + pointOffset[3] * dirY[corner]; int yf = yl + pointOffset[4] * dirY[corner]; int zf = zl + pointOffset[5] * dirY[corner];
        int xe = xf + pointOffset[0] * dirX[corner]; int ye = yf + pointOffset[1] * dirX[corner]; int ze = zf + pointOffset[2] * dirX[corner];
        int flaga = (flagY[corner] * (pointOffset[3] + pointOffset[4] * 4 + pointOffset[5] * 16) + 64) & current_bitmask;
        int flagb = (flagX[corner] * (pointOffset[0] + pointOffset[1] * 4 + pointOffset[2] * 16) + 64) & current_bitmask;
        int flagc = 64;
        int kodx = 0; int kody = 0; int kodz = 0;
        int smx = modFlagX[corner] * (pointOffset[0] + pointOffset[1] * 2 + pointOffset[2] * 4);
        int smy = modFlagY[corner] * (pointOffset[3] + pointOffset[4] * 2 + pointOffset[5] * 4);
        int smh = modFlagZ * (pointOffset[9] + pointOffset[10] * 2 + pointOffset[11] * 4);
        if ((flaga != 0) && (flagb != 0))
        {
            if ((safe_box_fragments(xa, ya, za) & flaga) == flaga)
            {
                if ((safe_box_fragments(xb, yb, zb) & flagb) == flagb)
                {
                    splitModule |= smx + smy + smh; pointMode[corner] = 7;// corner
                }
                else
                {
                    if ((safe_box_fragments(xe, ye, ze) & flagc) == flagc) kodx |= 1;
                    if ((safe_box_fragments(xf, yf, zf) & flagc) == flagc) kodx |= 2;
                    if ((safe_box_fragments(xd, yd, zd) & flagc) == flagc) kodx |= 4;
                    if ((safe_box_fragments(xh, yh, zh) & flagc) == flagc) kodx |= 8;
                    if ((kodx & 6) == 6) { splitModule |= smx + smh; pointMode[corner] = 2; } // pipe
                    if ((kodx == 3) || (kodx == 5) || (kodx == 0) || (kodx == 8) || (kodx == 9) || (kodx == 10) || (kodx == 11) || (kodx == 12) || (kodx == 13)) { splitModule |= smx + smh; pointMode[corner] = 2; } // slup
                }
            }
            else if ((safe_box_fragments(xb, yb, zb) & flagb) == flagb)
            {
                if ((safe_box_fragments(xe, ye, ze) & flagc) == flagc) kody |= 1;
                if ((safe_box_fragments(xh, yh, zh) & flagc) == flagc) kody |= 2;
                if ((safe_box_fragments(xd, yd, zd) & flagc) == flagc) kody |= 4;
                if ((safe_box_fragments(xf, yf, zf) & flagc) == flagc) kody |= 8;
                if ((kody & 6) == 6) { splitModule |= smy + smh; pointMode[corner] = 1; } // pipe
                if ((kody == 3) || (kody == 5) || (kody == 0) || (kody == 8) || (kody == 9) || (kody == 10) || (kody == 11) || (kody == 12) || (kody == 13)) { splitModule |= smy + smh; pointMode[corner] = 1; } // slup
                //splitModule |= smy + smh;
            }
            else
            {
                int flagd; if (flipDepth) flagd = 1; else flagd = 2;
                flagd = (flagd * (pointOffset[9] + pointOffset[10] * 4 + pointOffset[11] * 16));
                if ((flagd & current_bitmask) == 0)
                {
                    if ((safe_box_fragments(xh, yh, zh) & safe_box_fragments(xf, yf, zf) & flagd) == flagd) { splitModule |= smx + smy + disableDepth + 4096; pointMode[corner] = 3; } // shallow crushing on outer layer
                }
                flagd = (flagd + flagc) & current_bitmask;
                //==================================
                if ((valid_box_fragments(xh, yh, zh) & flagd) == flagd) kodz |= 1;
                if ((valid_box_fragments(xf, yf, zf) & flagd) == flagd) kodz |= 2;
                if ((safe_box_fragments(xe, ye, ze) & flagd) == flagd) kodz |= 4; // valid_box_fragments
                if ((safe_box_fragments(xd, yd, zd) & flagc) == flagc) kodz |= 8;
                if (kodz == 11) { splitModule |= smx | smy | disableDepth | 1024; pointMode[corner] = 3; } //smx + smy + smh; // slup 1024 mean code 3 instead code 7
                if ((kodz >= 13) && (kodz <= 15)) { splitModule |= smx | smy | disableDepth | 1024 | smh; pointMode[corner] = 3; }// slup
                if (kodz == 4) { splitModule |= smx | smy | disableDepth; pointMode[corner] = 3; }
                if (kodz == 5) { splitModule |= smx; pointMode[corner] = 2; }
                if (kodz == 6) { splitModule |= smy; pointMode[corner] = 1; }
            }
        }
        //=====================================
        return (splitModule);
    }


    /*****************************************************************************
     * 
     * 
     *              Medium detail Mesh
     * 
     * ***************************************************************************/

    void MakeMeshHighDetails()
    {
        //============================================
        int max_Line_len = box_hunks_sizes[0];
        if (max_Line_len < box_hunks_sizes[1]) max_Line_len = box_hunks_sizes[1];
        if (max_Line_len < box_hunks_sizes[2]) max_Line_len = box_hunks_sizes[2];
        int[,] temp_index_last_line = new int[max_Line_len + 1, TABVERTSIZE];
        int[,] last_index_last_line = new int[max_Line_len + 1, TABVERTSIZE];
        int[,] temp2_index_last_line = new int[max_Line_len + 1, TABVERTSIZE];
        int[,] last2_index_last_line = new int[max_Line_len + 1, TABVERTSIZE];
        //==========================================================
        int[] last_dam_last_line = new int[max_Line_len + 1];
        int[] temp_dam_last_line = new int[max_Line_len + 1];
        int[] last2_dam_last_line = new int[max_Line_len + 1];
        int[] temp2_dam_last_line = new int[max_Line_len + 1];

        /************************************************************************
         * 
         *      1=X big   2=X small   4=Y big  8=Y small  16=Z duz 32=Z small
         * 
         * **********************************************************************/
        //Debug.Log ("=========================== Layer out YZ ===========================");
        //float svmin = 0.001f;
        int clr = 0;
        int don = 0;
        int clr2 = 0;
        int don2 = 0;
        if (face_exist[0] > 1)
        {
            clr = 0;
            clr2 = 0;
            int x = box_hunks_sizes[0] - 1;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] & 1) != 0)
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[2]);
                        if ((testFace & 1) != 0) outer_triangles_len = computeFaceVertex(x + 1, y, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, box_fragments_power[x, y, z] & damageMask, pointMasks0, indOut0, false, z, pointOffset0, outer_triangles, outer_triangles_len, true);
                        clearIndex(temp2_index_last_line, z);
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        //Vector3 mSf = cornerVector (x, y, z);
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[2]); // indInn0
                        if ((testFace & 1) != 0) computeFaceVertexInn(x + 1, y, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, box_fragments_power[x, y, z] & damageMask, 0, pointMasks0L, indInn0, false, z, pointOffset0, true);// false
                        clearIndex(temp_index_last_line, z);
                    }
                    else clearIndex(temp_index_last_line, temp2_index_last_line, z);
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[2]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[2]); else clr2 = 0;
            }
        }
        //==============================================================================================
        if (face_exist[1] > 1)
        { //x=-1
            clr = 0;
            clr2 = 0;
            int x = 0;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    //if(( y<3 )&&( z==0)) {
                    if ((box_fragments_power[x, y, z] & 2) != 0)
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[2]);
                        if ((testFace & 2) != 0) outer_triangles_len = computeFaceVertex(x, y, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, box_fragments_power[x, y, z] & damageMask, pointMasks1, indOut1, true, z, pointOffset0, outer_triangles, outer_triangles_len, false);
                        clearIndex(temp2_index_last_line, z);
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[2]);
                        if ((testFace & 2) != 0) computeFaceVertexInn(x, y, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, box_fragments_power[x, y, z] & damageMask, 0, pointMasks1L, indInn1, true, z, pointOffset0, false);
                        clearIndex(temp_index_last_line, z);
                    }
                    else clearIndex(temp_index_last_line, temp2_index_last_line, z);
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[2]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[2]); else clr2 = 0;
            }
        }
        //==============================================================================================
        if (face_exist[2] > 1)
        { //y=1
            clr = 0;
            clr2 = 0;
            int y = box_hunks_sizes[1] - 1;
            for (int x = 0; x < box_hunks_sizes[0]; ++x)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] & 4) != 0)
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[2]);
                        if ((testFace & 4) != 0) outer_triangles_len = computeFaceVertex(x, y + 1, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, box_fragments_power[x, y, z] & damageMask, pointMasks2, indOut2, true, z, pointOffset1, outer_triangles, outer_triangles_len, true);
                        clearIndex(temp2_index_last_line, z);
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[2]);
                        if ((testFace & 4) != 0) computeFaceVertexInn(x, y + 1, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, box_fragments_power[x, y, z] & damageMask, 0, pointMasks2L, indInn2, true, z, pointOffset1, true); // false
                        clearIndex(temp_index_last_line, z);
                    }
                    else clearIndex(temp_index_last_line, temp2_index_last_line, z); // clear
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[2]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[2]); else clr2 = 0;
            }
        }
        //==============================face -Y ==========================================
        if (face_exist[3] > 1)
        { //y=-1
            clr = 0;
            clr2 = 0;
            int y = 0;
            for (int x = 0; x < box_hunks_sizes[0]; ++x)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] & 8) != 0)
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[2]);
                        if ((testFace & 8) != 0) outer_triangles_len = computeFaceVertex(x, y, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, box_fragments_power[x, y, z] & damageMask, pointMasks3, indOut3, false, z, pointOffset1, outer_triangles, outer_triangles_len, false);
                        clearIndex(temp2_index_last_line, z);
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[2]);
                        if ((testFace & 8) != 0) computeFaceVertexInn(x, y, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, box_fragments_power[x, y, z] & damageMask, 0, pointMasks3L, indInn3, false, z, pointOffset1, false);
                        clearIndex(temp_index_last_line, z);
                    }
                    else clearIndex(temp_index_last_line, temp2_index_last_line, z); // clear
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[2]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[2]); else clr2 = 0;
            }
        }
        //Debug.Log ("=========================== Layer out YX ===========================");
        //==============================face +Z ==========================================
        if (face_exist[4] > 1)
        { //z=1
            clr = 0;
            clr2 = 0;
            int z = box_hunks_sizes[2] - 1;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if ((box_fragments_power[x, y, z] & 16) != 0)
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[0]);
                        if ((testFace & 16) != 0) outer_triangles_len = computeFaceVertex(x, y, z + 1, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, box_fragments_power[x, y, z] & damageMask, pointMasks4, indOut4, true, x, pointOffset2, outer_triangles, outer_triangles_len, true);
                        clearIndex(temp2_index_last_line, x);
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[0]);
                        if ((testFace & 16) != 0) computeFaceVertexInn(x, y, z + 1, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, box_fragments_power[x, y, z] & damageMask, 0, pointMasks4L, indInn4, true, x, pointOffset2, true); // false
                        clearIndex(temp_index_last_line, x);
                    }
                    else clearIndex(temp_index_last_line, temp2_index_last_line, x);
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[0]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[0]); else clr2 = 0;
            }
        }
        //==============================face +Z ==========================================
        if (face_exist[5] > 1)
        { //z=-1
            clr = 0;
            clr2 = 0;
            int z = 0;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if ((box_fragments_power[x, y, z] & 32) != 0)
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[0]);
                        if ((testFace & 32) != 0) outer_triangles_len = computeFaceVertex(x, y, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, box_fragments_power[x, y, z] & damageMask, pointMasks5, indOut5, false, x, pointOffset2, outer_triangles, outer_triangles_len, false);
                        clearIndex(temp2_index_last_line, x);
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[0]);
                        if ((testFace & 32) != 0) computeFaceVertexInn(x, y, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, box_fragments_power[x, y, z] & damageMask, 0, pointMasks5L, indInn5, false, x, pointOffset2, false);
                        clearIndex(temp_index_last_line, x);
                    }
                    else clearIndex(temp_index_last_line, temp2_index_last_line, x);
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[0]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[0]); else clr2 = 0;
            }
        }
        /******************************************************************************************
         * 
         *                                Inner faces
         * 
         * *****************************************************************************************/
        //Debug.Log ("=========================== Layer inn YZ ===========================");
        clr = 0;
        don = 0;
        clr2 = 0;
        don2 = 0;
        //====================== make inner layer 0,1
        for (int x = 1; x < box_hunks_sizes[0]; ++x)
        { //x
            clr = 0;
            clr2 = 0;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if (((box_fragments_power[x, y, z] & damageMask1) != damageMask1) && ((box_fragments_power[x, y, z] & damageMask1) != (box_fragments_power[x - 1, y, z] & damageMask1)) && (box_fragments_power[x - 1, y, z] > 0))
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[2]);
                        int dam = box_fragments_power[x - 1, y, z] & damageMask; int dam2 = box_fragments_power[x, y, z] & damageMask;
                        if ((testFace & 1) != 0) computeFaceVertexInn(x, y, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, dam, dam2, pointMasks0L, indInn0, false, z, pointOffset0Inn, true);
                    }
                    else clearIndex(temp_index_last_line, z);
                    //==================
                    if ((box_fragments_power[x, y, z] > 0) && ((box_fragments_power[x - 1, y, z] & damageMask1) != damageMask1) && ((box_fragments_power[x - 1, y, z] & damageMask1) != (box_fragments_power[x, y, z] & damageMask1)))
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[2]);
                        int dam = box_fragments_power[x, y, z] & damageMask; int dam2 = box_fragments_power[x - 1, y, z] & damageMask;
                        if ((testFace & 2) != 0) computeFaceVertexInn(x, y, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, dam, dam2, pointMasks1L, indInn1, true, z, pointOffset0Inn, false);
                    }
                    else clearIndex(temp2_index_last_line, z);
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[2]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[2]); else clr2 = 0;
            }
        }
        //Debug.Log ("=========================== Layer inn XZ ===========================");
        //================ 2,3 =======================
        clr = 0;
        don = 0;
        clr2 = 0;
        don2 = 0;
        for (int y = 1; y < box_hunks_sizes[1]; ++y)
        { //y
            clr = 0;
            clr2 = 0;
            //=======================================================
            for (int x = 0; x < box_hunks_sizes[0]; ++x)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if (((box_fragments_power[x, y, z] & damageMask2) != damageMask2) && ((box_fragments_power[x, y, z] & damageMask2) != (box_fragments_power[x, y - 1, z] & damageMask2)) && (box_fragments_power[x, y - 1, z] > 0))
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[2]);
                        int dam = box_fragments_power[x, y - 1, z] & damageMask; int dam2 = box_fragments_power[x, y, z] & damageMask;
                        if ((testFace & 4) != 0) computeFaceVertexInn(x, y, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, dam, dam2, pointMasks2L, indInn2, true, z, pointOffset1Inn, true);
                    }
                    else clearIndex(temp_index_last_line, z);
                    //==================
                    if ((box_fragments_power[x, y, z] > 0) && ((box_fragments_power[x, y - 1, z] & damageMask2) != damageMask2) && ((box_fragments_power[x, y - 1, z] & damageMask2) != (box_fragments_power[x, y, z] & damageMask2)))
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[2]);
                        int dam = box_fragments_power[x, y, z] & damageMask; int dam2 = box_fragments_power[x, y - 1, z] & damageMask;
                        if ((testFace & 8) != 0) computeFaceVertexInn(x, y, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, dam, dam2, pointMasks3L, indInn3, false, z, pointOffset1Inn, false);
                    }
                    else clearIndex(temp2_index_last_line, z);
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[2]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[2]); else clr2 = 0;
            }
        }
        //Debug.Log ("=========================== Layer inn YX 4,5 ===========================");
        //================ 4,5 =======================
        clr = 0;
        don = 0;
        clr2 = 0;
        don2 = 0;
        for (int z = 1; z < box_hunks_sizes[2]; ++z)
        { //z
            clr = 0;
            clr2 = 0;
            //=======================================================
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                clearTempLine(temp_index_last_line, temp2_index_last_line);
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if (((box_fragments_power[x, y, z] & damageMask3) != damageMask3) && ((box_fragments_power[x, y, z] & damageMask3) != (box_fragments_power[x, y, z - 1] & damageMask3)) && (box_fragments_power[x, y, z - 1] > 0))
                    {
                        don = 1;
                        if (clr == 0) clr = clearLastIndexLine(last_index_last_line, box_hunks_sizes[0]);
                        int dam = box_fragments_power[x, y, z - 1] & damageMask; int dam2 = box_fragments_power[x, y, z] & damageMask;
                        if ((testFace & 16) != 0) computeFaceVertexInn(x, y, z, last_index_last_line, temp_index_last_line, last_dam_last_line, temp_dam_last_line, dam, dam2, pointMasks4L, indInn4, true, x, pointOffset2Inn, true);
                    }
                    else clearIndex(temp_index_last_line, x);
                    if ((box_fragments_power[x, y, z] > 0) && ((box_fragments_power[x, y, z - 1] & damageMask3) != damageMask3) && ((box_fragments_power[x, y, z - 1] & damageMask3) != (box_fragments_power[x, y, z] & damageMask3)))
                    {
                        don2 = 1;
                        if (clr2 == 0) clr2 = clearLastIndexLine(last2_index_last_line, box_hunks_sizes[0]);
                        int dam = box_fragments_power[x, y, z] & damageMask; int dam2 = box_fragments_power[x, y, z - 1] & damageMask;
                        if ((testFace & 32) != 0) computeFaceVertexInn(x, y, z, last2_index_last_line, temp2_index_last_line, last2_dam_last_line, temp2_dam_last_line, dam, dam2, pointMasks5L, indInn5, false, x, pointOffset2Inn, false);
                    }
                    else clearIndex(temp2_index_last_line, x);
                }
                if (don > 0) copyTempLast(temp_index_last_line, last_index_last_line, temp_dam_last_line, last_dam_last_line, box_hunks_sizes[0]); else clr = 0;
                if (don2 > 0) copyTempLast(temp2_index_last_line, last2_index_last_line, temp2_dam_last_line, last2_dam_last_line, box_hunks_sizes[0]); else clr2 = 0;
            }
        }
    }

    int computeFaceVertex(int x, int y, int z, int[,] lastIndex, int[,] tempIndex, int[] lastDam, int[] tempDam, int dam, int[] pointMasks, int[] indOut, bool flip, int ind, int[] pointOffset, int[] triangles, int trianglesOffset, bool flipDepth)
    {
        Vector2[] cmpDepth = new Vector2[4] { new Vector2(1f, 1f), new Vector2(1f, 1f), new Vector2(1f, 1f), new Vector2(1f, 1f) };
        int[] indSv = new int[4] { 0, 0, 0, 0 };
        int[] pointMode = new int[4] { 0, 0, 0, 0 };
        int[] pointMode2 = new int[4] { 0, 0, 0, 0 };
        int[] quadIndicesA = new int[4];
        int[] quadIndicesBp = new int[4];
        int[] quadIndicesBm = new int[4];
        int[] quadIndicesCp = new int[4];
        int[] quadIndicesCm = new int[4];
        int[] quadIndicesD = new int[4] { -1, -1, -1, -1 };
        fillQuadData(quadIndicesA, quadIndicesBp, quadIndicesBm, quadIndicesCp, quadIndicesCm, lastIndex, tempIndex, ind);
        displayFaceInfo = 0;
        //============================================================ point 0 =============================================
        Vector3 mSf = cornerVector(x, y, z);
        Vector3 hori_mSf = set_hori_mSf(mSf, pointOffset); // corner horizontal
        Vector3 vert_mSf = set_vert_mSf(mSf, pointOffset); // corner vertical
        indSv[0] = vertexDisplacementIndicator(0, x, y, z, dam, pointOffset, flipDepth, pointMode2, x, y, z);
        int footPrint = layerTest(x, y, z, dam, pointOffset);
        if ((lastIndex[ind + 0, 0] < 0) || (lastDam[ind + 0] != footPrint))
        {
            lastIndex[ind + 0, 0] = quadIndicesA[0] = makeVertexOutSV(x, y, z, indOut, layerXY(x, y, z, dam, pointOffset));
            lastDam[ind + 0] = footPrint;
        }
        if ((indSv[0] & pointMasks[1]) != 0) { lastIndex[ind + 0, 1] = quadIndicesBp[0] = makeVertexOutSV(x, y, z, indOut, hori_mSf); pointMode[0] |= 2; }
        if ((indSv[0] & pointMasks[2]) != 0) { lastIndex[ind + 0, 2] = quadIndicesCp[0] = makeVertexOutSV(x, y, z, indOut, vert_mSf); pointMode[0] |= 1; }
        if ((pointMode[0] & 3) == 0)
        {
            if (vertexOuterDisplacementH(0, x, y, z, dam, pointOffset, flipDepth, x, y, z)) { lastIndex[ind + 0, 1] = quadIndicesBp[0] = makeVertexOutSV(x, y, z, indOut, hori_mSf); pointMode[0] |= 2; }
            if (vertexOuterDisplacementV(0, x, y, z, dam, pointOffset, flipDepth, x, y, z)) { lastIndex[ind + 0, 2] = quadIndicesCp[0] = makeVertexOutSV(x, y, z, indOut, vert_mSf); pointMode[0] |= 1; }
        }
        //============================================================ point 1 =============================================
        int xa = x + pointOffset[0]; int ya = y + pointOffset[1]; int za = z + pointOffset[2];
        mSf = cornerVector(xa, ya, za);
        hori_mSf = set_hori_mSf(mSf, pointOffset); // corner horizontal
        vert_mSf = set_vert_mSf(mSf, pointOffset); // corner vertical
        indSv[1] = vertexDisplacementIndicator(1, x, y, z, dam, pointOffset, flipDepth, pointMode2, xa, ya, za);
        footPrint = layerTest(xa, ya, za, dam, pointOffset);
        if ((lastIndex[ind + 1, 0] < 0) || (lastDam[ind + 1] != footPrint))
        {
            lastIndex[ind + 1, 0] = quadIndicesA[1] = makeVertexOutSV(xa, ya, za, indOut, layerXY(xa, ya, za, dam, pointOffset));
            lastDam[ind + 1] = footPrint;
        }
        if ((indSv[1] & pointMasks[3]) != 0) { lastIndex[ind + 1, 3] = quadIndicesBm[1] = makeVertexOutSV(xa, ya, za, indOut, -hori_mSf); pointMode[1] |= 2; }
        if ((indSv[1] & pointMasks[2]) != 0) { lastIndex[ind + 1, 2] = quadIndicesCp[1] = makeVertexOutSV(xa, ya, za, indOut, vert_mSf); pointMode[1] |= 1; }
        if ((pointMode[1] & 3) == 0)
        {
            if (vertexOuterDisplacementH(1, x, y, z, dam, pointOffset, flipDepth, xa, ya, za)) { lastIndex[ind + 1, 3] = quadIndicesBm[1] = makeVertexOutSV(xa, ya, za, indOut, -hori_mSf); pointMode[1] |= 2; }
            if (vertexOuterDisplacementV(1, x, y, z, dam, pointOffset, flipDepth, xa, ya, za)) { lastIndex[ind + 1, 2] = quadIndicesCp[1] = makeVertexOutSV(xa, ya, za, indOut, vert_mSf); pointMode[1] |= 1; }
        }
        //============================================================ point 2 =============================================
        int xb = x + pointOffset[3]; int yb = y + pointOffset[4]; int zb = z + pointOffset[5];
        mSf = cornerVector(xb, yb, zb);
        hori_mSf = set_hori_mSf(mSf, pointOffset); //corner horizontal
        vert_mSf = set_vert_mSf(mSf, pointOffset); //corner vertical
        indSv[2] = vertexDisplacementIndicator(2, x, y, z, dam, pointOffset, flipDepth, pointMode2, xb, yb, zb);
        footPrint = layerTest(xb, yb, zb, dam, pointOffset);
        if ((tempIndex[ind + 0, 0] < 0) || (tempDam[ind + 0] != footPrint))
        {
            tempIndex[ind + 0, 0] = quadIndicesA[2] = makeVertexOutSV(xb, yb, zb, indOut, layerXY(xb, yb, zb, dam, pointOffset));
            tempDam[ind + 0] = footPrint;
        }
        if ((indSv[2] & pointMasks[1]) != 0) { tempIndex[ind + 0, 1] = quadIndicesBp[2] = makeVertexOutSV(xb, yb, zb, indOut, hori_mSf); pointMode[2] |= 2; }
        if ((indSv[2] & pointMasks[4]) != 0) { tempIndex[ind + 0, 4] = quadIndicesCm[2] = makeVertexOutSV(xb, yb, zb, indOut, -vert_mSf); pointMode[2] |= 1; }
        if ((pointMode[2] & 3) == 0)
        {
            if (vertexOuterDisplacementH(2, x, y, z, dam, pointOffset, flipDepth, xb, yb, zb)) { tempIndex[ind + 0, 1] = quadIndicesBp[2] = makeVertexOutSV(xb, yb, zb, indOut, hori_mSf); pointMode[2] |= 2; }
            if (vertexOuterDisplacementV(2, x, y, z, dam, pointOffset, flipDepth, xb, yb, zb)) { tempIndex[ind + 0, 4] = quadIndicesCm[2] = makeVertexOutSV(xb, yb, zb, indOut, -vert_mSf); pointMode[2] |= 1; }
        }
        //============================================================ point 3 =============================================
        int xc = x + pointOffset[6]; int yc = y + pointOffset[7]; int zc = z + pointOffset[8];
        mSf = cornerVector(xc, yc, zc);
        hori_mSf = set_hori_mSf(mSf, pointOffset); //corner horizontal
        vert_mSf = set_vert_mSf(mSf, pointOffset); //corner vertical
        indSv[3] = vertexDisplacementIndicator(3, x, y, z, dam, pointOffset, flipDepth, pointMode2, xc, yc, zc);
        tempIndex[ind + 1, 0] = quadIndicesA[3] = makeVertexOutSV(xc, yc, zc, indOut, layerXY(xc, yc, zc, dam, pointOffset));
        tempDam[ind + 1] = layerTest(xc, yc, zc, dam, pointOffset);
        if ((indSv[3] & pointMasks[3]) != 0) { tempIndex[ind + 1, 3] = quadIndicesBm[3] = makeVertexOutSV(xc, yc, zc, indOut, -hori_mSf); pointMode[3] |= 2; }
        if ((indSv[3] & pointMasks[4]) != 0) { tempIndex[ind + 1, 4] = quadIndicesCm[3] = makeVertexOutSV(xc, yc, zc, indOut, -vert_mSf); pointMode[3] |= 1; }
        if ((pointMode[3] & 3) == 0)
        {
            if (vertexOuterDisplacementH(3, x, y, z, dam, pointOffset, flipDepth, xc, yc, zc)) { tempIndex[ind + 1, 3] = quadIndicesBm[3] = makeVertexOutSV(xc, yc, zc, indOut, -hori_mSf); pointMode[3] |= 2; }
            if (vertexOuterDisplacementV(3, x, y, z, dam, pointOffset, flipDepth, xc, yc, zc)) { tempIndex[ind + 1, 4] = quadIndicesCm[3] = makeVertexOutSV(xc, yc, zc, indOut, -vert_mSf); pointMode[3] |= 1; }
        }
        //============ handle faces =====================
        //if(displayFaceInfo==0) return(trianglesOffset);
        return (makeMultiFaceD(quadIndicesA, quadIndicesBp, quadIndicesBm, quadIndicesCp, quadIndicesCm, quadIndicesD, pointMode, cmpDepth, flip, triangles, trianglesOffset));
    }

    void computeFaceVertexInn(int x, int y, int z, int[,] lastIndex, int[,] tempIndex, int[] lastDam, int[] tempDam, int dam, int dam2, int[] pointMasks, int[] indInn, bool flip, int ind, int[] pointOffset, bool flipDepth)
    {
        Vector2[] cmpDepth = new Vector2[4] { new Vector2(1f, 1f), new Vector2(1f, 1f), new Vector2(1f, 1f), new Vector2(1f, 1f) };
        int[] pointMode = new int[4] { 0, 0, 0, 0 };
        int[] quadIndicesA = new int[4];// { last_index_last_line [z + 0, 0],last_index_last_line [z + 1, 0],temp_index_last_line [z + 0, 0],temp_index_last_line [z + 1, 0] };
        int[] quadIndicesBp = new int[4];
        int[] quadIndicesBm = new int[4];
        int[] quadIndicesCp = new int[4] { -4, -4, -4, -4 };
        int[] quadIndicesCm = new int[4] { -5, -5, -5, -5 };
        int[] quadIndicesD = new int[4] { -1, -1, -1, -1 };
        int[] footPrint = new int[1];
        int indSv;
        fillQuadData(quadIndicesA, quadIndicesBp, quadIndicesBm, quadIndicesCp, quadIndicesCm, lastIndex, tempIndex, ind);
        //============================================================ point 0 =============================================
        Vector3 mSf = cornerVector(x, y, z);
        Vector3 hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal
        Vector3 vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical
        int mask = pointMasks[1] | pointMasks[2];
        Vector3 f = layerVec(0, x, y, z, dam, dam2, pointOffset, flipDepth, footPrint);
        if (lastDam[ind + 0] != footPrint[0]) clearIndexAkt(lastIndex, ind); lastDam[ind + 0] = footPrint[0];
        if ((footPrint[0] & 2048) == 0) indSv = 0; else indSv = vertexDisplacementIndicator(0, x, y, z, dam, pointOffset, flipDepth, pointMode, x, y, z);
        if (lastIndex[ind + 0, 0] < 0) lastIndex[ind + 0, 0] = quadIndicesA[0] = makeVertexInn(f + get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        if (pointMode[0] == 7) quadIndicesD[0] = makeVertexInn(f + hori_mSf + vert_mSf, indInn);
        if ((indSv & pointMasks[1]) != 0) { if (lastIndex[ind + 0, 1] < 0) lastIndex[ind + 0, 1] = quadIndicesBp[0] = makeVertexInn(f + hori_mSf + get_dept_mSf(mSf, indSv, pointMasks[2], pointOffset, flipDepth), indInn); }
        if ((indSv & pointMasks[2]) != 0) { if (lastIndex[ind + 0, 2] < 0) lastIndex[ind + 0, 2] = quadIndicesCp[0] = makeVertexInn(f + vert_mSf + get_dept_mSf(mSf, indSv, pointMasks[1], pointOffset, flipDepth), indInn); }
        if ((pointMode[0] != 0) && (pointMode[0] != 3)) cmpDepth[0] = compareVisibilityHV(0, x, y, z, dam, pointOffset, indSv, flipDepth, pointMode[0]);
        //============================================================ point 1 =============================================
        int xa = x + pointOffset[0]; int ya = y + pointOffset[1]; int za = z + pointOffset[2];
        mSf = cornerVector(xa, ya, za);
        hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal
        vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical
        mask = pointMasks[3] + pointMasks[2];
        f = layerVec(1, xa, ya, za, dam, dam2, pointOffset, flipDepth, footPrint);
        if (lastDam[ind + 1] != footPrint[0]) clearIndexAkt(lastIndex, ind + 1); lastDam[ind + 1] = footPrint[0];
        if ((footPrint[0] & 2048) == 0) indSv = 0; else indSv = vertexDisplacementIndicator(1, x, y, z, dam, pointOffset, flipDepth, pointMode, xa, ya, za);
        if (lastIndex[ind + 1, 0] < 0) lastIndex[ind + 1, 0] = quadIndicesA[1] = makeVertexInn(f + get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        if (pointMode[1] == 7) quadIndicesD[1] = makeVertexInn(f - hori_mSf + vert_mSf, indInn);
        if ((indSv & pointMasks[3]) != 0) { if (lastIndex[ind + 1, 3] < 0) lastIndex[ind + 1, 3] = quadIndicesBm[1] = makeVertexInn(f - hori_mSf + get_dept_mSf(mSf, indSv, pointMasks[2], pointOffset, flipDepth), indInn); }
        if ((indSv & pointMasks[2]) != 0) { if (lastIndex[ind + 1, 2] < 0) lastIndex[ind + 1, 2] = quadIndicesCp[1] = makeVertexInn(f + vert_mSf + get_dept_mSf(mSf, indSv, pointMasks[3], pointOffset, flipDepth), indInn); }
        if ((pointMode[1] != 0) && (pointMode[1] != 3)) cmpDepth[1] = compareVisibilityHV(1, xa, ya, za, dam, pointOffset, indSv, flipDepth, pointMode[1]);
        //============================================================ point 2 =============================================
        int xb = x + pointOffset[3]; int yb = y + pointOffset[4]; int zb = z + pointOffset[5];
        mSf = cornerVector(xb, yb, zb);
        hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal
        vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical
        mask = pointMasks[1] | pointMasks[4];
        f = layerVec(2, xb, yb, zb, dam, dam2, pointOffset, flipDepth, footPrint);
        if (tempDam[ind + 0] != footPrint[0]) clearIndexAkt(tempIndex, ind); tempDam[ind + 0] = footPrint[0];
        if ((footPrint[0] & 2048) == 0) indSv = 0; else indSv = vertexDisplacementIndicator(2, x, y, z, dam, pointOffset, flipDepth, pointMode, xb, yb, zb);
        if (tempIndex[ind + 0, 0] < 0) tempIndex[ind + 0, 0] = quadIndicesA[2] = makeVertexInn(f + get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        if (pointMode[2] == 7) quadIndicesD[2] = makeVertexInn(f + hori_mSf - vert_mSf, indInn);
        if ((indSv & pointMasks[1]) != 0) { if (tempIndex[ind + 0, 1] < 0) tempIndex[ind + 0, 1] = quadIndicesBp[2] = makeVertexInn(f + hori_mSf + get_dept_mSf(mSf, indSv, pointMasks[4], pointOffset, flipDepth), indInn); }
        if ((indSv & pointMasks[4]) != 0) { if (tempIndex[ind + 0, 4] < 0) tempIndex[ind + 0, 4] = quadIndicesCm[2] = makeVertexInn(f - vert_mSf + get_dept_mSf(mSf, indSv, pointMasks[1], pointOffset, flipDepth), indInn); }
        //============================================================ point 3 =============================================
        int xc = x + pointOffset[6]; int yc = y + pointOffset[7]; int zc = z + pointOffset[8];
        mSf = cornerVector(xc, yc, zc);
        hori_mSf = set_cornerHorizontal(mSf, pointOffset); //corner horizontal 
        vert_mSf = set_cornerVertical(mSf, pointOffset); //corner vertical  
        mask = pointMasks[3] | pointMasks[4];
        f = layerVec(3, xc, yc, zc, dam, dam2, pointOffset, flipDepth, footPrint);
        tempDam[ind + 1] = footPrint[0]; if ((footPrint[0] & 2048) == 0) indSv = 0; else indSv = vertexDisplacementIndicator(3, x, y, z, dam, pointOffset, flipDepth, pointMode, xc, yc, zc);
        tempIndex[ind + 1, 0] = quadIndicesA[3] = makeVertexInn(f + get_dept_mSf(mSf, indSv, mask | 1024, pointOffset, flipDepth), indInn);
        if (pointMode[3] == 7) quadIndicesD[3] = makeVertexInn(f - hori_mSf - vert_mSf, indInn);
        if ((indSv & pointMasks[3]) != 0) { tempIndex[ind + 1, 3] = quadIndicesBm[3] = makeVertexInn(f - hori_mSf + get_dept_mSf(mSf, indSv, pointMasks[4], pointOffset, flipDepth), indInn); }
        if ((indSv & pointMasks[4]) != 0) { tempIndex[ind + 1, 4] = quadIndicesCm[3] = makeVertexInn(f - vert_mSf + get_dept_mSf(mSf, indSv, pointMasks[3], pointOffset, flipDepth), indInn); }
        if ((pointMode[3] != 0) && (pointMode[3] != 3)) cmpDepth[3] = compareVisibilityHV(3, xc, yc, zc, dam, pointOffset, indSv, flipDepth, pointMode[3]);
        //===========================================================================================
        inner_triangles_len = makeMultiFaceD(quadIndicesA, quadIndicesBp, quadIndicesBm, quadIndicesCp, quadIndicesCm, quadIndicesD, pointMode, cmpDepth, flip, inner_triangles, inner_triangles_len);
        displayFaceInfo = 0;
    }

    //=================== global box_vectors ========================================================
    float layerX(int x, int y, int z, int dam, int[] pointOffset, int[] footPrint)
    {
        float ret;
        if (pointOffset[9] == 0) ret = box_vectors[x, y, z].x + layerMove(x, 0, dam & 2, dam & 1, footPrint); else ret = box_vectors[x, y, z].z + layerMove(z, 2, dam & 32, dam & 16, footPrint);
        return (ret);
    }

    float layerY(int x, int y, int z, int dam, int[] pointOffset, int[] footPrint)
    {
        float ret;
        if (pointOffset[10] != 0) ret = box_vectors[x, y, z].z + layerMove(z, 2, dam & 32, dam & 16, footPrint); else ret = box_vectors[x, y, z].y + layerMove(y, 1, dam & 8, dam & 4, footPrint);
        return (ret);
    }

    float layerZ(int x, int y, int z, int[] pointOffset, bool flipDepth)
    {
        float ret;
        ret = (box_vectors[x, y, z].x * pointOffset[9] + box_vectors[x, y, z].y * pointOffset[10] + box_vectors[x, y, z].z * pointOffset[11]) * pointOffset[12] + (fLayer.x * pointOffset[9] + fLayer.y * pointOffset[10] + fLayer.z * pointOffset[11]) * pointOffset[13];
        if (pointOffset[12] > 0) { if (flipDepth) ret = 1.0f - ret; }
        return (ret);
    }

    Vector3 layerVec(int corner, int x, int y, int z, int dam, int dama, int[] pointOffset, bool flipDepth, int[] footPrint)
    {//, int shape_mask, int action_mask ) {
        Vector3 f;
        footPrint[0] = 0;
        f.x = layerX(x, y, z, dam, pointOffset, footPrint);
        f.y = layerY(x, y, z, dam, pointOffset, footPrint);
        f.z = layerZ(x, y, z, pointOffset, flipDepth);
        //=========== check vertex visibility ========
        int[] xBits = new int[4] { 2, 1, 2, 1 };
        int[] yBits = new int[4] { 2, 2, 1, 1 };

        int vis = 0;
        if (pointOffset[9] == 0)
        {
            if ((dama & (xBits[corner])) == 0) vis = 1;
        }
        else if ((dama & (xBits[corner] * 16)) == 0) vis = 1;
        //===========================
        if (pointOffset[10] != 0)
        {
            if ((dama & (yBits[corner] * 16)) == 0) vis = 2;
        }
        else if ((dama & (yBits[corner] * 4)) == 0) vis = 2;
        if (vis != 0) footPrint[0] |= 2048;
        return (f);
    }

    int layerTest(int x, int y, int z, int dam, int[] pointOffset)
    {
        int flag = 0;
        if (pointOffset[9] == 0) flag |= layerCmp(x, 0, dam & 2, dam & 1); else flag |= layerCmp(z, 2, dam & 32, dam & 16);
        if (pointOffset[10] != 0) flag |= layerCmp(z, 2, dam & 32, dam & 16); else flag |= layerCmp(y, 1, dam & 8, dam & 4);
        return (flag);
    }

    int layerCmp(int val, int ind, int mask, int maskminus)
    {
        int[] retFlag = new int[3] { 1, 4, 16 };
        if ((val == 0) && (mask == 0)) return (retFlag[ind] * 2); else if ((val == box_hunks_sizes[ind]) && (maskminus == 0)) return (retFlag[ind]); else return (0);
    }

    Vector3 layerXY(int x, int y, int z, int dam, int[] pointOffset)
    {
        Vector3 move = new Vector3(0f, 0f, 0f);
        int[] footPrint = new int[1];
        if (pointOffset[9] == 0) move.x += layerMove(x, 0, dam & 2, dam & 1, footPrint); move.z += layerMove(z, 2, dam & 32, dam & 16, footPrint);
        if (pointOffset[10] != 0) move.z += layerMove(z, 2, dam & 32, dam & 16, footPrint); else move.y += layerMove(y, 1, dam & 8, dam & 4, footPrint);
        return (move);
    }


    float layerMove(int val, int ind, int mask, int maskminus, int[] footPrint)
    {
        int[] retFlag = new int[3] { 1, 4, 16 };
        if ((val == 0) && (mask == 0)) { footPrint[0] |= retFlag[ind] * 2; return (fLayer[ind]); }
        else if ((val == box_hunks_sizes[ind]) && (maskminus == 0)) { footPrint[0] |= retFlag[ind]; return (-fLayer[ind]); } else return (0f);
    }


    //====================================================================
    Vector3 set_hori_mSf(Vector3 mSf, int[] pointOffset)
    {
        Vector3 hori_mSf = new Vector3(mSf.x * (float)pointOffset[0], mSf.y * (float)pointOffset[1], mSf.z * (float)pointOffset[2]);
        //Vector3 hori_mSf = new Vector3 ( 0f, 0f, 0f );
        return (hori_mSf);
    }

    Vector3 set_vert_mSf(Vector3 mSf, int[] pointOffset)
    {
        Vector3 vert_mSf = new Vector3(mSf.x * (float)pointOffset[3], mSf.y * (float)pointOffset[4], mSf.z * (float)pointOffset[5]);
        return (vert_mSf);
    }

    Vector3 set_cornerDepth(Vector3 mSf, int[] pointOffset)
    {
        Vector3 dept_mSf = new Vector3(mSf.x * (float)pointOffset[9], mSf.y * (float)pointOffset[10], mSf.z * (float)pointOffset[11]);
        return (dept_mSf);
    }

    Vector3 set_cornerHorizontal(Vector3 mSf, int[] pointOffset)
    {
        Vector3 hori_mSf = new Vector3(mSf.z * (float)pointOffset[9] + mSf.x * (float)pointOffset[11], mSf.z * (float)pointOffset[10], 0f); // ok 2,3,4,5
        return (hori_mSf);
    }

    Vector3 set_cornerVertical(Vector3 mSf, int[] pointOffset)
    {
        Vector3 vert_mSf = new Vector3(mSf.x * (float)pointOffset[3], mSf.y * (float)pointOffset[4], 0f); // ok 0,1,2,3,4,5 
        return (vert_mSf);
    }

    Vector3 cornerVector(int x, int y, int z)
    {
        return (new Vector3(bluntFraction * chippedFragmentsDifferentiation[(x + z) & 7] / (float)box_hunks_sizes[0], bluntFraction * chippedFragmentsDifferentiation[(y + x) & 7] / (float)box_hunks_sizes[1], bluntFraction * chippedFragmentsDifferentiation[(z + y) & 7] / (float)box_hunks_sizes[2]));
    }

    Vector3 get_dept_mSf(Vector3 mSf, int shape_mask, int action_mask, int[] pointOffset, bool flip)
    {
        Vector3 ret = new Vector3(0f, 0f, 0f);
        if (((action_mask & 1024) == 0) && ((shape_mask & disableDepth) != 0)) return (ret);
        if ((shape_mask & action_mask) == 0) return (ret);
        if (flip) ret.z += gMf(1, shape_mask, -mSf.x) * (float)pointOffset[9] + gMf(2, shape_mask, -mSf.y) * (float)pointOffset[10] + gMf(4, shape_mask, -mSf.z) * (float)pointOffset[11];
        else ret.z += gMf(8, shape_mask, -mSf.x) * (float)pointOffset[9] + gMf(16, shape_mask, -mSf.y) * (float)pointOffset[10] + gMf(32, shape_mask, -mSf.z) * (float)pointOffset[11];
        return (ret);
    }


    Vector3 getDepthChip(int mask, bool flip)
    {
        Vector3 ret = new Vector3(0f, 0f, 0f);
        if (flip) ret.z += -gDp(1, 2, mask, fLayer.x) - gDp(4, 8, mask, fLayer.y) - gDp(16, 32, mask, fLayer.z);
        else ret.z += gDp(1, 2, mask, fLayer.x) + gDp(4, 8, mask, fLayer.y) + gDp(16, 32, mask, fLayer.z);
        return (ret);
    }


    float gMf(int maska, int code, float mSf) { if ((code & maska) == maska) return (mSf); else return (0f); }

    float gSf(int maska, int maskb, int code, float mSf)
    {
        if ((maska != 0) && ((code & maska) == maska)) return (mSf); else if ((maskb != 0) && ((code & maskb) == maskb)) return (-mSf); else return (0f);
    }

    float gDp(int maska, int maskb, int code, float mSf)
    {
        if (((code & maska) == maska) || ((code & maska + maskb) == maskb)) return (mSf); else return (0f);
    }

    //====================================================================================
    void clearTempLine(int[,] temp, int[,] temp2)
    {
        temp[0, 0] = -1; temp[0, 1] = -1; temp[0, 2] = -1; temp[0, 3] = -1; temp[0, 4] = -1; temp2[0, 0] = -1; temp2[0, 1] = -1; temp2[0, 2] = -1; temp2[0, 3] = -1; temp2[0, 4] = -1;  // TABVERTSIZE !
        temp[1, 0] = -1; temp[1, 1] = -1; temp[1, 2] = -1; temp[1, 3] = -1; temp[1, 4] = -1; temp2[1, 0] = -1; temp2[1, 1] = -1; temp2[1, 2] = -1; temp2[1, 3] = -1; temp2[1, 4] = -1;
    }

    int clearLastIndexLine(int[,] indexLine, int len)
    {
        for (int a = 0; a <= len; ++a) { indexLine[a, 0] = -1; indexLine[a, 1] = -1; indexLine[a, 2] = -1; indexLine[a, 3] = -1; indexLine[a, 4] = -1; } // TABVERTSIZE !
        return (1);
    }

    void fillQuadData(int[] qA, int[] qBp, int[] qBm, int[] qCp, int[] qCm, int[,] lastIndex, int[,] tempIndex, int ind)
    {
        qA[0] = lastIndex[ind + 0, 0];
        qA[1] = lastIndex[ind + 1, 0];
        qA[2] = tempIndex[ind + 0, 0];
        qA[3] = tempIndex[ind + 1, 0] = -1;
        qBp[0] = lastIndex[ind + 0, 1]; // TABVERTSIZE !
        qBp[1] = lastIndex[ind + 1, 1];
        qBp[2] = tempIndex[ind + 0, 1];
        qBp[3] = tempIndex[ind + 1, 1] = -1;
        qCp[0] = lastIndex[ind + 0, 2];
        qCp[1] = lastIndex[ind + 1, 2];
        qCp[2] = tempIndex[ind + 0, 2];
        qCp[3] = tempIndex[ind + 1, 2] = -1;
        qBm[0] = lastIndex[ind + 0, 3]; // TABVERTSIZE !
        qBm[1] = lastIndex[ind + 1, 3];
        qBm[2] = tempIndex[ind + 0, 3];
        qBm[3] = tempIndex[ind + 1, 3] = -1;
        qCm[0] = lastIndex[ind + 0, 4];
        qCm[1] = lastIndex[ind + 1, 4];
        qCm[2] = tempIndex[ind + 0, 4];
        qCm[3] = tempIndex[ind + 1, 4] = -1;
    }

    void clearIndexAkt(int[,] tempIndex, int z)
    {
        tempIndex[z, 0] = -1;
        tempIndex[z, 1] = -1;
        tempIndex[z, 2] = -1;
        tempIndex[z, 3] = -1;
        tempIndex[z, 4] = -1; // TABVERTSIZE !
    }

    void clearIndex(int[,] tempIndex, int z)
    {
        clearIndexAkt(tempIndex, z + 1);
    }

    void clearIndex(int[,] tempIndex, int[,] tempIndex2, int z)
    {
        clearIndexAkt(tempIndex, z + 1);
        clearIndexAkt(tempIndex2, z + 1);
    }

    void copyTempLast(int[,] tempIndex, int[,] lastIndex, int[] tempDan, int[] lastDan, int len)
    {
        for (int i = 0; i <= len; ++i)
        {
            lastIndex[i, 0] = tempIndex[i, 0];
            lastIndex[i, 1] = tempIndex[i, 1];
            lastIndex[i, 2] = tempIndex[i, 2];
            lastIndex[i, 3] = tempIndex[i, 3];
            lastIndex[i, 4] = tempIndex[i, 4];

        }
        Array.Copy(tempDan, lastDan, len + 1);
    }

    float compareVisibilityH(int corner, int x, int y, int z, int dam, int[] pointOffset, int shape_mask, bool flipDepth)
    {
        int wid = (shape_mask & 9) * pointOffset[9] + (shape_mask & 18) * pointOffset[10] + (shape_mask & 36) * pointOffset[11];
        if (wid > 0)
        {
            int xa = (x + z) & 7;
            int ya = (y + x) & 7;
            int za = (z + y) & 7;
            float depth = chippedFragmentsDifferentiation[xa] * pointOffset[9] + chippedFragmentsDifferentiation[ya] * pointOffset[10] + chippedFragmentsDifferentiation[za] * pointOffset[11];
            float cx = chippedFragmentsDifferentiation[xa] * pointOffset[0] + chippedFragmentsDifferentiation[za] * pointOffset[2];
            return (cx / depth); // to cmp.x
        }
        return (1f);
    }

    float compareVisibilityV(int corner, int x, int y, int z, int dam, int[] pointOffset, int shape_mask, bool flipDepth)
    {
        int wid = (shape_mask & 9) * pointOffset[9] + (shape_mask & 18) * pointOffset[10] + (shape_mask & 36) * pointOffset[11];
        if (wid > 0)
        {
            int xa = (x + z) & 7;
            int ya = (y + x) & 7;
            int za = (z + y) & 7;
            float depth = chippedFragmentsDifferentiation[xa] * pointOffset[9] + chippedFragmentsDifferentiation[ya] * pointOffset[10] + chippedFragmentsDifferentiation[za] * pointOffset[11];
            float cy = chippedFragmentsDifferentiation[xa] * pointOffset[3] + chippedFragmentsDifferentiation[ya] * pointOffset[4];
            return (cy / depth); // to cmp.y
        }
        return (1f);
    }

    Vector2 compareVisibilityHV(int corner, int x, int y, int z, int dam, int[] pointOffset, int shape_mask, bool flipDepth, int pointMode)
    {
        Vector2 cmp = new Vector2(1f, 1f);
        if ((pointMode & 2) != 0) cmp.x = compareVisibilityH(corner, x, y, z, dam, pointOffset, shape_mask, flipDepth);
        if ((pointMode & 1) != 0) cmp.y = compareVisibilityV(corner, x, y, z, dam, pointOffset, shape_mask, flipDepth);
        return (cmp);
    }

    Vector3 tstHf(Vector2 cmp, Vector2 cmp2) { return (new Vector3(cmp.x, cmp2.x, cmp.x * cmp2.x)); }
    Vector3 tstVf(Vector2 cmp, Vector2 cmp2) { return (new Vector3(cmp.y, cmp2.y, cmp.y * cmp2.y)); }

    // Vector2[] cmpDepth
    bool tstH(Vector2 cmp) { if (cmp.x >= 0.999f) return (true); else return (false); }
    bool tstH(Vector2 cmp, Vector2 cmp2) { if ((cmp.x * cmp2.x) >= 0.999f) return (true); else return (false); }

    bool tstV(Vector2 cmp) { if (cmp.y >= 0.999f) return (true); else return (false); }
    bool tstV(Vector2 cmp, Vector2 cmp2) { if ((cmp.y * cmp2.y) >= 0.999f) return (true); else return (false); }

    bool tstHV(Vector2 cmp) { if ((cmp.y >= 0.999f) && (cmp.x >= 0.999f)) return (true); else return (false); }

    /*****************************************************************************
     * 
     * 
     *              Medium detail Mesh
     * 
     * ***************************************************************************/

    void MakeMeshMediumDetails()
    {
        //=================== grubosc wallPlasterLayer =====
        float[] fLayerX = new float[box_hunks_sizes[0] + 1];
        for (int n = 1; n < box_hunks_sizes[0]; ++n) fLayerX[n] = 0.0f;
        fLayerX[0] = fLayer.x;
        fLayerX[box_hunks_sizes[0]] = fLayer.x;
        float[] fLayerY = new float[box_hunks_sizes[1] + 1];
        for (int n = 1; n < box_hunks_sizes[1]; ++n) fLayerY[n] = 0.0f;
        fLayerY[0] = fLayer.y;
        fLayerY[box_hunks_sizes[1]] = fLayer.y;
        float[] fLayerZ = new float[box_hunks_sizes[2] + 1];
        for (int n = 1; n < box_hunks_sizes[2]; ++n) fLayerZ[n] = 0.0f;
        fLayerZ[0] = fLayer.z;
        fLayerZ[box_hunks_sizes[2]] = fLayer.z;
        float[] damTab = new float[128];
        for (int n = 1; n < 128; ++n) damTab[n] = 0.0f;
        damTab[0] = 1.0f;
        //============================================
        int max_Line_len = box_hunks_sizes[0];
        if (max_Line_len < box_hunks_sizes[1]) max_Line_len = box_hunks_sizes[1];
        if (max_Line_len < box_hunks_sizes[2]) max_Line_len = box_hunks_sizes[2];
        int[] temp_index_last_line = new int[max_Line_len + 1];
        int[] last_index_last_line = new int[max_Line_len + 1];
        int[] temp2_index_last_line = new int[max_Line_len + 1];
        int[] last2_index_last_line = new int[max_Line_len + 1];
        int[] last_dam_last_line = new int[max_Line_len + 1];
        int[] temp_dam_last_line = new int[max_Line_len + 1];
        int[] last2_dam_last_line = new int[max_Line_len + 1];
        int[] temp2_dam_last_line = new int[max_Line_len + 1];
        /************************************************************************
         * 
         *      1=X big   2=X small   4=Y big  8=Y small  16=Z duz 32=Z small
         * 
         * **********************************************************************/
        int clr = 0;
        int don = 0;
        int clr2 = 0;
        int don2 = 0;
        if (face_exist[0] > 1)
        {
            clr = 0;
            clr2 = 0;
            int x = box_hunks_sizes[0] - 1;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] & 1) != 0)
                    {
                        don = 1;
                        if (clr == 0) { for (int a = 0; a <= box_hunks_sizes[2]; ++a) last_index_last_line[a] = -1; clr = 1; }
                        if (last_index_last_line[z + 0] < 0) last_index_last_line[z + 0] = makeVertexOut(x, y, z, indOut0);
                        if (last_index_last_line[z + 1] < 0) last_index_last_line[z + 1] = makeVertexOut(x, y, z + 1, indOut0);
                        if (temp_index_last_line[z + 0] < 0) temp_index_last_line[z + 0] = makeVertexOut(x, y + 1, z, indOut0);
                        temp_index_last_line[z + 1] = makeVertexOut(x, y + 1, z + 1, indOut0);
                        outer_triangles_len = makeFace(last_index_last_line[z], last_index_last_line[z + 1], temp_index_last_line[z], temp_index_last_line[z + 1], false, outer_triangles, outer_triangles_len);
                        temp2_index_last_line[z + 1] = -1;
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0) { for (int a = 0; a <= box_hunks_sizes[2]; ++a) last2_index_last_line[a] = -1; clr2 = 1; }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if (last2_index_last_line[z + 0] < 0) last2_index_last_line[z + 0] = makeVertexInn(box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], fLayer.x); // fLayer.x * face_pos_vectors [0, 2];
                        if (last2_index_last_line[z + 1] < 0) last2_index_last_line[z + 1] = makeVertexInn(box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y, z + 1].y + fLayerY[y] * damTab[dam & 8], fLayer.x);
                        if (temp2_index_last_line[z + 0] < 0) temp2_index_last_line[z + 0] = makeVertexInn(box_vectors[x, y + 1, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.x);
                        temp2_index_last_line[z + 1] = makeVertexInn(box_vectors[x, y + 1, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y + 1, z + 1].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.x);
                        inner_triangles_len = makeFace(last2_index_last_line[z], last2_index_last_line[z + 1], temp2_index_last_line[z], temp2_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                        temp_index_last_line[z + 1] = -1;
                    }
                    else temp_index_last_line[z + 1] = temp2_index_last_line[z + 1] = -1; // clear
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[2] + 1); else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1); else clr2 = 0;
            }
        }

        if (face_exist[1] > 1)
        { //x=-1
            clr = 0;
            clr2 = 0;
            int x = 0;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    //if(( y<3 )&&( z==0)) {
                    if ((box_fragments_power[x, y, z] & 2) != 0)
                    {
                        don = 1;
                        if (clr == 0) { for (int a = 0; a <= box_hunks_sizes[2]; ++a) last_index_last_line[a] = -1; clr = 1; }
                        if (last_index_last_line[z + 0] < 0) last_index_last_line[z + 0] = makeVertexOut(x, y, z, indOut1);
                        if (last_index_last_line[z + 1] < 0) last_index_last_line[z + 1] = makeVertexOut(x, y, z + 1, indOut1);
                        if (temp_index_last_line[z + 0] < 0) temp_index_last_line[z + 0] = makeVertexOut(x, y + 1, z, indOut1);
                        temp_index_last_line[z + 1] = makeVertexOut(x, y + 1, z + 1, indOut1);
                        outer_triangles_len = makeFace(last_index_last_line[z], temp_index_last_line[z], last_index_last_line[z + 1], temp_index_last_line[z + 1], false, outer_triangles, outer_triangles_len);
                        temp2_index_last_line[z + 1] = -1;
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0) { for (int a = 0; a <= box_hunks_sizes[2]; ++a) last2_index_last_line[a] = -1; clr2 = 1; }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if (last2_index_last_line[z + 0] < 0) last2_index_last_line[z + 0] = makeVertexInn1(box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], fLayer.x);
                        if (last2_index_last_line[z + 1] < 0) last2_index_last_line[z + 1] = makeVertexInn1(box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y, z + 1].y + fLayerY[y] * damTab[dam & 8], fLayer.x);
                        if (temp2_index_last_line[z + 0] < 0) temp2_index_last_line[z + 0] = makeVertexInn1(box_vectors[x, y + 1, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.x);
                        temp2_index_last_line[z + 1] = makeVertexInn1(box_vectors[x, y + 1, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y + 1, z + 1].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.x);
                        inner_triangles_len = makeFace(last2_index_last_line[z], temp2_index_last_line[z], last2_index_last_line[z + 1], temp2_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                        temp_index_last_line[z + 1] = -1;
                    }
                    else temp_index_last_line[z + 1] = temp2_index_last_line[z + 1] = -1; // clear
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[2] + 1); else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1); else clr2 = 0;
            }
        }
        //==============================face +Y ==========================================
        if (face_exist[2] > 1)
        { //y=1
            clr = 0;
            clr2 = 0;
            int y = box_hunks_sizes[1] - 1;
            for (int x = 0; x < box_hunks_sizes[0]; ++x)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] & 4) != 0)
                    {
                        don = 1;
                        if (clr == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last_index_last_line[a] = -1;
                            clr = 1;
                        }
                        if (last_index_last_line[z + 0] < 0) last_index_last_line[z + 0] = makeVertexOut(x, y, z, indOut2);
                        if (last_index_last_line[z + 1] < 0) last_index_last_line[z + 1] = makeVertexOut(x, y, z + 1, indOut2);
                        if (temp_index_last_line[z + 0] < 0) temp_index_last_line[z + 0] = makeVertexOut(x + 1, y, z, indOut2);
                        temp_index_last_line[z + 1] = makeVertexOut(x + 1, y, z + 1, indOut2);
                        outer_triangles_len = makeFace(last_index_last_line[z], temp_index_last_line[z], last_index_last_line[z + 1], temp_index_last_line[z + 1], false, outer_triangles, outer_triangles_len);
                        temp2_index_last_line[z + 1] = -1;
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last2_index_last_line[a] = -1;
                            clr2 = 1;
                        }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if (last2_index_last_line[z + 0] < 0) last2_index_last_line[z + 0] = makeVertexInn2(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], fLayer.y);
                        if (last2_index_last_line[z + 1] < 0) last2_index_last_line[z + 1] = makeVertexInn2(box_vectors[x, y, z + 1].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], fLayer.y);
                        if (temp2_index_last_line[z + 0] < 0) temp2_index_last_line[z + 0] = makeVertexInn2(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].z + fLayerZ[z] * damTab[dam & 32], fLayer.y);
                        temp2_index_last_line[z + 1] = makeVertexInn2(box_vectors[x + 1, y, z + 1].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], fLayer.y);
                        inner_triangles_len = makeFace(last2_index_last_line[z], temp2_index_last_line[z], last2_index_last_line[z + 1], temp2_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                        temp_index_last_line[z + 1] = -1;
                    }
                    else temp_index_last_line[z + 1] = temp2_index_last_line[z + 1] = -1; // clear
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[2] + 1);
                else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1);
                else clr2 = 0;
            }
        }

        //==============================face -Y ==========================================
        if (face_exist[3] > 1)
        { //y=-1
            clr = 0;
            clr2 = 0;
            int y = 0;
            for (int x = 0; x < box_hunks_sizes[0]; ++x)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] & 8) != 0)
                    {
                        don = 1;
                        if (clr == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last_index_last_line[a] = -1;
                            clr = 1;
                        }
                        if (last_index_last_line[z + 0] < 0) last_index_last_line[z + 0] = makeVertexOut(x, y, z, indOut3);
                        if (last_index_last_line[z + 1] < 0) last_index_last_line[z + 1] = makeVertexOut(x, y, z + 1, indOut3);
                        if (temp_index_last_line[z + 0] < 0) temp_index_last_line[z + 0] = makeVertexOut(x + 1, y, z, indOut3);
                        temp_index_last_line[z + 1] = makeVertexOut(x + 1, y, z + 1, indOut3);
                        outer_triangles_len = makeFace(last_index_last_line[z], last_index_last_line[z + 1], temp_index_last_line[z], temp_index_last_line[z + 1], false, outer_triangles, outer_triangles_len);
                        temp2_index_last_line[z + 1] = -1;
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last2_index_last_line[a] = -1;
                            clr2 = 1;
                        }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if (last2_index_last_line[z + 0] < 0) last2_index_last_line[z + 0] = makeVertexInn3(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], fLayer.y);
                        if (last2_index_last_line[z + 1] < 0) last2_index_last_line[z + 1] = makeVertexInn3(box_vectors[x, y, z + 1].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], fLayer.y);
                        if (temp2_index_last_line[z + 0] < 0) temp2_index_last_line[z + 0] = makeVertexInn3(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].z + fLayerZ[z] * damTab[dam & 32], fLayer.y);
                        temp2_index_last_line[z + 1] = makeVertexInn3(box_vectors[x + 1, y, z + 1].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], fLayer.y);
                        inner_triangles_len = makeFace(last2_index_last_line[z], last2_index_last_line[z + 1], temp2_index_last_line[z], temp2_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                        temp_index_last_line[z + 1] = -1;
                    }
                    else temp_index_last_line[z + 1] = temp2_index_last_line[z + 1] = -1; // clear
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[2] + 1);
                else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1);
                else clr2 = 0;
            }
        }
        //==============================face +Z ==========================================
        if (face_exist[4] > 1)
        { //z=1
            clr = 0;
            clr2 = 0;
            int z = box_hunks_sizes[2] - 1;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if ((box_fragments_power[x, y, z] & 16) != 0)
                    {
                        don = 1;
                        if (clr == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[0]; ++a) last_index_last_line[a] = -1;
                            clr = 1;
                        }
                        if (last_index_last_line[x + 0] < 0) last_index_last_line[x + 0] = makeVertexOut(x, y, z, indOut4); // 0 0
                        if (last_index_last_line[x + 1] < 0) last_index_last_line[x + 1] = makeVertexOut(x + 1, y, z, indOut4); // 0 1
                        if (temp_index_last_line[x + 0] < 0) temp_index_last_line[x + 0] = makeVertexOut(x, y + 1, z, indOut4); // 1 0
                        temp_index_last_line[x + 1] = makeVertexOut(x + 1, y + 1, z, indOut4); // 1 1
                        outer_triangles_len = makeFace(last_index_last_line[x], temp_index_last_line[x], last_index_last_line[x + 1], temp_index_last_line[x + 1], false, outer_triangles, outer_triangles_len); // last_index_last_line [y+1], temp_index_last_line [y],
                        temp2_index_last_line[x + 1] = -1;
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[0]; ++a) last2_index_last_line[a] = -1;
                            clr2 = 1;
                        }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if (last2_index_last_line[x + 0] < 0) last2_index_last_line[x + 0] = makeVertexInn4(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], fLayer.z);
                        if (last2_index_last_line[x + 1] < 0) last2_index_last_line[x + 1] = makeVertexInn4(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].y + fLayerY[y] * damTab[dam & 8], fLayer.z);
                        if (temp2_index_last_line[x + 0] < 0) temp2_index_last_line[x + 0] = makeVertexInn4(box_vectors[x, y + 1, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.z);
                        temp2_index_last_line[x + 1] = makeVertexInn4(box_vectors[x + 1, y + 1, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.z);
                        inner_triangles_len = makeFace(last2_index_last_line[x], temp2_index_last_line[x], last2_index_last_line[x + 1], temp2_index_last_line[x + 1], false, inner_triangles, inner_triangles_len);
                        temp_index_last_line[x + 1] = -1;
                    }
                    else temp_index_last_line[x + 1] = temp2_index_last_line[x + 1] = -1; // clear
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[0] + 1);
                else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1);
                else clr2 = 0;
            }
        }
        //==============================face +Z ==========================================
        if (face_exist[5] > 1)
        { //z=-1
            clr = 0;
            clr2 = 0;
            int z = 0;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if ((box_fragments_power[x, y, z] & 32) != 0)
                    {
                        don = 1;
                        if (clr == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[0]; ++a) last_index_last_line[a] = -1;
                            clr = 1;
                        }
                        if (last_index_last_line[x + 0] < 0) last_index_last_line[x + 0] = makeVertexOut(x, y, z, indOut5); // 0 0
                        if (last_index_last_line[x + 1] < 0) last_index_last_line[x + 1] = makeVertexOut(x + 1, y, z, indOut5); // 0 1
                        if (temp_index_last_line[x + 0] < 0) temp_index_last_line[x + 0] = makeVertexOut(x, y + 1, z, indOut5); // 1 0
                        temp_index_last_line[x + 1] = makeVertexOut(x + 1, y + 1, z, indOut5); // 1 1
                        outer_triangles_len = makeFace(last_index_last_line[x], last_index_last_line[x + 1], temp_index_last_line[x], temp_index_last_line[x + 1], false, outer_triangles, outer_triangles_len);
                        temp2_index_last_line[x + 1] = -1;
                    }
                    else if (box_fragments_power[x, y, z] > 0)
                    {
                        don2 = 1;
                        if (clr2 == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[0]; ++a) last2_index_last_line[a] = -1;
                            clr2 = 1;
                        }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if (last2_index_last_line[x + 0] < 0) last2_index_last_line[x + 0] = makeVertexInn5(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], fLayer.z);
                        if (last2_index_last_line[x + 1] < 0) last2_index_last_line[x + 1] = makeVertexInn5(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].y + fLayerY[y] * damTab[dam & 8], fLayer.z);
                        if (temp2_index_last_line[x + 0] < 0) temp2_index_last_line[x + 0] = makeVertexInn5(box_vectors[x, y + 1, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.z);
                        temp2_index_last_line[x + 1] = makeVertexInn5(box_vectors[x + 1, y + 1, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], fLayer.z);
                        inner_triangles_len = makeFace(last2_index_last_line[x], last2_index_last_line[x + 1], temp2_index_last_line[x], temp2_index_last_line[x + 1], false, inner_triangles, inner_triangles_len);
                        temp_index_last_line[x + 1] = -1;
                    }
                    else temp_index_last_line[x + 1] = temp2_index_last_line[x + 1] = -1; // clear
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[0] + 1);
                else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1);
                else clr2 = 0;
            }
        }
        /*********************************************************************************************************************************
         * 
         *                                                                      Inner faces
         * 
         * *******************************************************************************************************************************/

        clr = 0;
        don = 0;
        clr2 = 0;
        don2 = 0;
        //====================== make inner layer 0,1
        for (int x = 1; x < box_hunks_sizes[0]; ++x)
        { //x
            clr = 0;
            clr2 = 0;
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] != box_fragments_power[x - 1, y, z]) && (box_fragments_power[x - 1, y, z] > 0))
                    {
                        don = 1;
                        if (clr == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last_index_last_line[a] = -1;
                            clr = 1;
                        }
                        int dam = box_fragments_power[x - 1, y, z] & damageMask;
                        if ((last_index_last_line[z + 0] < 0) || (last_dam_last_line[z + 0] != dam)) last_index_last_line[z + 0] = makeVertexInn(box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], 1.0f - box_vectors[x, y, z].x);
                        if ((last_index_last_line[z + 1] < 0) || (last_dam_last_line[z + 1] != dam)) last_index_last_line[z + 1] = makeVertexInn(box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y, z + 1].y + fLayerY[y] * damTab[dam & 8], 1.0f - box_vectors[x, y, z + 1].x);
                        if ((temp_index_last_line[z + 0] < 0) || (temp_dam_last_line[z + 0] != dam)) temp_index_last_line[z + 0] = makeVertexInn(box_vectors[x, y + 1, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], 1.0f - box_vectors[x, y + 1, z].x);
                        temp_index_last_line[z + 1] = makeVertexInn(box_vectors[x, y + 1, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y + 1, z + 1].y - fLayerY[y + 1] * damTab[dam & 4], 1.0f - box_vectors[x, y + 1, z + 1].x);
                        last_dam_last_line[z + 0] = last_dam_last_line[z + 1] = temp_dam_last_line[z + 0] = temp_dam_last_line[z + 1] = dam;
                        inner_triangles_len = makeFace(last_index_last_line[z], last_index_last_line[z + 1], temp_index_last_line[z], temp_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                    }
                    else temp_index_last_line[z + 1] = -1; // clear
                    //==================
                    if ((box_fragments_power[x, y, z] > 0) && (box_fragments_power[x - 1, y, z] != box_fragments_power[x, y, z]))
                    {
                        don2 = 1;
                        if (clr2 == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last2_index_last_line[a] = -1;
                            clr2 = 1;
                        }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if ((last2_index_last_line[z + 0] < 0) || (last2_dam_last_line[z + 0] != dam)) last2_index_last_line[z + 0] = makeVertexInn1(box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], box_vectors[x, y, z].x);
                        if ((last2_index_last_line[z + 1] < 0) || (last2_dam_last_line[z + 1] != dam)) last2_index_last_line[z + 1] = makeVertexInn1(box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y, z + 1].y + fLayerY[y] * damTab[dam & 8], box_vectors[x, y, z + 1].x);
                        if ((temp2_index_last_line[z + 0] < 0) || (temp2_dam_last_line[z + 0] != dam)) temp2_index_last_line[z + 0] = makeVertexInn1(box_vectors[x, y + 1, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], box_vectors[x, y + 1, z].x);
                        temp2_index_last_line[z + 1] = makeVertexInn1(box_vectors[x, y + 1, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y + 1, z + 1].y - fLayerY[y + 1] * damTab[dam & 4], box_vectors[x, y + 1, z + 1].x);
                        last2_dam_last_line[z + 0] = last2_dam_last_line[z + 1] = temp2_dam_last_line[z + 0] = temp2_dam_last_line[z + 1] = dam;
                        inner_triangles_len = makeFace(last2_index_last_line[z], temp2_index_last_line[z], last2_index_last_line[z + 1], temp2_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                    }
                    else temp2_index_last_line[z + 1] = -1; // clear
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[2] + 1);
                else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1);
                else clr2 = 0;
            }
        }

        //================ 2,3 =======================
        clr = 0;
        don = 0;
        clr2 = 0;
        don2 = 0;
        for (int y = 1; y < box_hunks_sizes[1]; ++y)
        { //y
            clr = 0;
            clr2 = 0;
            //=======================================================
            for (int x = 0; x < box_hunks_sizes[0]; ++x)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int z = 0; z < box_hunks_sizes[2]; ++z)
                {
                    if ((box_fragments_power[x, y, z] != box_fragments_power[x, y - 1, z]) && (box_fragments_power[x, y - 1, z] > 0))
                    {
                        don = 1;
                        if (clr == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last_index_last_line[a] = -1;
                            clr = 1;
                        }
                        int dam = box_fragments_power[x, y - 1, z] & damageMask;
                        if ((last_index_last_line[z + 0] < 0) || (last_dam_last_line[z + 0] != dam)) last_index_last_line[z + 0] = makeVertexInn2(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], 1.0f - box_vectors[x, y, z].y);
                        if ((last_index_last_line[z + 1] < 0) || (last_dam_last_line[z + 1] != dam)) last_index_last_line[z + 1] = makeVertexInn2(box_vectors[x, y, z + 1].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], 1.0f - box_vectors[x, y, z + 1].y);
                        if ((temp_index_last_line[z + 0] < 0) || (temp_dam_last_line[z + 0] != dam)) temp_index_last_line[z + 0] = makeVertexInn2(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].z + fLayerZ[z] * damTab[dam & 32], 1.0f - box_vectors[x + 1, y, z].y);
                        temp_index_last_line[z + 1] = makeVertexInn2(box_vectors[x + 1, y, z + 1].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], 1.0f - box_vectors[x + 1, y, z + 1].y);
                        last_dam_last_line[z + 0] = last_dam_last_line[z + 1] = temp_dam_last_line[z + 0] = temp_dam_last_line[z + 1] = dam;
                        inner_triangles_len = makeFace(last_index_last_line[z], temp_index_last_line[z], last_index_last_line[z + 1], temp_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                    }
                    else temp_index_last_line[z + 1] = -1;
                    //==================

                    if ((box_fragments_power[x, y, z] > 0) && (box_fragments_power[x, y - 1, z] != box_fragments_power[x, y, z]))
                    {
                        don2 = 1;
                        if (clr2 == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[2]; ++a) last2_index_last_line[a] = -1;
                            clr2 = 1;
                        }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if ((last2_index_last_line[z + 0] < 0) || (last2_dam_last_line[z + 0] != dam)) last2_index_last_line[z + 0] = makeVertexInn3(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x, y, z].y);
                        if ((last2_index_last_line[z + 1] < 0) || (last2_dam_last_line[z + 1] != dam)) last2_index_last_line[z + 1] = makeVertexInn3(box_vectors[x, y, z + 1].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x, y, z + 1].y);
                        if ((temp2_index_last_line[z + 0] < 0) || (temp2_dam_last_line[z + 0] != dam)) temp2_index_last_line[z + 0] = makeVertexInn3(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].z + fLayerZ[z] * damTab[dam & 32], box_vectors[x + 1, y, z].y);
                        temp2_index_last_line[z + 1] = makeVertexInn3(box_vectors[x + 1, y, z + 1].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z + 1].z - fLayerZ[z + 1] * damTab[dam & 16], box_vectors[x + 1, y, z + 1].y);
                        last2_dam_last_line[z + 0] = last2_dam_last_line[z + 1] = temp2_dam_last_line[z + 0] = temp2_dam_last_line[z + 1] = dam;
                        inner_triangles_len = makeFace(last2_index_last_line[z], last2_index_last_line[z + 1], temp2_index_last_line[z], temp2_index_last_line[z + 1], false, inner_triangles, inner_triangles_len);
                    }
                    else temp2_index_last_line[z + 1] = -1;
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[2] + 1); else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[2] + 1); else clr2 = 0;
            }
        }
        //================ 4,5 =======================
        clr = 0;
        don = 0;
        clr2 = 0;
        don2 = 0;
        for (int z = 1; z < box_hunks_sizes[2]; ++z)
        { //z
            clr = 0;
            clr2 = 0;
            //=======================================================
            for (int y = 0; y < box_hunks_sizes[1]; ++y)
            {
                temp_index_last_line[0] = -1;
                temp2_index_last_line[0] = -1;
                for (int x = 0; x < box_hunks_sizes[0]; ++x)
                {
                    if ((box_fragments_power[x, y, z] != box_fragments_power[x, y, z - 1]) && (box_fragments_power[x, y, z - 1] > 0))
                    {
                        don = 1;
                        if (clr == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[0]; ++a) last_index_last_line[a] = -1;
                            clr = 1;
                        }
                        int dam = box_fragments_power[x, y, z - 1] & damageMask;
                        if ((last_index_last_line[x + 0] < 0) || (last_dam_last_line[x + 0] != dam)) last_index_last_line[x + 0] = makeVertexInn4(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], 1.0f - box_vectors[x, y, z].z);
                        if ((last_index_last_line[x + 1] < 0) || (last_dam_last_line[x + 1] != dam)) last_index_last_line[x + 1] = makeVertexInn4(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].y + fLayerY[y] * damTab[dam & 8], 1.0f - box_vectors[x + 1, y, z].z);
                        if ((temp_index_last_line[x + 0] < 0) || (temp_dam_last_line[x + 0] != dam)) temp_index_last_line[x + 0] = makeVertexInn4(box_vectors[x, y + 1, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], 1.0f - box_vectors[x, y + 1, z].z);
                        temp_index_last_line[x + 1] = makeVertexInn4(box_vectors[x + 1, y + 1, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], 1.0f - box_vectors[x + 1, y + 1, z].z);
                        last_dam_last_line[z + 0] = last_dam_last_line[z + 1] = temp_dam_last_line[z + 0] = temp_dam_last_line[z + 1] = dam;
                        inner_triangles_len = makeFace(last_index_last_line[x], temp_index_last_line[x], last_index_last_line[x + 1], temp_index_last_line[x + 1], false, inner_triangles, inner_triangles_len);
                    }
                    else temp_index_last_line[x + 1] = -1;
                    if ((box_fragments_power[x, y, z] > 0) && (box_fragments_power[x, y, z - 1] != box_fragments_power[x, y, z]))
                    {
                        don2 = 1;
                        if (clr2 == 0)
                        {
                            for (int a = 0; a <= box_hunks_sizes[0]; ++a) last2_index_last_line[a] = -1;
                            clr2 = 1;
                        }
                        int dam = box_fragments_power[x, y, z] & damageMask;
                        if ((last2_index_last_line[x + 0] < 0) || (last2_dam_last_line[x + 0] != dam)) last2_index_last_line[x + 0] = makeVertexInn5(box_vectors[x, y, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y, z].y + fLayerY[y] * damTab[dam & 8], box_vectors[x, y, z].z);
                        if ((last2_index_last_line[x + 1] < 0) || (last2_dam_last_line[x + 1] != dam)) last2_index_last_line[x + 1] = makeVertexInn5(box_vectors[x + 1, y, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y, z].y + fLayerY[y] * damTab[dam & 8], box_vectors[x + 1, y, z].z);
                        if ((temp2_index_last_line[x + 0] < 0) || (temp2_dam_last_line[x + 0] != dam)) temp2_index_last_line[x + 0] = makeVertexInn5(box_vectors[x, y + 1, z].x + fLayerX[x] * damTab[dam & 2], box_vectors[x, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], box_vectors[x, y + 1, z].z);
                        temp2_index_last_line[x + 1] = makeVertexInn5(box_vectors[x + 1, y + 1, z].x - fLayerX[x + 1] * damTab[dam & 1], box_vectors[x + 1, y + 1, z].y - fLayerY[y + 1] * damTab[dam & 4], box_vectors[x + 1, y + 1, z].z);
                        last2_dam_last_line[z + 0] = last2_dam_last_line[z + 1] = temp2_dam_last_line[z + 0] = temp2_dam_last_line[z + 1] = dam;
                        inner_triangles_len = makeFace(last2_index_last_line[x], last2_index_last_line[x + 1], temp2_index_last_line[x], temp2_index_last_line[x + 1], false, inner_triangles, inner_triangles_len);
                        //Debug.Log("face y: "+ y + " dam: " + dam + " wsp8: " + fLayerY[y]*damTab[dam&8] + " wsp4: " + fLayerY[y]*damTab[dam&4]);
                    }
                    else temp2_index_last_line[x + 1] = -1;
                }
                if (don > 0) Array.Copy(temp_index_last_line, last_index_last_line, box_hunks_sizes[0] + 1); else clr = 0;
                if (don2 > 0) Array.Copy(temp2_index_last_line, last2_index_last_line, box_hunks_sizes[0] + 1); else clr2 = 0;
            }
        }
    }

    int makeMultiFaceD(int[] quadIndicesA, int[] quadIndicesBp, int[] quadIndicesBm, int[] quadIndicesCp, int[] quadIndicesCm, int[] quadIndicesD, int[] pointMode, Vector2[] cmpDepth, bool flip, int[] triangles, int offset)
    {
        int[] q0 = new int[4] { quadIndicesA[0], quadIndicesA[0], quadIndicesA[2], quadIndicesA[2] }; // left
        int[] q1 = new int[4] { quadIndicesA[1], quadIndicesA[1], quadIndicesA[3], quadIndicesA[3] }; // right
        int[] q2 = new int[4] { quadIndicesA[0], quadIndicesA[0], quadIndicesA[1], quadIndicesA[1] }; // top
        int[] q3 = new int[4] { quadIndicesA[2], quadIndicesA[2], quadIndicesA[3], quadIndicesA[3] }; // bottom
        int[] q4 = new int[4] { quadIndicesA[0], quadIndicesA[1], quadIndicesA[3], quadIndicesA[2] }; // center
        //=================================== Point 0 ===================================
        int point = 0;
        if (pointMode[point] == 2)
        {
            q4[0] = quadIndicesBp[point];
            q0[1] = quadIndicesBp[point];
            q2[0] = quadIndicesBp[point]; q2[1] = quadIndicesBp[point];
            if (quadIndicesCm[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesCm[point], quadIndicesBp[point], flip, triangles, offset); // add tri 
        }
        else if (pointMode[point] == 1)
        {
            q4[0] = quadIndicesCp[point];
            q2[0] = quadIndicesCp[point];
            q0[0] = quadIndicesCp[point]; q0[1] = quadIndicesCp[point];
            if (quadIndicesBm[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesCp[point], quadIndicesBm[point], flip, triangles, offset); // add tri 
        }
        else if (pointMode[point] == 3)
        {
            q4[0] = quadIndicesBp[point];
            q0[0] = quadIndicesCp[point]; q0[1] = quadIndicesBp[point];
            q2[0] = quadIndicesBp[point]; q2[1] = quadIndicesBp[point];
            if (quadIndicesA[point] != quadIndicesBp[point]) offset = makeFace(quadIndicesA[point], quadIndicesBp[point], quadIndicesCp[point], flip, triangles, offset);
            if (quadIndicesBm[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesCp[point], quadIndicesBm[point], flip, triangles, offset); // add tri 
            if (quadIndicesCm[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesCm[point], quadIndicesBp[point], flip, triangles, offset); // add tri 
        }
        else if (pointMode[point] == 7)
        {
            q4[0] = quadIndicesD[point];
            q0[0] = quadIndicesCp[point]; q0[1] = quadIndicesD[point];
            q2[0] = quadIndicesD[point]; q2[1] = quadIndicesBp[point];
            if (tstHV(cmpDepth[point])) offset = makeFace(quadIndicesCp[point], quadIndicesBp[point], quadIndicesD[point], flip, triangles, offset);
        }
        //=================================== Point 1 ===================================
        point = 1;
        if (pointMode[point] == 2)
        {
            q4[1] = quadIndicesBm[point];
            q1[0] = quadIndicesBm[point];
            q2[2] = quadIndicesBm[point]; q2[3] = quadIndicesBm[point];
            if (quadIndicesCm[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesBm[point], quadIndicesCm[point], flip, triangles, offset); // add tri 
        }
        else if (pointMode[point] == 1)
        {
            q4[1] = quadIndicesCp[point];
            q2[3] = quadIndicesCp[point];
            q1[0] = quadIndicesCp[point]; q1[1] = quadIndicesCp[point];
            if (quadIndicesBp[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesBp[point], quadIndicesCp[point], flip, triangles, offset); // add tri 
        }
        else if (pointMode[point] == 3)
        {
            q4[1] = quadIndicesBm[point];
            q1[0] = quadIndicesBm[point]; q1[1] = quadIndicesCp[point];
            q2[2] = quadIndicesBm[point]; q2[3] = quadIndicesBm[point];
            if (quadIndicesA[point] != quadIndicesCp[point]) offset = makeFace(quadIndicesA[point], quadIndicesCp[point], quadIndicesBm[point], flip, triangles, offset);
            if (quadIndicesBp[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesBp[point], quadIndicesCp[point], flip, triangles, offset); // add tri 
            if (quadIndicesCm[point] >= 0) offset = makeFace(quadIndicesA[point], quadIndicesBm[point], quadIndicesCm[point], flip, triangles, offset); // add tri 
        }
        else if (pointMode[point] == 7)
        {
            q4[1] = quadIndicesD[point];
            q1[0] = quadIndicesD[point]; q1[1] = quadIndicesCp[point];
            q2[2] = quadIndicesBm[point]; q2[3] = quadIndicesD[point];
            if (tstHV(cmpDepth[point])) offset = makeFace(quadIndicesBm[point], quadIndicesCp[point], quadIndicesD[point], flip, triangles, offset);
        }
        //=================================== Point 2 ===================================
        point = 2;
        if (pointMode[point] == 2)
        {
            q4[3] = quadIndicesBp[point];
            q0[2] = quadIndicesBp[point];
            q3[0] = quadIndicesBp[point]; q3[1] = quadIndicesBp[point];
        }
        else if (pointMode[point] == 1)
        {
            q4[3] = quadIndicesCm[point];
            q3[1] = quadIndicesCm[point];
            q0[2] = quadIndicesCm[point]; q0[3] = quadIndicesCm[point];
        }
        else if (pointMode[point] == 3)
        {
            q4[3] = quadIndicesBp[point];
            q0[2] = quadIndicesBp[point]; q0[3] = quadIndicesCm[point];
            q3[0] = quadIndicesBp[point]; q3[1] = quadIndicesBp[point];
            if (quadIndicesA[point] != quadIndicesCm[point]) offset = makeFace(quadIndicesA[point], quadIndicesCm[point], quadIndicesBp[point], flip, triangles, offset);
        }
        else if (pointMode[point] == 7)
        {
            q4[3] = quadIndicesD[point];
            q0[2] = quadIndicesD[point]; q0[3] = quadIndicesCm[point];
            q3[0] = quadIndicesBp[point]; q3[1] = quadIndicesD[point];
            if (tstHV(cmpDepth[point])) offset = makeFace(quadIndicesBp[point], quadIndicesCm[point], quadIndicesD[point], flip, triangles, offset);
        }
        //=================================== Point 3 ===================================
        point = 3;
        if (pointMode[point] == 2)
        {
            q4[2] = quadIndicesBm[point];
            q1[3] = quadIndicesBm[point];
            q3[2] = quadIndicesBm[point]; q3[3] = quadIndicesBm[point];
        }
        else if (pointMode[point] == 1)
        {
            q4[2] = quadIndicesCm[point];
            q3[2] = quadIndicesCm[point];
            q1[2] = quadIndicesCm[point]; q1[3] = quadIndicesCm[point];
        }
        else if (pointMode[point] == 3)
        {
            q4[2] = quadIndicesBm[point];
            q1[2] = quadIndicesCm[point]; q1[3] = quadIndicesBm[point];
            q3[2] = quadIndicesBm[point]; q3[3] = quadIndicesBm[point];
            if (quadIndicesA[point] != quadIndicesBm[point]) offset = makeFace(quadIndicesA[point], quadIndicesBm[point], quadIndicesCm[point], flip, triangles, offset);
        }
        else if (pointMode[point] == 7)
        {
            q4[2] = quadIndicesD[point];
            q1[2] = quadIndicesCm[point]; q1[3] = quadIndicesD[point];
            q3[2] = quadIndicesD[point]; q3[3] = quadIndicesBm[point];
            if (tstHV(cmpDepth[point])) offset = makeFace(quadIndicesCm[point], quadIndicesBm[point], quadIndicesD[point], flip, triangles, offset);
        }
        //================================================================================
        offset = makeFaceSeq(q4[0], q4[1], q4[3], q4[2], flip, triangles, offset);
        if (tstH(cmpDepth[0], cmpDepth[2])) offset = makeFaceSeq(q0[0], q0[1], q0[3], q0[2], flip, triangles, offset);
        if (tstH(cmpDepth[1], cmpDepth[3])) offset = makeFaceSeq(q1[0], q1[1], q1[3], q1[2], flip, triangles, offset);
        if (tstV(cmpDepth[0], cmpDepth[1])) offset = makeFaceSeq(q2[0], q2[1], q2[3], q2[2], flip, triangles, offset);
        if (tstV(cmpDepth[2], cmpDepth[3])) offset = makeFaceSeq(q3[0], q3[1], q3[3], q3[2], flip, triangles, offset);
        //==================
        return (offset);
    }


    /*****************************************************************
     * 
     * 
     * 
     * ****************************************************************/

    int makeVertexInn(Vector3 f, int[] ind)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = f.z * face_pos_vectors[ind[0], ind[1]] + f.x * face_pos_vectors[ind[2], ind[3]] + f.y * face_pos_vectors[ind[4], ind[5]] + primitive_vertices[quadIndicesface[ind[6], ind[7]]];
            outer_uv[outer_vertices_index] = f.x * face_teksture_vectors[ind[2], ind[3]] + f.y * face_teksture_vectors[ind[4], ind[5]] + primitive_uv[quadIndicesface[ind[6], ind[7]]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexInn(float fa, float fb, float fc, int[] ind)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = fc * face_pos_vectors[ind[0], ind[1]] + fa * face_pos_vectors[ind[2], ind[3]] + fb * face_pos_vectors[ind[4], ind[5]] + primitive_vertices[quadIndicesface[ind[6], ind[7]]];
            outer_uv[outer_vertices_index] = fa * face_teksture_vectors[ind[2], ind[3]] + fb * face_teksture_vectors[ind[4], ind[5]] + primitive_uv[quadIndicesface[ind[6], ind[7]]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexInn(float fa, float fb, float fc)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = fc * face_pos_vectors[0, 2] + fa * face_pos_vectors[0, 0] + fb * face_pos_vectors[0, 1] + primitive_vertices[quadIndicesface[0, 0]];
            outer_uv[outer_vertices_index] = fa * face_teksture_vectors[0, 0] + fb * face_teksture_vectors[0, 1] + primitive_uv[quadIndicesface[0, 0]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }


    int makeVertexInn1(float fa, float fb, float fc)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = fc * face_pos_vectors[1, 2] + fa * face_pos_vectors[1, 0] + fb * face_pos_vectors[1, 1] + primitive_vertices[quadIndicesface[1, 3]];
            outer_uv[outer_vertices_index] = fa * face_teksture_vectors[1, 0] + fb * face_teksture_vectors[1, 1] + primitive_uv[quadIndicesface[1, 3]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index1 exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexInn2(float fa, float fb, float fc)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = fc * face_pos_vectors[2, 2] + fa * face_pos_vectors[2, 0] + fb * face_pos_vectors[2, 1] + primitive_vertices[quadIndicesface[2, 0]];
            outer_uv[outer_vertices_index] = fa * face_teksture_vectors[2, 0] + fb * face_teksture_vectors[2, 1] + primitive_uv[quadIndicesface[2, 0]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index2 exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexInn3(float fa, float fb, float fc)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = fc * face_pos_vectors[3, 2] + fa * face_pos_vectors[3, 0] + fb * face_pos_vectors[3, 1] + primitive_vertices[quadIndicesface[3, 3]];
            outer_uv[outer_vertices_index] = fa * face_teksture_vectors[3, 0] + fb * face_teksture_vectors[3, 1] + primitive_uv[quadIndicesface[3, 3]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index3 exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexInn4(float fa, float fb, float fc)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = fc * face_pos_vectors[4, 2] + fa * face_pos_vectors[4, 0] + fb * face_pos_vectors[4, 1] + primitive_vertices[quadIndicesface[4, 0]];
            outer_uv[outer_vertices_index] = fa * face_teksture_vectors[4, 0] + fb * face_teksture_vectors[4, 1] + primitive_uv[quadIndicesface[4, 0]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index4 exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexInn5(float fa, float fb, float fc)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = fc * face_pos_vectors[5, 2] + fa * face_pos_vectors[5, 0] + fb * face_pos_vectors[5, 1] + primitive_vertices[quadIndicesface[5, 1]];
            outer_uv[outer_vertices_index] = fa * face_teksture_vectors[5, 0] + fb * face_teksture_vectors[5, 1] + primitive_uv[quadIndicesface[5, 1]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index5 exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexOut(int x, int y, int z, int[] ind)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = box_vectors[x, y, z].x * face_pos_vectors[ind[0], ind[1]] + box_vectors[x, y, z].y * face_pos_vectors[ind[2], ind[3]] + box_vectors[x, y, z].z * face_pos_vectors[ind[4], ind[5]] + primitive_vertices[quadIndicesface[ind[6], ind[7]]];
            outer_uv[outer_vertices_index] = box_vectors[x, y, z].x * face_teksture_vectors[ind[0], ind[1]] + box_vectors[x, y, z].y * face_teksture_vectors[ind[2], ind[3]] + box_vectors[x, y, z].z * face_teksture_vectors[ind[4], ind[5]] + primitive_uv[quadIndicesface[ind[6], ind[7]]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************** outer_vertices_index exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    int makeVertexOutSV(int x, int y, int z, int[] ind, Vector3 sv)
    {
        if (outer_vertices_index < maxvertex)
        {
            outer_vertices[outer_vertices_index] = (box_vectors[x, y, z].x + sv.x) * face_pos_vectors[ind[0], ind[1]] + (box_vectors[x, y, z].y + sv.y) * face_pos_vectors[ind[2], ind[3]] + (box_vectors[x, y, z].z + sv.z) * face_pos_vectors[ind[4], ind[5]] + primitive_vertices[quadIndicesface[ind[6], ind[7]]];
            outer_uv[outer_vertices_index] = (box_vectors[x, y, z].x + sv.x) * face_teksture_vectors[ind[0], ind[1]] + (box_vectors[x, y, z].y + sv.y) * face_teksture_vectors[ind[2], ind[3]] + (box_vectors[x, y, z].z + sv.z) * face_teksture_vectors[ind[4], ind[5]] + primitive_uv[quadIndicesface[ind[6], ind[7]]];
            ++outer_vertices_index;
        }
        else Debug.Log("*************************** outer_vertices_index exceeded:" + outer_vertices_index);
        return (outer_vertices_index - 1);
    }

    //========================= make face ===============================================================
    int makeFace(int a, int b, int c, bool flip, int[] triangles, int offset)
    {
        if ((offset + 3) < maxtriangles)
        {
            triangles[offset + 0] = a;
            if (flip)
            {
                triangles[offset + 1] = b;
                triangles[offset + 2] = c;
            }
            else
            {
                triangles[offset + 1] = c;
                triangles[offset + 2] = b;
            }
            offset += 3;
        }
        else Debug.Log("*************************** maxtriangles exceeded: " + maxtriangles);
        return (offset);
    }

    int makeFaceSeq(int a, int b, int c, int d, bool flip, int[] triangles, int offset)
    {
        if (a == b)
        {
            if (c == d) return (offset);
            offset = makeFace(b, d, c, flip, triangles, offset);
        }
        else if (c == d)
        {
            offset = makeFace(a, b, c, flip, triangles, offset);
        }
        else offset = makeFace(a, b, c, d, flip, triangles, offset); // offset = makeFaceInSequence ( a, b, c, d,flip, triangles, offset );
        return (offset);
    }

    int makeFace(int a, int b, int c, int d, bool flip, int[] triangles, int offset)
    {
        if ((offset + 6) < maxtriangles)
        {
            triangles[offset + 0] = a;
            triangles[offset + 3] = c; // c
            if (flip)
            {
                triangles[offset + 1] = b;
                triangles[offset + 2] = c;
                triangles[offset + 4] = b; // d
                triangles[offset + 5] = d; // b
            }
            else
            {
                triangles[offset + 1] = c;
                triangles[offset + 2] = b;
                triangles[offset + 4] = d; // d
                triangles[offset + 5] = b; // b
            }
            offset += 6;
        }
        else Debug.Log("*************************** maxtriangles exceeded:" + maxtriangles);
        return (offset);
    }

    int makeFaceInSequence(int a, int b, int c, int d, bool flip, int[] triangles, int offset)
    {
        if ((offset + 6) < maxtriangles)
        {
            triangles[offset + 0] = a;
            triangles[offset + 3] = a; // c
            if (flip)
            {
                triangles[offset + 1] = b;
                triangles[offset + 2] = c;
                triangles[offset + 4] = c; // d
                triangles[offset + 5] = d; // b
            }
            else
            {
                triangles[offset + 1] = c;
                triangles[offset + 2] = b;
                triangles[offset + 4] = d; // d
                triangles[offset + 5] = c; // b
            }
            offset += 6;
        }
        else Debug.Log("*************************** maxtriangles exceeded: " + maxtriangles);
        return (offset);
    }

    //=================== In ================
    void makeFaceInn(int a, int b, int c, bool flip)
    {
        if ((a == b) || (b == c)) return;
        inner_triangles_len = makeFace(a, b, c, flip, inner_triangles, inner_triangles_len);
    }

    void makeFaceInn(int a, int b, int c, int d, bool flip)
    {
        if (a == b)
        {
            if (c == d) return;
            makeFaceInn(b, c, d, flip);
        }
        else if (c == d)
        {
            makeFaceInn(a, b, c, flip);
        }
        else inner_triangles_len = makeFace(a, b, c, d, flip, inner_triangles, inner_triangles_len);
    }

    void makeFaceSeq(int a, int b, int c, int d, bool flip)
    {
        if (a == b)
        {
            if (c == d) return;
            makeFaceInn(b, c, d, flip);
        }
        else if (c == d)
        {
            makeFaceInn(a, b, c, flip);
        }
        else inner_triangles_len = makeFaceInSequence(a, b, c, d, flip, inner_triangles, inner_triangles_len);
    }

    //=================== out ================
    void makeFaceOut(int a, int b, int c, bool flip)
    {
        if ((a == b) || (b == c)) return;
        outer_triangles_len = makeFace(a, b, c, flip, outer_triangles, outer_triangles_len);
    }

    void makeFaceOut(int a, int b, int c, int d, bool flip)
    {
        if (a == b)
        {
            if (c == d) return;
            makeFaceOut(b, c, d, flip);
        }
        else if (c == d)
        {
            makeFaceOut(a, b, c, flip);
        }
        else outer_triangles_len = makeFaceInSequence(a, b, c, d, flip, outer_triangles, outer_triangles_len);
    }

    //===================chip ========================
    int makeFaceChip(int a, int b, int c, int d, int len, int max, int[] tab)
    {
        if ((len + 6) < max)
        {
            tab[len + 0] = a;
            tab[len + 1] = c;
            tab[len + 2] = b;
            tab[len + 3] = c; // c
            tab[len + 4] = d; // d
            tab[len + 5] = b; // b
            len += 6;
        }
        return (len);
    }

    Vector2 makeFaceTekVector(int a, int b)
    {
        Vector2 vek = primitive_uv[b] - primitive_uv[a];
        return (vek);
    }

    Vector3 makeFacePosVector(int a, int b)
    {
        Vector3 vek = primitive_vertices[b] - primitive_vertices[a];
        return (vek);
    }

    void ScaleLocalVertex(Vector3[] tab, int len)
    {
        for (int n = 0; n < len; ++n) tab[n] = new Vector3(tab[n].x * primitive_scale.x, tab[n].y * primitive_scale.y, tab[n].z * primitive_scale.z);
    }

    Vector3 FindLocalCentre(Vector3[] tab, int len)
    {
        Vector3 vec = tab[0]; for (int n = 1; n < len; ++n) vec += tab[n];
        vec = vec / len;
        //======================= recalculate =================================
        for (int n = 0; n < len; ++n) tab[n] -= vec;
        return (vec);
    }

    Vector3 FindLocalSize(Vector3[] tab, int len, Vector3 centre)
    {
        Vector3 size = new Vector3(0, 0, 0);
        for (int n = 0; n < len; ++n) { size.x += Math.Abs(tab[n].x); size.y += Math.Abs(tab[n].y); size.z += Math.Abs(tab[n].z); }
        return (size / len);
    }

    float randVektor(int x, int dx)
    {
        float vek = 0.0f;
        float krok = 1.0f / (float)dx;
        if (x == 0) vek = 0.0f;
        else if (x == dx) vek = 1.0f;
        else if (fragmentDeform) vek = ((float)x * krok) + UnityEngine.Random.Range(-0.2f * krok, 0.2f * krok);
        else vek = ((float)x * krok) + UnityEngine.Random.Range(-0.03f * krok, 0.03f * krok); // FIXME grid deformation
        return (vek);
    }

    void checkOryginalFaces()
    {
        //====================== clear side info =============================================================
        for (int side = 0; side < MAXFACE; ++side) { quadIndicesface[side, 0] = -1; quadIndicesface[side, 1] = -1; quadIndicesface[side, 2] = -1; quadIndicesface[side, 3] = -1; face_exist[side] = 0; }
        //===========================================================
        for (int n = 0; n < primitive_triangles_len; n += 3)
        {
            //============================================================
            Vector3 norm = faceNormal(primitive_vertices[primitive_triangles[n + 0]], primitive_vertices[primitive_triangles[n + 1]], primitive_vertices[primitive_triangles[n + 2]]);
            if (norm.x > 0.98f) getFacePosAndUv(n, 0); else if (norm.x < -0.98f) getFacePosAndUv(n, 1);
            if (norm.y > 0.98f) getFacePosAndUv(n, 2); else if (norm.y < -0.98f) getFacePosAndUv(n, 3);
            if (norm.z > 0.98f) getFacePosAndUv(n, 4); else if (norm.z < -0.98f) getFacePosAndUv(n, 5);
        }
    }

    void getFacePosAndUv(int n, int side)
    {
        int[] qf = new int[4];
        if (face_exist[side] == 0)
        {
            face_exist_n1[side] = n; face_exist[side] = 1;
        }
        else if (face_exist[side] == 1)
        { // find quadIndices
            int k = face_exist_n1[side];
            for (int i = 0; i < 3; ++i)
            {
                if (primitive_triangles[k + 0] == primitive_triangles[n + i])
                {
                    if (primitive_triangles[k + 1] == primitive_triangles[n + prevp[i]])
                    { // nextp
                        qf[0] = primitive_triangles[k + 0];
                        qf[1] = primitive_triangles[n + nextp[i]];
                        qf[2] = primitive_triangles[k + 1];
                        qf[3] = primitive_triangles[k + 2];
                        face_exist[side] = 5;
                    }
                    else if (primitive_triangles[k + 2] == primitive_triangles[n + nextp[i]])
                    {
                        qf[0] = primitive_triangles[k + 0];
                        qf[1] = primitive_triangles[k + 1];
                        qf[2] = primitive_triangles[k + 2];
                        qf[3] = primitive_triangles[n + prevp[i]];
                        face_exist[side] = 3;
                    }
                }
                else if (primitive_triangles[k + 1] == primitive_triangles[n + i])
                {
                    if (primitive_triangles[k + 2] == primitive_triangles[n + prevp[i]])
                    { // nextp
                        qf[0] = primitive_triangles[k + 0];
                        qf[1] = primitive_triangles[k + 1];
                        qf[2] = primitive_triangles[n + nextp[i]];
                        qf[3] = primitive_triangles[k + 2];
                        face_exist[side] = 4;
                    }
                }
            }
            if (face_exist[side] > 1)
            {
                if (side == 0) rotateVerticesToFit(side, qf, 2, 1, 4); else if (side == 1) rotateVerticesToFit(side, qf, 4, 1, 2);
                if (side == 2) rotateVerticesToFit(side, qf, 0, 2, 4); else if (side == 3) rotateVerticesToFit(side, qf, 4, 2, 0);
                if (side == 4) rotateVerticesToFit(side, qf, 0, 1, 4); else if (side == 5) rotateVerticesToFit(side, qf, 4, 1, 0);
            }
        }
    }

    void rotateVerticesToFit(int side, int[] qf, int a, int b, int ma)
    {
        int minp = 0;
        float mindx = 0.0f;
        if (a == 0) mindx += primitive_vertices[qf[0]].x; else if (a == 1) mindx += primitive_vertices[qf[0]].y; else if (a == 2) mindx += primitive_vertices[qf[0]].z;
        if (b == 0) mindx += primitive_vertices[qf[0]].x; else if (b == 1) mindx += primitive_vertices[qf[0]].y; else if (b == 2) mindx += primitive_vertices[qf[0]].z;
        if (ma == 0) mindx -= primitive_vertices[qf[0]].x; else if (ma == 1) mindx -= primitive_vertices[qf[0]].y; else if (ma == 2) mindx -= primitive_vertices[qf[0]].z;
        for (int i = 1; i < 4; ++i)
        {
            float dx = 0.0f;
            if (a == 0) dx += primitive_vertices[qf[i]].x; else if (a == 1) dx += primitive_vertices[qf[i]].y; else if (a == 2) dx += primitive_vertices[qf[i]].z;
            if (b == 0) dx += primitive_vertices[qf[i]].x; else if (b == 1) dx += primitive_vertices[qf[i]].y; else if (b == 2) dx += primitive_vertices[qf[i]].z;
            if (ma == 0) dx -= primitive_vertices[qf[i]].x; else if (ma == 1) dx -= primitive_vertices[qf[i]].y; else if (ma == 2) dx -= primitive_vertices[qf[i]].z;
            if (dx < mindx) { mindx = dx; minp = i; }
        }
        //======================================================================
        for (int i = 0; i < 4; ++i) quadIndicesface[side, i] = qf[(minp + i) & 3];

    }

    Vector3 faceNormal(Vector3 a, Vector3 b, Vector3 c) { Vector3 d = Vector3.Cross(b - a, c - a); return (Vector3.Normalize(d)); }

}



